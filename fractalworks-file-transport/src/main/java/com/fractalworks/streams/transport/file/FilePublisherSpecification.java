/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.file;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.formatters.Formatter;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;


import java.util.Objects;

/**
 * File publisher using CSV as the default formatter
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "file")
public class FilePublisherSpecification extends AbstractTransportSpecification {

    private String bucketName;
    private String directoryPath;
    private String filename;

    private Formatter formatter;

    // Default 64MB file size
    private long maxFileSize = 67108864;

    public FilePublisherSpecification() {
        super();
        sendTimeout = 15000;
    }

    public FilePublisherSpecification(String name) {
        super(name);
        sendTimeout = 15000;
    }

    public String getBucketName() {
        return bucketName;
    }

    @JsonProperty(value = "bucket", required = false)
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    @JsonProperty(value = "path", required = true)
    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public String getFilename() {
        return filename;
    }

    @JsonProperty(value = "filename", required = true)
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public long getMaxFileSize() {
        return maxFileSize;
    }

    @JsonProperty(value = "maxFileSize")
    public void setMaxFileSize(long maxFileSize) {
        this.maxFileSize = maxFileSize;
    }

    public Formatter getFormatter() {
        return formatter;
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();

        if (directoryPath == null || directoryPath.isEmpty()) {
            throw new InvalidSpecificationException("path must be provided.");
        }

        if (filename == null || filename.isEmpty()) {
            throw new InvalidSpecificationException("filename must be provided.");
        }

        if (formatter == null) {
            throw new InvalidSpecificationException("formatter must be provided.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FilePublisherSpecification that)) return false;
        if (!super.equals(o)) return false;
        return maxFileSize == that.maxFileSize &&
                Objects.equals(directoryPath, that.directoryPath) &&
                Objects.equals(filename, that.filename) &&
                Objects.equals(formatter, that.formatter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), directoryPath, filename, maxFileSize, formatter);
    }

    @Override
    public String toString() {
        return "FilePublisherSpecification{" +
                "directoryPath='" + directoryPath + '\'' +
                ", filename='" + filename + '\'' +
                ", maxFileSize=" + maxFileSize +
                ", batchSize=" + batchSize +
                ", timeout=" + sendTimeout +
                ", formatter=" + formatter +
                "} ";
    }

    @JsonIgnore
    @Override
    public Class<? extends Transport> getComponentClass() {
        return FilePublisherTransport.class;
    }
}
