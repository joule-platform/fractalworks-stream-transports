/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.file;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.formatters.Formatter;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;



/**
 * File publisher specification builder
 *
 * @author Lyndon Adams
 */
public class FilePublisherSpecificationBuilder extends AbstractSpecificationBuilder<FilePublisherSpecification> {

    private Formatter formatter;
    private String bucketName;
    private String directoryPath;
    private String filename;
    private long maxFileSize = 67108864;
    private int batchSize = 1024;
    private long timeout;


    public FilePublisherSpecificationBuilder() {
        specificationClass = FilePublisherSpecification.class;
    }

    public FilePublisherSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public FilePublisherSpecificationBuilder setBucketName(String bucketName) {
        this.bucketName = bucketName;
        return this;
    }

    public FilePublisherSpecificationBuilder setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
        return this;
    }

    public FilePublisherSpecificationBuilder setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    public FilePublisherSpecificationBuilder setMaxFileSize(long maxFileSize) {
        this.maxFileSize = maxFileSize;
        return this;
    }

    public FilePublisherSpecificationBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public FilePublisherSpecificationBuilder setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public FilePublisherSpecificationBuilder setFormatter(Formatter formatter) {
        this.formatter = formatter;
        return this;
    }

    @Override
    public FilePublisherSpecification build() throws InvalidSpecificationException {

        FilePublisherSpecification spec = new FilePublisherSpecification(name);
        spec.setBucketName(bucketName);
        spec.setFilename(filename);
        spec.setDirectoryPath(directoryPath);
        spec.setMaxFileSize(maxFileSize);
        spec.setBatchSize(batchSize);
        spec.setSendTimeout(timeout);
        spec.setFormatter(formatter);

        spec.validate();
        return spec;
    }
}
