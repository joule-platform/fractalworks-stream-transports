/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.file;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.joule.config.SupportedInfrastructures;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Objects;

/**
 * File publisher transport
 *
 * @author Lyndon Adams
 */
public class FilePublisherTransport extends AbstractPublisherTransport {

    SupportedInfrastructures envType = SupportedInfrastructures.LOCAL;

    FilePublishingDelegate publisherDelegate;

    public FilePublisherTransport() {
        super();
        logger = LoggerFactory.getLogger(FilePublisherTransport.class);
    }

    public FilePublisherTransport(FilePublisherSpecification specification) {
        super(specification);
        logger = LoggerFactory.getLogger(FilePublisherTransport.class);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        switch( envType ){
            case LOCAL -> {
                publisherDelegate = new LocalFilePublisherDelegate((FilePublisherSpecification)specification, metrics,isPublishing);
            }
            case GCP -> {
                publisherDelegate = new GoogleCloudStoragePublisher((FilePublisherSpecification)specification, metrics,isPublishing);
            }
        }
        publisherDelegate.initialize();
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        publisherDelegate.publish(events);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FilePublisherTransport that = (FilePublisherTransport) o;
        return envType == that.envType && Objects.equals(publisherDelegate, that.publisherDelegate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), envType, publisherDelegate);
    }

    @Override
    public String toString() {
        return "FilePublisherTransport{" +
                "envType=" + envType +
                ", publisherDelegate=" + publisherDelegate +
                '}';
    }
}
