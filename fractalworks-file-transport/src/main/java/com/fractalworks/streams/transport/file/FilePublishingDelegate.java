package com.fractalworks.streams.transport.file;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;

import java.util.Collection;

/**
 * File Publishing Delegate interface.
 * Defines methods for publishing events and initializing the delegate.
 *
 * @author Lyndon Adams
 */
public interface FilePublishingDelegate {
    /**
     * Publishes a collection of StreamEvent objects.
     *
     * @param events The collection of StreamEvent objects to be published.
     */
    void publish(Collection<StreamEvent> events);

    /**
     * Initializes the file publishing delegate.
     *
     * @throws TransportException if an exception occurs during initialization.
     */
    void initialize() throws TransportException;
}
