/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.file;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.Metrics;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.formatters.Formatter;
import com.fractalworks.streams.sdk.storage.GCPStorageUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Google file generation
 *
 * @author Lyndon Adams
 */
public class GoogleCloudStoragePublisher implements FilePublishingDelegate {

    private Logger logger = LoggerFactory.getLogger(GoogleCloudStoragePublisher.class);
    private FilePublisherSpecification specification;
    private AtomicBoolean isPublishing;
    private Metrics metricsHandler;
    private Formatter formatterSpecification;
    private String bucketname;

    private final GCPStorageUtils utils = new GCPStorageUtils();
    private String currentFileName;
    private long maxFileSize;
    private String filename;
    private String directory;

    protected int bytesWritten = 0;

    private boolean isStringEncoding = true;

    public GoogleCloudStoragePublisher(FilePublisherSpecification specification, Metrics metricsHandler, AtomicBoolean isPublishing) {
        this.specification = specification;
        this.metricsHandler = metricsHandler;
        this.isPublishing = isPublishing;
    }

    @Override
    public void initialize() throws TransportException  {
        formatterSpecification = specification.getFormatter();
        formatterSpecification.initialise();
        maxFileSize = specification.getMaxFileSize();
        filename = String.format("%s/%s.%s", specification.getDirectoryPath(), specification.getFilename(), formatterSpecification.getFileExtension());
        isStringEncoding = "utf-8".equalsIgnoreCase(formatterSpecification.getEncoding());
        bucketname = specification.getBucketName();
        directory = specification.getDirectoryPath();

        if (logger.isInfoEnabled()) {
            logger.info("Initialization completed");
        }
    }

    public void publish(Collection<StreamEvent> events) {
        try {
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Publishing %d events to google bucket", events.size()));
                }
                for (StreamEvent evt : events) {
                        var formattedEvt = formatterSpecification.format(evt);
                        byte[] payload = SerializationUtils.serialize((Serializable) formattedEvt);
                        utils.writeDataToBucket(bucketname, currentFileName, payload);
                }
                metricsHandler.incrementMetric(Metric.PROCESSED, events.size());
            }
        } catch (IOException e) {
            metricsHandler.incrementMetric(Metric.PROCESS_FAILED);
            logger.error(String.format("Error during publishing to google bucket [%s]", bucketname), e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    @Override
    public String toString() {
        return "GoogleCloudStoragePublisher{" +
                "specification=" + specification +
                ", isPublishing=" + isPublishing +
                ", metricsHandler=" + metricsHandler +
                ", formatterSpecification=" + formatterSpecification +
                ", bucketname='" + bucketname + '\'' +
                ", utils=" + utils +
                ", currentFileName='" + currentFileName + '\'' +
                ", maxFileSize=" + maxFileSize +
                ", filename='" + filename + '\'' +
                ", directory='" + directory + '\'' +
                ", bytesWritten=" + bytesWritten +
                ", isStringEncoding=" + isStringEncoding +
                '}';
    }
}
