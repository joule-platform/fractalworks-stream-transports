/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.file;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.Metrics;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.formatters.CSVStreamEventFormatter;
import com.fractalworks.streams.sdk.formatters.Formatter;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Local file generation
 *
 * @author Lyndon Adams
 */
public class LocalFilePublisherDelegate implements FilePublishingDelegate {

    private Logger logger = LoggerFactory.getLogger(LocalFilePublisherDelegate.class);
    private FilePublisherSpecification specification;
    private AtomicBoolean isPublishing;
    private Metrics metricsHandler;
    private Formatter formatterSpecification;

    private long maxFileSize;
    protected File currentWritingFile;
    private String filename;
    private String header;

    private boolean isStringEncoding = true;

    private int eventByteSize = 0;

    private boolean bulkWriteFlag = true;

    private static final int BUFFER_SIZE = 1024*4;

    public LocalFilePublisherDelegate(FilePublisherSpecification specification, Metrics metricsHandler, AtomicBoolean isPublishing) {
        this.specification = specification;
        this.metricsHandler = metricsHandler;
        this.isPublishing = isPublishing;
    }

    @Override
    public void initialize() throws TransportException {
        formatterSpecification = specification.getFormatter();
        formatterSpecification.initialise();
        maxFileSize = specification.getMaxFileSize();
        filename = String.format("%s/%s.%s", specification.getDirectoryPath(), specification.getFilename(), formatterSpecification.getFileExtension());
        isStringEncoding = "utf-8".equalsIgnoreCase(formatterSpecification.getEncoding());

        try {
            // Check directory exists
            File dirFile = new File(specification.getDirectoryPath());
            FileUtils.forceMkdir(dirFile);
            currentWritingFile = new File(filename);

            // Move initial file is exist
            if (currentWritingFile.exists()) {
                String mvFilename = String.format("%s/%s-%d.%s", specification.getDirectoryPath(), specification.getFilename(), System.currentTimeMillis(), formatterSpecification.getFileExtension());
                FileUtils.moveFile(currentWritingFile, new File(mvFilename));

                // Create new file
                currentWritingFile = new File(filename);
            }
            if (logger.isInfoEnabled()) {
                logger.info("Initialization completed");
            }
        } catch (IOException e) {
            logger.error("Initialization failed");
            throw new TransportException("Fatal error occurred during initialization.", e);
        }
    }

    /**
     * Creates a new file if the current writing file exceeds the maximum file size.
     *
     * @param eventCount the number of events being written to the file
     * @throws IOException if an error occurs while creating the new file
     */
    private void createNewFile(int eventCount, StreamEvent sampleEvent) throws IOException {
        var bytesToWrite = eventCount * eventByteSize;
        // If the filesize has breached size move and create new file
        if (currentWritingFile.exists() && (FileUtils.sizeOf(currentWritingFile) + bytesToWrite) >=  maxFileSize) {
            String mvFilename = String.format("%s/%s-%d.%s", specification.getDirectoryPath(), specification.getFilename(), System.currentTimeMillis(),formatterSpecification.getFileExtension());
            FileUtils.moveFile(currentWritingFile, new File(mvFilename));

            // Create new file
            currentWritingFile = new File(filename);

            // Add header if required
            if(formatterSpecification instanceof CSVStreamEventFormatter csvStreamEventFormatter && sampleEvent!=null){
                if(header == null) {
                    header = csvStreamEventFormatter.createHeader(sampleEvent);
                }
                FileUtils.writeStringToFile(currentWritingFile, header, formatterSpecification.getEncoding(), true);
            }
        }
    }

    /**
     * Publishes a collection of StreamEvent objects.
     *
     * @param events the collection of StreamEvent objects to be published
     */
    public void publish(Collection<StreamEvent> events) {
        try {
            metricsHandler.incrementMetric(Metric.RECEIVED, events.size());
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                StreamEvent sampleEvent = events.stream().findFirst().orElse(null);
                createNewFile(events.size(),sampleEvent);
                var bytesWritten = 0;
                if (bulkWriteFlag) {
                    bytesWritten = bulkEventWrite(events);
                } else {
                    bytesWritten = singleEventWrite(events);
                }
                eventByteSize = Math.max(bytesWritten/events.size(), eventByteSize);
                metricsHandler.incrementMetric(Metric.PROCESSED,events.size());
                metricsHandler.incrementMetric(Metric.BYTES_WRITE, bytesWritten);
            } else
                metricsHandler.incrementMetric(Metric.DISCARDED, events.size());

        } catch (Exception e) {
            metricsHandler.incrementMetric(Metric.PROCESS_FAILED);
            logger.error("Error occurred during event publishing to file.", e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    /**
     * Writes a single collection of StreamEvent objects to a file.
     *
     * @param events the collection of StreamEvent objects to be written
     * @return the total number of bytes written to the file
     * @throws IOException if an error occurs while writing to the file
     */
    private int singleEventWrite(final Collection<StreamEvent> events) throws IOException {
        var bytesWritten = 0;
        for (StreamEvent evt : events) {
            Object formattedObj = formatterSpecification.format(evt);
            bytesWritten += writeFormattedEventToFile(formattedObj);
        }
        return bytesWritten;
    }

    /**
     * Writes a collection of StreamEvent objects in bulk to a file.
     *
     * @param events the collection of StreamEvent objects to be written
     * @return the total number of bytes written to the file
     * @throws IOException if an error occurs while writing to the file
     */
    private int bulkEventWrite(final Collection<StreamEvent> events) throws IOException {
        Object formattedObj = formatterSpecification.format(events);
        return writeFormattedEventToFile(formattedObj);
    }

    /**
     * Writes a formatted event to a file.
     *
     * @param formattedEvent the formatted event to be written
     * @return the total number of bytes written to the file
     * @throws IOException if an error occurs while writing to the file
     */
    private int writeFormattedEventToFile(Object formattedEvent) throws IOException {
        var bytesWritten = 0;
        if (isStringEncoding) {
            if (formattedEvent instanceof String strObject) {
                FileUtils.writeStringToFile(currentWritingFile, strObject, formatterSpecification.getEncoding(), true);
                bytesWritten = ((String) formattedEvent).getBytes().length;
            } else if (formattedEvent instanceof StringBuilder strBuilder) {
                try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(currentWritingFile,true))){
                    final int aLength = strBuilder.length();
                    final char[] aChars = new char[BUFFER_SIZE];
                    for (int aPosStart = 0; aPosStart < aLength; aPosStart += BUFFER_SIZE) {
                        final int aPosEnd = Math.min(aPosStart + BUFFER_SIZE, aLength);
                        strBuilder.getChars(aPosStart, aPosEnd, aChars, 0); // Create no new buffer
                        var charsToWrite = aPosEnd - aPosStart;
                        bufferedWriter.write(aChars, 0, charsToWrite); // This is faster than just copying one byte at the time
                        bytesWritten += charsToWrite * 2;
                    }
                    bufferedWriter.flush();
                }
                strBuilder.delete(0,strBuilder.length());
            }
        } else if (formattedEvent instanceof byte[] bytesToSend) {
            // Writing binary
            FileUtils.writeByteArrayToFile(currentWritingFile, bytesToSend, true);
            bytesWritten = bytesToSend.length;
        } else {
            // Convert byte array
            try (ByteArrayOutputStream bStream = new ByteArrayOutputStream(); ObjectOutputStream oStream = new ObjectOutputStream(bStream)) {
                oStream.writeObject(formattedEvent);

                byte[] bytesToSend = bStream.toByteArray();
                FileUtils.writeByteArrayToFile(currentWritingFile, bytesToSend, true);
                bytesWritten = bytesToSend.length;
            }
        }
        return bytesWritten;
    }

    @Override
    public String toString() {
        return "LocalFilePublisherDelegate{" +
                "specification=" + specification +
                ", isPublishing=" + isPublishing +
                ", metricsHandler=" + metricsHandler +
                ", formatterSpecification=" + formatterSpecification +
                ", maxFileSize=" + maxFileSize +
                ", currentWritingFile=" + currentWritingFile +
                ", filename='" + filename + '\'' +
                ", isStringEncoding=" + isStringEncoding +
                ", eventByteSize=" + eventByteSize +
                '}';
    }
}
