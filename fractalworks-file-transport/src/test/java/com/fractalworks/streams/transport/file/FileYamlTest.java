/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.file;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fractalworks.streams.core.dsl.specifications.DataPublishingSpecification;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import com.fractalworks.streams.sdk.formatters.CSVStreamEventFormatter;
import com.fractalworks.streams.sdk.formatters.JsonStreamEventFormatter;
import com.fractalworks.streams.sdk.transport.TransportSpecification;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * File transport unit test for valid and invalid yaml files
 *
 * @author Lyndon Adams
 */
public class FileYamlTest {

    @Test
    public void validFilePublisherYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("validPublishingFileEvents.yaml").getFile());

        FilePublisherSpecificationBuilder builder = new FilePublisherSpecificationBuilder();
        FilePublisherSpecification spec = (FilePublisherSpecification) builder.build(file);
        assertNotNull(spec );
    }

    @Test(expected = InvalidSpecificationException.class)
    public void invalidFilePublishingYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("invalidPublishingFileEvents.yaml").getFile());

        FilePublisherSpecificationBuilder builder = new FilePublisherSpecificationBuilder();
        FilePublisherSpecification spec = (FilePublisherSpecification) builder.build(file);
    }

    @Test
    public void validDataPublisherYamlTest() throws InvalidSpecificationException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("simplePublishingEvents.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        DataPublishingSpecification spec = readMapper.readValue(file, DataPublishingSpecification.class);
        assertNotNull(spec );
    }


    @Test
    public void testWithObject() throws Exception {

        FilePublisherSpecification file = new FilePublisherSpecification();
        file.setFilename("test.csv");
        file.setDirectoryPath("/a/b/c");
        file.setFormatter(new CSVStreamEventFormatter());

        FilePublisherSpecification file2 = new FilePublisherSpecification();
        file2.setFilename("test.json");
        file2.setDirectoryPath("/e/f/g");
        file2.setFormatter(new JsonStreamEventFormatter());

        List<TransportSpecification> specs = new ArrayList<>();
        specs.add(file);
        specs.add(file2);

        DataPublishingSpecification dp = new DataPublishingSpecification();
        dp.setName("testName");
        dp.setSource("mySource");
        dp.setSinks(specs);


        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.getTypeFactory().constructCollectionType(List.class, TransportSpecification.class);
        mapper.registerSubtypes(new NamedType(CSVStreamEventFormatter.class, "csvFormatter"));
        mapper.registerSubtypes(new NamedType(JsonStreamEventFormatter.class, "jsonFormatter"));
        mapper.registerSubtypes(new NamedType(FilePublisherSpecification.class, "file"));


        String yamlString = mapper.writeValueAsString(dp);
        System.out.println(yamlString);

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        DataPublishingSpecification dp2 = readMapper.readValue(yamlString, DataPublishingSpecification.class);

        assertNotNull(dp2 );
    }

}
