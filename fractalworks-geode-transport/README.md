# FractalWorks Apache Geode Transport
[Apache Geode](https://geode.apache.org) provides a database-like consistency model, reliable transaction processing and a shared-nothing architecture to maintain very low latency performance with high concurrency processing.

Joule uses Geode for an in-memory reference data cache and a high performance event publishing model based upon partitioning. 
Both of these approaches have been proven within enterprise production environments for over 15 years.

## Reference Data
For high performance reads local cache technology is a key architectural component. 
Joule embeds a local cache which connects to a distributed cache cluster. 
The embedded cache can be primed using a GII (Get Initial Image) function on startup and receive updates as they occur on the cluster.

## Event publisher
StreamEvents can be published to a distributed cluster ready for downstream asych advanced processing. 

## Local Testing
Supporting scripts, test cases and gradle tasks have been provided to demonstrate the implementation capabilities.

#### Setup environment
 - [Download](https://geode.apache.org/releases/) Apache Geode 1.15.1
 - Build the project `gradle clean build -x test`
 - Copy dependencies and scrips by executing `gradle copyDependencies` task

#### Run test
- DSL `gradle test --tests GeodePlatformDataStoreSpecificationYamlTest`
- Connectivity `gradle test --tests GeodeClusterConnectivityTest`
- Publishing `gradle test --tests GeodeStreamEventPublishingTest`
- Reference data `gradle test --tests GeodeReferenceDataTest`

## Documentation
- [Publisher](https://docs.fractalworks.io/joule/connector-catalog/sinks/geode)
- [Reference Data](https://docs.fractalworks.io/joule/reference-data/sources/apache-geode)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please update tests as appropriate.