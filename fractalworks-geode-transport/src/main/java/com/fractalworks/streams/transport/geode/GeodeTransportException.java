/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import com.fractalworks.streams.sdk.exceptions.transport.TransportException;

/**
 * Runtime exception for Geode transport
 *
 * @author lyndonadams
 */
public class GeodeTransportException extends TransportException {

    public GeodeTransportException() {
    }

    public GeodeTransportException(String message) {
        super(message);
    }

    public GeodeTransportException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeodeTransportException(Throwable cause) {
        super(cause);
    }

    public GeodeTransportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
