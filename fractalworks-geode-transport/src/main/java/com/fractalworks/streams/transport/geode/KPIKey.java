/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import org.apache.geode.cache.EntryOperation;
import org.apache.geode.cache.PartitionResolver;



/**
 * KPI partition resolver using the hash code of the kpi name and partitionValue.
 * For even data distribution provide a unique value for partitionValue
 *
 * @author Lyndon Adams
 */
public class KPIKey implements PartitionResolver<String, Number> {

    String kpiName;
    Object partitionValue;

    public KPIKey() {
    }

    public KPIKey(String kpiName, Object partitionValue) {
        this.kpiName = kpiName;
        this.partitionValue = partitionValue;
    }

    @Override
    public Object getRoutingObject(EntryOperation<String, Number> opDetails) {
        return  kpiName.hashCode() + partitionValue.hashCode();
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public void setPartitionValue(Object partitionValue) {
        this.partitionValue = partitionValue;
    }

    @Override
    public String getName() {
        return kpiName;
    }

}
