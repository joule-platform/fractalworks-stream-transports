/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.client;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.transport.geode.specification.AbstractGeodeSpecification;
import com.fractalworks.streams.transport.geode.serialization.PlatformPDXSerializerFactory;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.client.ClientCache;
import org.apache.geode.cache.client.ClientCacheFactory;
import org.apache.geode.cache.client.ClientRegionShortcut;
import org.apache.geode.cache.query.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Shared Geode client cache factory
 *
 * @author Lyndon Adams
 */
public class GeodeClientCacheFactory {

    private static GeodeClientCacheFactory instance;
    private static ClientCache cache;

    private static final Logger logger = LoggerFactory.getLogger(GeodeClientCacheFactory.class.getName());

    private GeodeClientCacheFactory() {
        //Required
    }

    /**
     * Returns the instance of GeodeClientCacheFactory. This method is synchronized to ensure
     * thread safety.
     *
     * @return the instance of GeodeClientCacheFactory
     */
    public static GeodeClientCacheFactory getInstance() {
        return instance;
    }

    /**
     * Method to retrieve the client cache.
     *
     * @return The client cache instance.
     */
    public ClientCache getClientCache() {
        return cache;
    }

    /**
     * Creates a Geode client cache using the provided specification.
     *
     * @param spec the specification for creating the Geode client cache
     * @return the created Geode client cache
     * @throws IOException if an I/O error occurs while creating the cache
     */
    public static ClientCache createGeodeClientCache(final AbstractGeodeSpecification spec) throws IOException {
        if (instance == null && cache == null) {
            instance = new GeodeClientCacheFactory();
            if (spec.getClientPropertiesPath() != null && !spec.getClientPropertiesPath().isEmpty()) {
                cache = instance.createCacheWithXML(spec.getClientPropertiesPath());
            } else {
                cache = instance.createClientCache(spec).create();
            }

            if( cache.getDistributedSystem().isConnected()){
                logger.info("Single geode client cache instance created and connected to distributed system");
            } else {
                logger.error("Single geode client cache instance creation failure");
            }
        }
        return cache;
    }

    /**
     * Creates a client cache using the given specification.
     *
     * @param spec The geode specification used to configure the client cache.
     * @return The created client cache.
     */
    private ClientCacheFactory createClientCache(AbstractGeodeSpecification spec) {
        PlatformPDXSerializerFactory pf = new PlatformPDXSerializerFactory(spec.getPdxSerializersPath());
        pf.initialize();
        return new ClientCacheFactory()
                .addPoolLocator(spec.getEndpointSpecification().getLocatorAddress(), spec.getEndpointSpecification().getLocatorPort())
                .setPdxSerializer(pf)
                .set("cache-xml-file", "")
                .set("log-file", spec.getName() + ".log")
                .setPoolReadTimeout(10000)
                .setPoolMinConnections(5)
                .setPoolMaxConnections( spec.getEndpointSpecification().getPoolMaxSize())
                .setPoolPRSingleHopEnabled( true);
    }

    /**
     * Creates a client cache with the given Geode properties XML file.
     *
     * @param geodePropertiesPath The path of the Geode properties XML file.
     * @return The created client cache.
     * @throws IOException If an I/O error occurs while reading the properties file.
     */
    private ClientCache createCacheWithXML(final String geodePropertiesPath) throws IOException {
        ClientCache cc;
        try (FileInputStream in = new FileInputStream(geodePropertiesPath)) {
            Properties props = new Properties();
            props.load(in);

            // Create cache
            cc = new ClientCacheFactory(props)
                    .create();
        }
        return cc;
    }

    /**
     * Creates a new region for storing StreamEvent objects in a Geode client cache.
     *
     * @param regionName the name of the region to create
     * @return the created region
     */
    public Region<Object, StreamEvent> createStreamEventRegion(final String regionName) {
        if (cache.getRegion(regionName) == null) {
            cache.<Object, StreamEvent>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                    .create(regionName);
        }
        return cache.getRegion(regionName);
    }

    /**
     * Creates a JSON region in the cache if it does not already exist.
     *
     * @param regionName the name of the region to be created
     * @return the JSON region with the specified name
     */
    public Region<Object, String> createJsonRegion(final String regionName) {
        if (cache.getRegion(regionName) == null) {
            cache.<Object, String>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                    .create(regionName);
        }
        return cache.getRegion(regionName);
    }

    /**
     * Creates a region with the given name if it does not already exist in the cache.
     * The region is created using the client region factory and CACHING_PROXY shortcut.
     *
     * @param regionName the name of the region to create
     * @param <K> the key type of the region
     * @param <V> the value type of the region
     * @return the created or existing region with the given name
     */
    public <K, V> Region<K, V> createRegion(final String regionName) {
        if (cache.getRegion(regionName) == null) {
            cache.<K, V>createClientRegionFactory(ClientRegionShortcut.CACHING_PROXY)
                    .create(regionName);
        }
        return cache.getRegion(regionName);
    }

    /**
     * Returns the QueryService for executing queries on the cache.
     *
     * @param poolname the name of the pool (optional)
     * @return the QueryService
     */
    public QueryService getQueryService(String poolname) {
        if (!poolname.isBlank()) {
            return cache.getQueryService(poolname);
        }
        return cache.getQueryService();
    }
}
