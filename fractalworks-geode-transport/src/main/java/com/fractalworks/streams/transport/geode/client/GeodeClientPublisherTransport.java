/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.client;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.formatters.JsonStreamEventFormatter;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.sdk.util.helpers.ObjectCascadeSearchHelper;
import com.fractalworks.streams.transport.geode.specification.GeodePublisherSpecification;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.client.ClientCache;
import org.apache.geode.cache.query.QueryService;

import java.io.IOException;
import java.util.*;

/**
 * Geode client publishing transport which published either serialised <code>StreamEvent</code> objects
 * or Pdx versions of the object. The client can be configure using the standard gemfire.properties method or via
 * a set of basic configuration variables, for a richer set of configurations the gemfire.properties method should be
 * followed. If a region is provided via the configuration specification a <code>ClientRegionShortcut.CACHING_PROXY</code>
 * region will be created.
 *
 * @author Lyndon Adams
 */
public class GeodeClientPublisherTransport extends AbstractPublisherTransport {

    private ClientCache cache;
    private Region publishingRegion;  //NOSONAR
    private String keyAttribute;
    private String[] searchPath;
    private JsonStreamEventFormatter jsonFormatter;
    private boolean connected = false;

    public GeodeClientPublisherTransport() {
        super();
    }

    public GeodeClientPublisherTransport(GeodePublisherSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        var spec = (GeodePublisherSpecification) getSpecification();
        try {
            cache = GeodeClientCacheFactory.createGeodeClientCache(spec);
            switch (spec.getSerializationType()) {
                case PDX,DATA:
                    publishingRegion = GeodeClientCacheFactory.getInstance().createStreamEventRegion(spec.getRegionName());
                    break;
                case JSON:
                    jsonFormatter = new JsonStreamEventFormatter();
                    jsonFormatter.initialise();
                    publishingRegion = GeodeClientCacheFactory.getInstance().createJsonRegion(spec.getRegionName());
                    break;
                default:
                    break;
            }

            // Set key details
            keyAttribute = spec.getKeyName();

            // Split keyAttribute to component parts only once
            searchPath = keyAttribute.split("\\.");
            connected = true;

            if (logger.isInfoEnabled()) {
                logger.info("Initialization successful");
            }
        } catch (IOException ex) {
            throw new TransportException("Initialization failed.", ex);
        }
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        try {
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                Map<Object, Object> eventMap = new HashMap<>(events.size());
                events.forEach(e -> {
                    Object key = getKey(e);
                    if (key != null) {
                        eventMap.put(key, ((jsonFormatter != null) ? jsonFormatter.format(e) : e));
                    }
                });

                // push events
                if (!eventMap.isEmpty()) {
                    publishingRegion.putAll(eventMap);
                }
                metrics.incrementMetric(Metric.WRITE_STORE, events.size());
            }
        }catch(Exception e){
            metrics.incrementMetric(Metric.PROCESS_FAILED, events.size());
            logger.error(String.format("Failed to publish %d events",events.size() ), e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    /**
     * Retrieves the QueryService instance for the specified pool or the default pool.
     *
     * @param poolname the name of the pool to retrieve the QueryService for, null or empty string to use the default pool
     * @return the QueryService instance
     */
    public QueryService getClusterQueryService(String poolname) {
        return (poolname != null && !poolname.isEmpty()) ? cache.getQueryService(poolname) : cache.getQueryService();
    }

    /**
     * Retrieves the key attribute value from a StreamEvent object.
     *
     * @param e The StreamEvent object from which to retrieve the key attribute value.
     * @return The key attribute value of the StreamEvent object.
     */
    private Object getKey(StreamEvent e) {
        Object key;
        if (keyAttribute.equals("uuid")) {
            key = e.getUUID().toString();
        } else if (keyAttribute.equals(StreamEvent.EVENT_TYPE_FIELD)) {
            key = e.getEventType();
        } else if (keyAttribute.equals(StreamEvent.EVENT_SUBTYPE_FIELD)) {
            key = e.getSubType();
        } else if (keyAttribute.equals(StreamEvent.EVENT_TIME_FIELD)) {
            key = e.getEventTime();
        } else if (keyAttribute.equals(StreamEvent.INGEST_TIME_FIELD)) {
            key = e.getIngestTime();
        } else {
            key = ObjectCascadeSearchHelper.findValue(e, Arrays.asList(searchPath));
            if (key == null) {
                key = e.getUUID().toString();
            }
        }
        return key;
    }

    /**
     * Retrieves the client cache instance.
     *
     * @return the client cache instance
     */
    public ClientCache getCache() {
        return cache;
    }

    /**
     * Check if the client is currently connected.
     *
     * @return true if the client is connected, false otherwise.
     */
    public boolean isConnected() {
        return connected;
    }

    @Override
    public void shutdown() {
        super.shutdown();
        cache.close(false);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeodeClientPublisherTransport that)) return false;
        if (!super.equals(o)) return false;
        return Objects.equals(cache, that.cache) &&
                Objects.equals(publishingRegion, that.publishingRegion) &&
                Objects.equals(keyAttribute, that.keyAttribute) &&
                Arrays.equals(searchPath, that.searchPath);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), cache, publishingRegion, keyAttribute);
        result = 31 * result + Arrays.hashCode(searchPath);
        return result;
    }

    @Override
    public String toString() {
        return "GeodeClientPublisherTransport{" +
                "cache=" + cache +
                ", region=" + publishingRegion +
                ", keyAttribute='" + keyAttribute + '\'' +
                ", searchPath=" + Arrays.toString(searchPath) +
                "} " + super.toString();
    }
}
