/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.client;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.storage.AbstractStore;
import com.fractalworks.streams.sdk.storage.StoreType;
import com.fractalworks.streams.transport.geode.specification.GeodeQueryMountPointSpecification;
import com.fractalworks.streams.transport.geode.serialization.ReferenceDataObjectSerializer;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.query.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import java.util.*;

import static com.fractalworks.streams.transport.geode.server.ReferenceDataCacheLoader.OPERATION;

/**
 * Geode client cache for Read only key/value store
 */
public class GeodeClientStore extends AbstractStore<Collection<ReferenceDataObject>> {

    static {
        new ReferenceDataObjectSerializer();
    }

    private final Logger logger;

    private Region<Object, ReferenceDataObject> kvRegion;
    private String poolName;
    private boolean giiDone = false;

    public GeodeClientStore() {
        logger = LoggerFactory.getLogger(GeodeClientStore.class.getName());
    }

    public GeodeClientStore(GeodeQueryMountPointSpecification spec) {
        super( spec);
        logger = LoggerFactory.getLogger(GeodeClientStore.class.getName());
    }

    @Override
    public void initialize()throws TransportException {
        GeodeQueryMountPointSpecification geodeSpec = (GeodeQueryMountPointSpecification)specification;
        GeodeClientCacheFactory factory = GeodeClientCacheFactory.getInstance();

        kvRegion = factory.createRegion(geodeSpec.getRegionName());
        poolName = kvRegion.getAttributes().getPoolName();

        if( geodeSpec.isGii()){
            executeGIIDataRequest(geodeSpec);
        }
    }

    /**
     * Executes a GII (Get Initial Image) data request for a given GeodeQueryMountPointSpecification.
     *
     * @param spec The GeodeQueryMountPointSpecification object specifying the region and query.
     */
    private void executeGIIDataRequest(GeodeQueryMountPointSpecification spec) throws TransportException {
        try {
            if(logger.isInfoEnabled())
                logger.info(String.format("Getting initial image for %s", spec.getRegionName()));
            var results = getInitialImage(spec.getGii().getQuery());
            if(results.isPresent()){
                if(logger.isInfoEnabled())
                    logger.info(String.format("%d elements loaded in to local %s cache", results.get().size(), spec.getRegionName()));
            } else {
                if(logger.isInfoEnabled())
                    logger.info(String.format("No data loaded in to local %s cache", spec.getRegionName()));
            }
        } catch (StorageException e) {
            throw new TransportException("Failure during client cache initialization", e);
        }
    }

    @Override
    public StoreType getStoreType() {
        return StoreType.KEY_VALUE;
    }

    @Override
    public Optional<Collection<ReferenceDataObject>> query(Object key) {
        Collection<ReferenceDataObject> response = null;
        try {
            var value = kvRegion.get(key);
            if(value!=null) {
                response = Collections.singletonList(value);
            }
            metrics.incrementMetric(Metric.READ_STORE);
        }catch (Exception e){
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            if(logger.isErrorEnabled())
                logger.error("Critical error occurred during query operation", e );
        }
        return Optional.ofNullable(response);
    }

    /**
     * Execute a cluster query and returns a List of results
     *
     * @param query
     * @param params
     * @return
     */
    @Override
    public Optional<Collection<ReferenceDataObject>> query(String query, Object... params) {
        QueryService queryService = GeodeClientCacheFactory.getInstance().getQueryService(poolName);
        Query qry = queryService.newQuery(query);

        Collection<ReferenceDataObject> response = null;
        try {
            SelectResults<ReferenceDataObject> results = (SelectResults<ReferenceDataObject>) qry.execute(params);
            response = results.asList();
            metrics.incrementMetric(Metric.READ_STORE);
        } catch (FunctionDomainException | TypeMismatchException | NameResolutionException | QueryInvocationTargetException e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            logger.error("Failed to execute Geode query", e);
        }
        return Optional.ofNullable(response);
    }

    /**
     * Writes a ReferenceDataObject to the key-value region and increments the WRITE_STORE metric.
     *
     * @param v The ReferenceDataObject to be written.
     */
    public void write(ReferenceDataObject v){
        kvRegion.put(v.getKey(), v, null);
        metrics.incrementMetric(Metric.WRITE_STORE);
    }

    @Override
    public Optional<Collection<ReferenceDataObject>> getInitialImage(String queryString) throws StorageException {
        Collection<ReferenceDataObject> response = null;
        if( !giiDone ) {
            // Prime region if region empty on server
            if (kvRegion.isEmptyOnServer()) {
                kvRegion.get(0, OPERATION.PRIME);  //NOSONAR
            }
            giiDone = true;
        }

        // Run a user query
        if (queryString != null && !queryString.isBlank()) {
            return query(queryString, new Object[]{});  // NOSONAR
        }

        // Get all
        Map<Object,ReferenceDataObject> rs = kvRegion.getAll(kvRegion.keySetOnServer());
        if(rs != null){
            response = rs.values();
        }
        return Optional.ofNullable(response);
    }
}
