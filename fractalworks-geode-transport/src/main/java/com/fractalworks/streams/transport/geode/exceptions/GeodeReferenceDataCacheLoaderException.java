package com.fractalworks.streams.transport.geode.exceptions;

/**
 * Reference data cache loader runtime exception
 * @author Lyndon Adams
 */
public class GeodeReferenceDataCacheLoaderException extends RuntimeException{
    public GeodeReferenceDataCacheLoaderException() {
    }

    public GeodeReferenceDataCacheLoaderException(String message) {
        super(message);
    }

    public GeodeReferenceDataCacheLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeodeReferenceDataCacheLoaderException(Throwable cause) {
        super(cause);
    }

    public GeodeReferenceDataCacheLoaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
