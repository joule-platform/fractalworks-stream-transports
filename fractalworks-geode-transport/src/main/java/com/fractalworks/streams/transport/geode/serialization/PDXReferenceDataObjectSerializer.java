/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.serialization;

import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import org.apache.geode.pdx.PdxReader;
import org.apache.geode.pdx.PdxWriter;

import java.util.Map;
import java.util.Set;

/**
 * PDX ReferenceDataObject serialization type
 *
 * @author Lyndon Adams
 */
public class PDXReferenceDataObjectSerializer implements PDXDomainSerializer {

    public PDXReferenceDataObjectSerializer(){
        // Required
    }

    @Override
    public boolean toData(Object o, PdxWriter out) {
        if (o instanceof ReferenceDataObject r) {
            out.writeString("key", (String)r.getKey());
            out.markIdentityField("key");
            out.markIdentityField( r.getKey().toString());
            out.writeObject("data", r.getData());
            out.writeObject("entryset", r.entrySet());
        }
        return false;
    }

    @Override
    public Object fromData(Class<?> clazz, PdxReader in) {
        if (ReferenceDataObject.class.isAssignableFrom(clazz)) {
            String key = in.readString("key");
            Object data = in.readObject("data");
            Set<Map.Entry<String,Object>> entrySet = (Set<Map.Entry<String,Object>>)in.readObject("entryset");

            ReferenceDataObject o = new ReferenceDataObject();
            o.setKey(key);
            o.setData(data);
            entrySet.forEach(e -> o.put(e.getKey(), e.getValue()));

            return o;
        }
        return null;
    }

    @Override
    public Class<?> getDomainType() {
        return ReferenceDataObject.class;
    }
}
