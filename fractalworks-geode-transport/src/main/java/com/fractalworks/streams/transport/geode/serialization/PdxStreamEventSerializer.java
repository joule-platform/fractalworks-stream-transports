/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.serialization;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import org.apache.geode.pdx.PdxReader;
import org.apache.geode.pdx.PdxWriter;

import java.util.Map;
import java.util.UUID;

/**
 * Geode PDX event for StreamEvent
 *
 * @author Lyndon Adams
 */
public class PdxStreamEventSerializer implements PDXDomainSerializer {

    public PdxStreamEventSerializer() {
        // Required
    }

    @Override
    public boolean toData(Object o, PdxWriter out) {
        if (o instanceof StreamEvent event) {
            out.writeString("uuid", event.getUUID().toString());
            out.writeString("eventType", event.getEventType());
            out.writeLong("ingestTime", event.getIngestTime());
            out.writeLong("eventTime", event.getEventTime());
            out.writeObject("subType", event.getSubType());
            out.writeObject("dictionary", event.getDictionary());
            return true;
        }
        return false;
    }

    @Override
    public Object fromData(Class<?> clazz, PdxReader in) {
        if (StreamEvent.class.isAssignableFrom(clazz)) {
            StreamEvent event = new StreamEvent();
            event.setUuid(UUID.fromString(in.readString("uuid")));
            event.setEventType(in.readString("eventType"));
            event.setIngestTime(in.readLong("ingestTime"));
            event.setEventTime(in.readLong("eventTime"));
            event.setSubType(in.readObject("subType"));

            Object m = in.readObject("dictionary");
            if (m instanceof Map) {
                Map<String, Object> dictionary = (Map<String, Object>) m;
                event.setDictionary(dictionary);
            }
            return event;
        }
        return null;
    }

    @Override
    public Class<?> getDomainType() {
        return StreamEvent.class;
    }
}
