/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.serialization;

import com.fractalworks.streams.core.data.Tuple;
import org.apache.geode.pdx.PdxReader;
import org.apache.geode.pdx.PdxWriter;

/**
 * Geode PDX event for {@link Tuple}
 *
 * @author Lyndon Adams
 */
public class PdxTupleSerializer implements PDXDomainSerializer {

    public PdxTupleSerializer() {
        // Required
    }

    /**
     * <code>{@link Tuple}</code> serializer
     *
     * @param o
     * @param out
     * @return
     */
    @Override
    public boolean toData(Object o, PdxWriter out) {
        if (o instanceof Tuple tuple) {
            out.writeObject("x", tuple.getX());
            out.writeObject("y", tuple.getY());
            return true;
        }
        return false;
    }

    /**
     * Converts the data from PDX format to the specified class.
     *
     * @param clazz the class to which the data will be converted
     * @param in the PdxReader object used to read the data
     * @return the converted object or null if the class is not supported
     */
    @Override
    public Object fromData(Class<?> clazz, PdxReader in) {
        if (Tuple.class.isAssignableFrom(clazz)) {
            Tuple<Object,Object> t = new Tuple<>();
            t.setX(in.readObject("x"));
            t.setY(in.readObject("y"));
            return t;
        }
        return null;
    }

    @Override
    public Class<?> getDomainType() {
        return Tuple.class;
    }
}
