/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.serialization;

import org.apache.geode.pdx.PdxReader;
import org.apache.geode.pdx.PdxSerializer;
import org.apache.geode.pdx.PdxWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Reflective helper class to serialize domain classes using a single {@link PdxSerializer} instance.
 *
 * @author Lyndon Adams
 */
public class PlatformPDXSerializerFactory implements PdxSerializer {

    private final Logger logger = LoggerFactory.getLogger(PlatformPDXSerializerFactory.class);
    private final String pdxPropertiesPath;
    Map<String, PDXDomainSerializer> serializers;

    public PlatformPDXSerializerFactory(String propertiesPath) {
        this.pdxPropertiesPath = propertiesPath;
    }

    public void initialize() {

        Properties properties = new Properties();

        if( pdxPropertiesPath != null) {
            try (InputStream input = new FileInputStream(pdxPropertiesPath)) {
                // load a properties file
                properties.load(input);
            } catch (IOException ex) {
                logger.error(String.format("Unable load %s pdx properties file", pdxPropertiesPath), ex);
            }
        }

        // Add core platform data types
        properties.put("PdxStreamEventSerializer", "com.fractalworks.streams.transport.geode.serialization.PdxStreamEventSerializer");
        properties.put("PdxTupleSerializer", "com.fractalworks.streams.transport.geode.serialization.PdxTupleSerializer");
        properties.put("PDXReferenceDataObjectSerializer", "com.fractalworks.streams.transport.geode.serialization.PDXReferenceDataObjectSerializer");

        serializers = new HashMap<>(properties.size());

        // Reflect and load all serializers
        properties.forEach((clazzName, fullPath) -> {
            try {
                Class<?> clazz = Class.forName((String) fullPath);
                Object obj = clazz.getDeclaredConstructor().newInstance();
                if (obj instanceof PDXDomainSerializer pd) {
                    serializers.put(pd.getDomainType().getName(), pd);
                }
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                logger.error(String.format("Unable to load %s PDXDomainSerializer class", clazzName));
            }
        });
    }

    @Override
    public boolean toData(Object o, PdxWriter out) {
        String name = o.getClass().getName();
        if (serializers.containsKey(name)) {
            return serializers.get(name).toData(o, out);
        } else if( logger.isErrorEnabled()) {
            logger.error(String.format("Unable to serialize %s . Corresponding PDXDomainSerializer class not found", name));
        }
        return false;
    }

    @Override
    public Object fromData(Class<?> clazz, PdxReader in) {
        String name = clazz.getName();
        if (serializers.containsKey(name)) {
            return serializers.get(name).fromData(clazz, in);
        }
        if( logger.isErrorEnabled()) {
            logger.error(String.format("Unable to deserialize %s . Corresponding PDXDomainSerializer class not found", name));
        }
        return null;
    }
}
