/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.serialization;

import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import org.apache.geode.DataSerializer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;

/**
 * Geode {@link ReferenceDataObject} data serializer
 *
 * @author Lyndon Adams
 */
public class ReferenceDataObjectSerializer extends DataSerializer {

    public static final int TYPEID = 9999;

    static {
        DataSerializer.register(ReferenceDataObjectSerializer.class);
    }

    public ReferenceDataObjectSerializer() {
        // Required
    }

    @Override
    public Class[] getSupportedClasses() {
        return new Class[]{ReferenceDataObject.class};
    }

    @Override
    public int getId() {
        return TYPEID;
    }

    @Override
    public boolean toData(Object o, DataOutput out) throws IOException {
        if (o instanceof ReferenceDataObject r) {
            HashMap<String,Object> map = new HashMap<>(r);
            DataSerializer.writeHashMap(map,out);
            DataSerializer.writeObject(r.getKey(), out);
            DataSerializer.writeObject(r.getData(), out);
            return true;
        }
        return false;
    }

    @Override
    public Object fromData(DataInput in) throws IOException, ClassNotFoundException {
        HashMap<String,Object> map =  DataSerializer.readHashMap(in);
        Object key = DataSerializer.readObject(in);
        Object data = DataSerializer.readObject(in);
        return new ReferenceDataObject(key, data, map);
    }
}
