/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.serialization;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import org.apache.geode.DataSerializer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Geode <code>StreamEvent</code> data serializer
 *
 * @author Lyndon Adams
 */
public class StreamEventDataSerializer extends DataSerializer {

    public static final int TYPEID = 1234;

    static {
        DataSerializer.register(StreamEventDataSerializer.class);
    }

    public StreamEventDataSerializer() {
        // Required
    }

    @Override
    public Class[] getSupportedClasses() {
        return new Class[]{StreamEvent.class};
    }

    @Override
    public int getId() {
        return TYPEID;
    }

    @Override
    public boolean toData(Object o, DataOutput out) throws IOException {
        if (o instanceof StreamEvent event) {
            out.writeUTF(event.getUUID().toString());
            out.writeUTF(event.getEventType());
            out.writeLong(event.getIngestTime());
            out.writeLong(event.getEventTime());
            DataSerializer.writeObject(event.getSubType(), out);
            DataSerializer.writeHashMap(event.getDictionary(), out);
            return true;
        }
        return false;
    }

    @Override
    public Object fromData(DataInput in) throws IOException, ClassNotFoundException {
        StreamEvent event = new StreamEvent();
        event.setUuid(UUID.fromString(in.readUTF()));
        event.setEventType(in.readUTF());
        event.setIngestTime(in.readLong());
        event.setEventTime(in.readLong());
        event.setSubType(DataSerializer.readObject(in));

        Map<String, Object> dictionary = DataSerializer.readHashMap(in);
        event.setDictionary(dictionary);
        return event;
    }
}
