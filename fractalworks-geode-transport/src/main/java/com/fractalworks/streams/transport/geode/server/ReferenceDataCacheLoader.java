/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.server;

import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.transport.geode.GeodeTransportException;
import com.fractalworks.streams.transport.geode.exceptions.GeodeReferenceDataCacheLoaderException;
import com.fractalworks.streams.transport.geode.serialization.ReferenceDataObjectSerializer;
import com.fractalworks.streams.transport.rdms.RDMSQueryFactory;
import com.fractalworks.streams.transport.rdms.RDMSQueryHelper;
import org.apache.geode.cache.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.*;

/**
 * Geode serverside cache loader
 * </P>
 * Directions
 * - Install CacheLoader on to required region with the required properties file
 * - Able to perform a GII on a get using an OPERATION callback argument from caller
 * </P>
 * @author Lyndon Adams
 */
public class ReferenceDataCacheLoader<K> implements CacheLoader<K, ReferenceDataObject> {

    public enum OPERATION {PRIME, LOAD}

    // Intialize serializers
    static {
        new ReferenceDataObjectSerializer();
    }

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private RDMSQueryHelper rdmsQueryHelper;

    private String query;
    private String primingQuery;

    private String keyField;

    private boolean primeRegion = false;
    private boolean isprimed = true;

    public static final int DEFAULT_BATCH_SIZE = 700;
    private int batchSize;

    @Override
    public ReferenceDataObject load(LoaderHelper<K, ReferenceDataObject> helper) throws CacheLoaderException {
        ReferenceDataObject referenceDataObject = null;
        OPERATION ops = ( helper.getArgument() != null ) ? (OPERATION) helper.getArgument() : OPERATION.LOAD;
        if (ops == OPERATION.PRIME && primeRegion && !isprimed) {
            Object firstKey = primeRegion(helper.getRegion());
            referenceDataObject = helper.getRegion().get(firstKey);
        } else if (ops == OPERATION.LOAD) {
            Object[] params = new Object[]{helper.getKey()};
            List<ReferenceDataObject> data = rdmsQueryHelper.queryReferenceData(keyField, query, params);

            if (!data.isEmpty()) {
                referenceDataObject = data.get(0);
            }
        }
        return referenceDataObject;
    }

    /**
     * Primes the given region with reference data.
     *
     * @param region The region to prime with data.
     * @return The key of the first object added to the region, or null if no objects were added.
     */
    public Object primeRegion(final Region<K, ReferenceDataObject> region){
        Object firstKey = null;
        try {
            if (logger.isInfoEnabled()) {
                logger.info(String.format("Starting priming region %s with data", region.getName()));
            }
            Collection<ReferenceDataObject> data = rdmsQueryHelper.queryReferenceData(keyField, primingQuery, null);
            Map<K, ReferenceDataObject> batch = new HashMap<>();
            int i = 0;

            for (ReferenceDataObject object : data) {
                if( firstKey == null){
                    firstKey = object.getKey();
                }

                batch.put((K) object.getKey(), object);
                if (++i > batchSize) {
                    region.putAll(batch);
                    i = 0;
                    batch.clear();
                }
            }

            // Add final batch
            if ( !batch.isEmpty()) {
                region.putAll(batch);
            }

            if (logger.isInfoEnabled()) {
                logger.info(String.format("Completed priming region %s with data", region.getName()));
            }
        }catch(Exception e){
            logger.error("Failed in region priming ", e);
        }
        return firstKey;
    }

    /**
     * Loads user properties from a given file path.
     *
     * @param path The path of the properties file.
     * @return The loaded Properties object.
     */
    private Properties loadUserProperties(final String path){
        Properties properties = new Properties();

        // Directly load from file system
        try (InputStream input = new FileInputStream(path)) {
            properties.load(input);
            if(logger.isInfoEnabled()) {
                logger.info(String.format("Loaded %s from filesystem", path));
            }
        } catch (FileNotFoundException ex) {
            logger.warn( String.format("Unable to find %s in filesystem", path));
        } catch (IOException e) {
            logger.error("Failure during filesystem properties load", e);
        }

        if(properties.isEmpty()){
            // First try classpath
            try (InputStream input = ClassLoader.getSystemResourceAsStream(path)) {
                if (input != null) {
                    properties.load(input);
                    if( logger.isInfoEnabled()) {
                        logger.info(String.format("Loaded %s from classpath", path));
                    }
                } else {
                    if( logger.isWarnEnabled()) {
                        logger.warn(String.format("Unable to find %s on classpath", path));
                    }
                }
            }catch(IOException ex) {
                logger.error("Failure during classpath properties load", ex);
            }
        }
        return properties;
    }

    /**
     * Initializes the database connection based on the given properties.
     *
     * @param dataSourcePropertiesPath The path to the properties file containing the database connection properties.
     * @throws GeodeTransportException if the database fails to connect.
     */
    private void initDatabaseConnection(String dataSourcePropertiesPath) throws GeodeTransportException {
        // Load properties file from passed path
        Properties dataSourceProperties = loadUserProperties(dataSourcePropertiesPath);
        if( !dataSourceProperties.isEmpty() ) {
            logger.info("Setting up database connectivity");
            try {
                rdmsQueryHelper = RDMSQueryFactory.getRDMSQueryHelper( dataSourceProperties);

                if(logger.isInfoEnabled()) {
                    logger.info("Database successfully connected");
                }
            } catch (Exception ex) {
                throw new GeodeTransportException("Database failed to connect", ex);
            }
        }
    }

    /**
     * Initializes the loaded properties based on the provided Properties object.
     * Throws InvalidParameterException if required properties are missing or blank.
     *
     * @param loadedProperties The Properties object containing the loaded properties.
     * @throws InvalidParameterException if the required properties are missing or blank.
     */
    private void initLoadedProperties(Properties loadedProperties){
        query = loadedProperties.getProperty("query");
        if (query == null || query.isBlank()) {
            throw new InvalidParameterException("Missing query property");
        }
        keyField = loadedProperties.getProperty("keyfield");
        if (keyField == null || keyField.isBlank()) {
            throw new InvalidParameterException("Missing keyfield property");
        }

        String bSize = loadedProperties.getProperty("batchsize");
        batchSize = (bSize != null && !bSize.isBlank()) ? Integer.valueOf(bSize) : DEFAULT_BATCH_SIZE;

        // Prime region with data
        primingQuery = loadedProperties.getProperty("primingquery");

        primeRegion = Boolean.valueOf(loadedProperties.getProperty("primeregion", "false"));
        if (primeRegion && primingQuery != null && !primingQuery.isBlank() ) {
            isprimed = false;
        } else {
            primeRegion = false;
        }
    }

    @Override
    public void initialize(Cache cache, Properties properties) {
        logger = LoggerFactory.getLogger(this.getClass().getName());

        // Setup database connection
        setupDatabaseConnection(properties);

        Properties loadedProperties = new Properties();
        // Load properties file from passed path
        if(properties.containsKey("propertiesPath") ){
            String propertiesPath = properties.getProperty("propertiesPath");
            if( propertiesPath != null && !propertiesPath.isBlank()){
                loadedProperties = loadUserProperties(propertiesPath);
            }
        }

        if( !loadedProperties.isEmpty()) {
            initLoadedProperties(loadedProperties);
            if( logger.isInfoEnabled()) {
                logger.info("CacheLoader ready");
            }
        } else {
            logger.error("Failed to load propertiesPath");
        }
    }

    /**
     * Sets up the database connection based on the given properties.
     *
     * @param properties the properties to configure the database connection
     * @throws GeodeReferenceDataCacheLoaderException if there is an error in the cache loader
     * @throws InvalidParameterException             if the dataSourceProperties parameter is missing
     */
    private void setupDatabaseConnection(Properties properties) throws GeodeReferenceDataCacheLoaderException, InvalidParameterException {
        if(properties.containsKey("dataSourceProperties") ){
            String dataSourcePropertiesPath = properties.getProperty("dataSourceProperties");
            if( dataSourcePropertiesPath != null && !dataSourcePropertiesPath.isBlank()){
                try {
                    initDatabaseConnection(dataSourcePropertiesPath);
                } catch (GeodeTransportException e) {
                    throw new GeodeReferenceDataCacheLoaderException(e);
                }
            }
        } else {
            throw new InvalidParameterException("Missing dataSourceProperties");
        }
    }
}
