/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;



import java.util.Objects;

/**
 * Base Geode class for client cache implementations
 *
 * @author Lyndon Adams
 */
public abstract class AbstractGeodeSpecification extends AbstractTransportSpecification {

    protected EndpointSpecification endpointSpecification;
    protected String clientPropertiesPath;
    protected String pdxSerializersPath;

    public EndpointSpecification getEndpointSpecification() {
        return endpointSpecification;
    }

    @JsonProperty(value = "connection", required = true)
    public void setEndpointSpecification(EndpointSpecification endpointSpecification) {
        this.endpointSpecification = endpointSpecification;
    }

    public String getPdxSerializersPath() {
        return pdxSerializersPath;
    }

    @JsonProperty(value = "pdxSerializersPath")
    public void setPdxSerializersPath(String pdxserializersPath) {
        this.pdxSerializersPath = pdxserializersPath;
    }

    public String getClientPropertiesPath() {
        return clientPropertiesPath;
    }

    @JsonProperty(value = "properties")
    public void setClientPropertiesPath(String clientPropertiesPath) {
        this.clientPropertiesPath = clientPropertiesPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractGeodeSpecification that = (AbstractGeodeSpecification) o;
        return Objects.equals(endpointSpecification, that.endpointSpecification) && Objects.equals(clientPropertiesPath, that.clientPropertiesPath) && Objects.equals(pdxSerializersPath, that.pdxSerializersPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), endpointSpecification, clientPropertiesPath, pdxSerializersPath);
    }
}
