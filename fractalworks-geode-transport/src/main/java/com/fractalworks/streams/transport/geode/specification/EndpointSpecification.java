package com.fractalworks.streams.transport.geode.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;


/**
 * Geode connection specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "connection")
public class EndpointSpecification {

    protected String locatorAddress = "localhost";
    protected int locatorPort = 10334;
    protected String poolName = "fractalPool";
    protected int poolMaxSize = 20;
    protected int poolReadTimeout = 5000;

    public EndpointSpecification(){
        // Required
    }

    @JsonProperty(value = "locator address")
    public String getLocatorAddress() {
        return locatorAddress;
    }

    public EndpointSpecification setLocatorAddress(String locatorAddress) {
        this.locatorAddress = locatorAddress;
        return this;
    }

    public int getLocatorPort() {
        return locatorPort;
    }

    @JsonProperty(value = "locator port")
    public void setLocatorPort(int locatorPort) {
        this.locatorPort = locatorPort;
    }


    public String getPoolName() {
        return poolName;
    }

    @JsonProperty(value = "pool name")
    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public int getPoolMaxSize() {
        return poolMaxSize;
    }

    @JsonProperty(value = "pool maxSize")
    public void setPoolMaxSize(int poolMaxSize) {
        this.poolMaxSize = poolMaxSize;
    }

    public int getPoolReadTimeout() {
        return poolReadTimeout;
    }

    @JsonProperty(value = "pool readTimeout")
    public void setPoolReadTimeout(int poolReadTimeout) {
        this.poolReadTimeout = poolReadTimeout;
    }

    /**
     * Validates the endpoint specification.
     *
     * @throws InvalidSpecificationException if any of the required fields are missing or have invalid values.
     */
    public void validate() throws InvalidSpecificationException {
        if(locatorAddress == null || locatorAddress.isBlank())
            throw new InvalidSpecificationException("locator address must be provided");

        if(locatorPort <= 1000)
            throw new InvalidSpecificationException("locator port must be greater than 1000");

        if(poolName == null || poolName.isBlank())
            throw new InvalidSpecificationException("pool name must be provided");

        if(poolMaxSize < 1)
            throw new InvalidSpecificationException("pool maxSize be greater than 0");


        if(poolReadTimeout < 5000)
            throw new InvalidSpecificationException("pool readTimeout must be greater than 5000");

    }
}

