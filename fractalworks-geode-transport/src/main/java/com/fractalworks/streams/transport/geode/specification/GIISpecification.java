package com.fractalworks.streams.transport.geode.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.Specification;

/**
 * Get initial image specification
 *
 * @author Lyndon Adams
 */
public class GIISpecification implements Specification {

    private boolean enabled = true;
    private String query;

    public GIISpecification() {
        // REQUIRED
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @JsonProperty(value = "enabled", required = true)
    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getQuery() {
        return query;
    }

    @JsonProperty(value = "query", required = false)
    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if(query == null || query.isBlank())
            throw new InvalidSpecificationException("query must be provided");
    }
}
