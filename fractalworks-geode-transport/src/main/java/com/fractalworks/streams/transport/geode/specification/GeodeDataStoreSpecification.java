/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreSpecification;
import com.fractalworks.streams.transport.geode.GeodeTransportException;
import com.fractalworks.streams.transport.geode.client.GeodeClientCacheFactory;
import com.fractalworks.streams.transport.geode.client.GeodeClientStore;
import com.google.common.base.Objects;


import java.io.IOException;
import java.util.Map;

/**
 * Single specification to define multiple regions for a single cluster configuration
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "geode stores")
public class GeodeDataStoreSpecification extends AbstractGeodeSpecification implements DataStoreSpecification {

    private Map<String, GeodeQueryMountPointSpecification> stores;

    public GeodeDataStoreSpecification() {
        // Required
    }

    @Override
    public void startup() throws TransportException {
        try {
            GeodeClientCacheFactory.createGeodeClientCache(this );
        } catch (IOException e) {
            throw new GeodeTransportException(e);
        }
    }

    @Override
    public void close() {
        GeodeClientCacheFactory instance = GeodeClientCacheFactory.getInstance();
        if( instance!= null ) instance.getClientCache().close();
    }

    @JsonProperty(value = "stores", required = true)
    public void setStores(Map<String, GeodeQueryMountPointSpecification> stores) {
        this.stores = stores;
    }

    public Map<String, GeodeQueryMountPointSpecification> getStores() {
        return stores;
    }

    @Override
    public Class<? extends Store> getComponentClass() {
        return GeodeClientStore.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GeodeDataStoreSpecification that = (GeodeDataStoreSpecification) o;
        return Objects.equal(stores, that.stores);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), stores);
    }

    @Override
    public void validate() throws InvalidSpecificationException {
       if( stores == null || stores.isEmpty()){
           throw new InvalidSpecificationException("stores must be provided and not empty");
       }

       for(GeodeQueryMountPointSpecification store : stores.values()){
           store.validate();
       }
    }
}
