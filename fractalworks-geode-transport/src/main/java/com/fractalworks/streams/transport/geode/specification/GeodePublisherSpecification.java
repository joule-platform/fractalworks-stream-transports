/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.geode.SerializationType;
import com.fractalworks.streams.transport.geode.client.GeodeClientPublisherTransport;
import com.google.common.base.Objects;




/**
 * Geode publishing specification using Geode defaults where possible
 * <p>
 * Defaults:
 * - locatorAddress = "localhost"
 * - locatorPort = 10334
 * - regionName = "streamEvents"
 * - keyName = "uuid"
 * - serializationType = SerializationType.PDX
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "geode publisher")
public class GeodePublisherSpecification extends AbstractGeodeSpecification {

    private SerializationType serializationType = SerializationType.PDX;
    private String keyName = "uuid";

    private String regionName = "streamEvents";

    public GeodePublisherSpecification() {
        // Required
    }

    public static GeodePublisherSpecificationBuilder builder(){
        return new GeodePublisherSpecificationBuilder();
    }

    public String getKeyName() {
        return keyName;
    }

    @JsonProperty(value = "key")
    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getRegionName() {
        return regionName;
    }

    @JsonProperty(value = "region")
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public SerializationType getSerializationType() {
        return serializationType;
    }

    @JsonProperty(value = "serialization type")
    public void setSerializationType(SerializationType serializationType) {
        this.serializationType = serializationType;
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return GeodeClientPublisherTransport.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GeodePublisherSpecification that = (GeodePublisherSpecification) o;
        return serializationType == that.serializationType && Objects.equal(keyName, that.keyName) && Objects.equal(regionName, that.regionName);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), serializationType, keyName, regionName);
    }
}
