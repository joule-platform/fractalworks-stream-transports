package com.fractalworks.streams.transport.geode.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.geode.SerializationType;

/**
 * Builder for GeodePublisherSpecification
 *
 * @author Lyndon Adams
 */
public class GeodePublisherSpecificationBuilder extends AbstractSpecificationBuilder<GeodePublisherSpecification> {

    private SerializationType serializationType = SerializationType.PDX;
    private String keyField = "uuid";
    private String regionName = "streamEvents";

    private EndpointSpecification endpointSpecification;
    private String clientPropertiesPath;
    private String pdxSerializersPath;

    public GeodePublisherSpecificationBuilder() {
        specificationClass = GeodePublisherSpecification.class;
    }

    public GeodePublisherSpecificationBuilder setSerializationType(SerializationType serializationType) {
        this.serializationType = serializationType;
        return this;
    }

    public GeodePublisherSpecificationBuilder setKeyField(String keyField) {
        this.keyField = keyField;
        return this;
    }

    public GeodePublisherSpecificationBuilder setRegionName(String regionName) {
        this.regionName = regionName;
        return this;
    }

    public GeodePublisherSpecificationBuilder setEndpointSpecification(EndpointSpecification endpointSpecification) {
        this.endpointSpecification = endpointSpecification;
        return this;
    }

    public GeodePublisherSpecificationBuilder setClientPropertiesPath(String clientPropertiesPath) {
        this.clientPropertiesPath = clientPropertiesPath;
        return this;
    }

    public GeodePublisherSpecificationBuilder setPdxSerializersPath(String pdxSerializersPath) {
        this.pdxSerializersPath = pdxSerializersPath;
        return this;
    }

    @Override
    public GeodePublisherSpecification build() throws InvalidSpecificationException {
        GeodePublisherSpecification spec = new GeodePublisherSpecification();
        spec.setEndpointSpecification(endpointSpecification);
        spec.setKeyName(keyField);
        spec.setRegionName(regionName);
        spec.setSerializationType(serializationType);
        spec.setPdxSerializersPath(pdxSerializersPath);
        spec.setClientPropertiesPath(clientPropertiesPath);
        spec.validate();
        return spec;
    }
}
