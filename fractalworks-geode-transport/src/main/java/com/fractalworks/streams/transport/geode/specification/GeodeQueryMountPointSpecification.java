/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreMountPointSpecification;
import com.fractalworks.streams.transport.geode.SerializationType;
import com.fractalworks.streams.transport.geode.client.GeodeClientStore;



/**
 * GemFire region configuration
 *   Example
 *   -------
 *   stores:
 *     customers:
 *         region: customer
 *         keyClass : java.lang.Integer
 *     bundles:
 *         region: bundles
 *         keyClass: java.lang.Integer
 *
 * @author Lyndon Adams
 */
public class GeodeQueryMountPointSpecification implements DataStoreMountPointSpecification {

    protected String regionName = "streamEvents";
    private Class<?> keyClass = String.class;
    private Class<?> valueClass = ReferenceDataObject.class;
    private SerializationType deserializationType = SerializationType.PDX;
    private boolean enabled = true;
    private GIISpecification gii;

    public GeodeQueryMountPointSpecification() {
        // Required
    }

    public String getRegionName() {
        return regionName;
    }

    @JsonProperty(value = "region")
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Class<?> getValueClass() {
        return valueClass;
    }

    @JsonProperty(value = "value class")
    public void setValueClass(Class<?> valueClass) {
        this.valueClass = valueClass;
    }

    public Class<?> getKeyClass() {
        return keyClass;
    }

    @JsonProperty(value = "key class", required = true)
    public void setKeyClass(Class<?> keyClass) {
        this.keyClass = keyClass;
    }

    public SerializationType getDeserializationType() {
        return deserializationType;
    }

    @JsonProperty(value = "deserialization type")
    public void setDeserializationType(SerializationType serializationType) {
        this.deserializationType = serializationType;
    }

    public boolean isGii() {
        return (gii!=null);
    }

    @JsonProperty(value = "gii")
    public void setGii(GIISpecification gii) {
        this.gii = gii;
    }

    public GIISpecification getGii() {
        return gii;
    }

    @Override
    public Class<? extends Store> getComponentClass() {
        return GeodeClientStore.class;
    }

    @Override
    public String getName() {
        return regionName;
    }

    @Override
    @JsonIgnore
    public void setName(String name) {
        regionName = name;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    @JsonIgnore
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if( regionName == null || regionName.isBlank()){
            throw new InvalidSpecificationException("region must be provided");
        }

        if( keyClass == null){
            throw new InvalidSpecificationException("key class must be provided");
        }

        if( valueClass == null){
            throw new InvalidSpecificationException("value class must be provided");
        }

        if( deserializationType == null){
            throw new InvalidSpecificationException("deserialization type must be provided");
        }
    }
}
