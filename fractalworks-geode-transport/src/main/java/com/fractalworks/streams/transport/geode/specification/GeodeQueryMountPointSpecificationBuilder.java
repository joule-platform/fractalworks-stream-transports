/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;


import java.util.Map;

/**
 * Builder for GeodePlatformDataStoreSpecification which includes one or many data regions
 *
 * @author Lyndon Adams
 */
public class GeodeQueryMountPointSpecificationBuilder extends AbstractSpecificationBuilder<GeodeDataStoreSpecification> {

    private Map<String, GeodeQueryMountPointSpecification> stores;

    public GeodeQueryMountPointSpecificationBuilder() {
        specificationClass = GeodeDataStoreSpecification.class;
    }

    public GeodeQueryMountPointSpecificationBuilder setStores(Map<String, GeodeQueryMountPointSpecification> stores) {
        this.stores = stores;
        return this;
    }

    @Override
    public GeodeDataStoreSpecification build() throws InvalidSpecificationException {
        GeodeDataStoreSpecification spec = new GeodeDataStoreSpecification();
        spec.setStores( stores);
        spec.validate();
        return spec;
    }
}
