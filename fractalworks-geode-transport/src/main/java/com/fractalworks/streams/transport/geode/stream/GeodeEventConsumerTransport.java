/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.stream;


import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.channels.subscribable.PublishSubscribeChannel;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import org.apache.geode.cache.asyncqueue.AsyncEvent;
import org.apache.geode.cache.asyncqueue.AsyncEventListener;


import java.util.List;

/**
 * Geode consumer which receives events from an attached region
 *
 * @author Lyndon Adams
 */
public class GeodeEventConsumerTransport extends AbstractConsumerTransport implements AsyncEventListener {

    // Event pipe
    private PublishSubscribeChannel publishSubscribeChannel;

    public void setPublishSubscribeChannel(PublishSubscribeChannel publishSubscribeChannel) {
        this.publishSubscribeChannel = publishSubscribeChannel;
    }

    @Override
    public boolean processEvents(List<AsyncEvent> events) {
        if (publishSubscribeChannel != null) {
            for (AsyncEvent<?,?> e : events) {
                StreamEvent event = (StreamEvent) e.getDeserializedValue();
                publishSubscribeChannel.publish(event, new Context());
            }
            return true;
        }
        return false;
    }

    @Override
    public void shutdown() {
        publishSubscribeChannel = null;
    }

    @Override
    public void close() {
        shutdown();
    }
}
