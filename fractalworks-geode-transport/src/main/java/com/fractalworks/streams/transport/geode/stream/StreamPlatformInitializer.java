/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode.stream;

import org.apache.geode.cache.Cache;
import org.apache.geode.cache.Declarable;

import java.util.Properties;

/**
 * This class is an initializer for a stream platform. It implements the {@link Declarable} interface,
 * which requires the implementation of the {@link #initialize(Cache, Properties)} method.
 * <p>
 * The {@link #initialize(Cache, Properties)} method is used to initialize the stream platform by setting
 * the cache and properties.
 * <p>
 * The StreamPlatformInitializer class provides getter and setter methods for the cache and properties,
 * allowing them to be accessed and modified externally. The setter methods also have a fluent interface,
 * returning an instance of the StreamPlatformInitializer class to allow method chaining.
 */
public class StreamPlatformInitializer implements Declarable {

    private Cache cache;
    private Properties properties;

    @Override
    public void initialize(Cache cache, Properties properties) {
        this.cache = cache;
        this.properties = properties;
    }

    /**
     * Retrieves the cache associated with the StreamPlatformInitializer.
     * @return The cache associated with the StreamPlatformInitializer.
     */
    public Cache getCache() {
        return cache;
    }

    /**
     * Sets the cache for the stream platform initializer.
     *
     * @param cache The cache to set.
     * @return The stream platform initializer instance.
     */
    public StreamPlatformInitializer setCache(Cache cache) {
        this.cache = cache;
        return this;
    }

    /**
     * Returns the properties associated with the StreamPlatformInitializer.
     * <p>
     * The properties represent a collection of key-value pairs that provide additional configuration
     * for the stream platform. These properties can be accessed and modified externally through
     * the {@link StreamPlatformInitializer} class.
     *
     * @return the properties associated with the StreamPlatformInitializer
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * Sets the properties for the {@link StreamPlatformInitializer}.
     *
     * @param properties the properties to be set
     *
     * @return the {@link StreamPlatformInitializer} instance with the properties set
     */
    public StreamPlatformInitializer setProperties(Properties properties) {
        this.properties = properties;
        return this;
    }
}
