/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import com.fractalworks.streams.core.management.TransportRegister;
import com.fractalworks.streams.core.management.environment.LocalExecutionEnvironment;
import com.fractalworks.streams.core.management.environment.StreamExecutionEnvironment;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresBuilder;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresSpecification;
import com.fractalworks.streams.transport.geode.client.GeodeClientStore;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test to validate connector is configured correctly and connects to cluster
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class GeodeClusterConnectivityTest {

    @Test
    public void validateConnectivity() throws IOException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validGeodePlatformStores.yaml").getFile());

        ReferenceDataStoresBuilder builder = new ReferenceDataStoresBuilder();
        ReferenceDataStoresSpecification referDataSources = builder.build(file);
        assertNotNull(referDataSources);

        // Register
        StreamExecutionEnvironment env = new LocalExecutionEnvironment();
        env.addReferenceDataStores( file);

        TransportRegister register = TransportRegister.getInstance();
        GeodeClientStore store = (GeodeClientStore)register.getTransport("test");
        assertNotNull(store);
    }
}
