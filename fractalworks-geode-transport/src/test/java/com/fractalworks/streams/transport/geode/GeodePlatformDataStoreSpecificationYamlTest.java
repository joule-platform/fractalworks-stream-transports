/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresBuilder;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresSpecification;
import com.fractalworks.streams.transport.geode.specification.GeodeDataStoreSpecification;
import com.fractalworks.streams.transport.geode.specification.GeodeQueryMountPointSpecificationBuilder;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

/**
 * @author Lyndon Adams
 */
public class GeodePlatformDataStoreSpecificationYamlTest {

    @Test
    public void validGeodePlatformStoreYamlTest() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validGeodePlatformStores.yaml").getFile());

        ReferenceDataStoresBuilder builder = new ReferenceDataStoresBuilder();
        ReferenceDataStoresSpecification referDataSources = builder.build(file);
        assertNotNull(referDataSources);
    }

    @Test
    public void validRefStoresYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validMultipleReferenceStores.yaml").getFile());

        GeodeQueryMountPointSpecificationBuilder builder = new GeodeQueryMountPointSpecificationBuilder();
        GeodeDataStoreSpecification spec = (GeodeDataStoreSpecification) builder.build(file);
        assertNotNull(spec );
    }

}
