/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.TransportRegister;
import com.fractalworks.streams.core.management.environment.LocalExecutionEnvironment;
import com.fractalworks.streams.core.management.environment.StreamExecutionEnvironment;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresBuilder;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresSpecification;
import com.fractalworks.streams.transport.geode.client.GeodeClientStore;
import com.fractalworks.streams.transport.geode.specification.GeodePublisherSpecification;
import com.fractalworks.streams.transport.geode.specification.GeodePublisherSpecificationBuilder;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

/**
 * Test to validate connector is configured correctly and connects to cluster
 *
 * @author Lyndon Adams
 */
public class GeodePublisherValidYamlTest {

    @Test
    public void validatePublisherSpec() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validGeodePublisher.yaml").getFile());

        var builder = GeodePublisherSpecification.builder();
        var publisherSpecification = builder.build(file);
        assertNotNull(publisherSpecification);
    }
}
