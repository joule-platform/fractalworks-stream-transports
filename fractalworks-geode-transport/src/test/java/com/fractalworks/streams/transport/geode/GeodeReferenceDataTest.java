/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.management.TransportRegister;
import com.fractalworks.streams.core.management.environment.LocalExecutionEnvironment;
import com.fractalworks.streams.core.management.environment.StreamExecutionEnvironment;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresBuilder;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresSpecification;
import com.fractalworks.streams.transport.geode.client.GeodeClientStore;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Geode client test to ensure reference data stores can be defined using yaml and dynamically
 * created and assigned to a processor
 * Date: 05-01-2021 - Test connects to local cluster and able to retrieve data using query API
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class GeodeReferenceDataTest {

    @Test
    public void validateReferenceDataObjectPutGetTest() throws IOException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validGeodePlatformStores.yaml").getFile());

        ReferenceDataStoresBuilder builder = new ReferenceDataStoresBuilder();
        ReferenceDataStoresSpecification referDataSources = builder.build(file);

        assertNotNull(referDataSources);

        // Register
        StreamExecutionEnvironment env = new LocalExecutionEnvironment();
        env.addReferenceDataStores( file);

        TransportRegister register = TransportRegister.getInstance();
        GeodeClientStore store = (GeodeClientStore)register.getTransport("test");

        ReferenceDataObject obj = new ReferenceDataObject();
        obj.setKey("id-asc-124");
        obj.setData( new String("testObject"));
        obj.put("name", "Smith");
        store.write(obj);

        Optional<Collection<ReferenceDataObject>> response = store.query("id-asc-124");
        assertTrue(response.isPresent());
        assertEquals(1, response.get().size());
        ReferenceDataObject refObject = response.get().stream().toList().get(0);
        assertEquals("testObject", refObject.getData());
        assertEquals("Smith", refObject.get("name"));
    }

    @Test
    public void validRefStoresCreationAndRegistrationTest() throws IOException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validGeodePlatformStores.yaml").getFile());

        ReferenceDataStoresBuilder builder = new ReferenceDataStoresBuilder();
        ReferenceDataStoresSpecification referDataSources = builder.build(file);
        assertNotNull(referDataSources);

        // Register
        StreamExecutionEnvironment env = new LocalExecutionEnvironment();
        env.addReferenceDataStores( file);

        TransportRegister register = TransportRegister.getInstance();
        Store store = (Store)register.getTransport("customer");
        Object obj = store.query(10001);
        assertNotNull(obj);
    }

}
