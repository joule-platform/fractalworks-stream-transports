/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.geode;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.transport.geode.client.GeodeClientPublisherTransport;
import com.fractalworks.streams.transport.geode.specification.EndpointSpecification;
import com.fractalworks.streams.transport.geode.specification.GeodePublisherSpecification;
import org.apache.geode.cache.query.Query;
import org.apache.geode.cache.query.QueryService;
import org.apache.geode.cache.query.SelectResults;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Geode test harness which reuses a Geode client cache and creates required data regions
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class GeodeStreamEventPublishingTest {

    public static final String[] SYMBOLS = {"VMW", "EMC", "IBM", "MSFT", "ORCL"};

    /**
     * Generate set of market tick events
     *
     * @param numberOfEvents
     * @param deltaSpacing
     * @return
     */
    public static List<StreamEvent> createSingleSymbolMarketDataEventSet(int numberOfEvents, long deltaSpacing) {
        Random random = new Random(System.currentTimeMillis());
        UUID uuid = UUID.randomUUID();

        List<StreamEvent> events = new ArrayList<>();
        long eventTime = System.currentTimeMillis();

        Map<String, Double> prices = new HashMap<>();
        prices.put("VMW", 100.94);
        prices.put("EMC", 32.46);
        prices.put("IBM", 91.10);
        prices.put("MSFT", 135.19);
        prices.put("ORCL", 44.43);

        for (int i = 0; i < numberOfEvents; i++) {
            StreamEvent event = new StreamEvent("marketData", eventTime);

            String symbol = SYMBOLS[Math.abs(random.nextInt() % SYMBOLS.length)];

            event.addValue(uuid, "symbol", symbol);
            event.addValue(uuid, "volume", (Math.abs(random.nextInt() % 5000)));

            double px = prices.computeIfPresent(symbol, (key, val) -> {
                if (random.nextBoolean()) {
                    val += (random.nextDouble() % 5) / 10;
                } else {
                    val -= (random.nextDouble() % 3) / 10;
                }
                return val;
            });
            event.addValue(uuid, "price", px);
            events.add(event);
            eventTime += Math.abs(random.nextInt() % deltaSpacing);
        }
        return events;
    }

    /**
     * Get region count using <code>{@link QueryService}</code>
     *
     * @param transport
     * @param query
     * @return
     * @throws Exception
     */
    public static int getCount(GeodeClientPublisherTransport transport, String query) throws Exception {
        QueryService qs = transport.getClusterQueryService(null);
        Query q = qs.newQuery(query);
        SelectResults results = (SelectResults) q.execute();
        return (int) results.iterator().next();
    }

    public static EndpointSpecification getEndpointSpecification(){
        EndpointSpecification endpointSpecification = new EndpointSpecification();
        endpointSpecification.setLocatorAddress("localhost");
        endpointSpecification.setLocatorPort(10334);
        return endpointSpecification;
    }
    @Test
    public void publishJsonEvents() throws Exception {
        GeodePublisherSpecification spec = new GeodePublisherSpecification();
        spec.setEndpointSpecification(getEndpointSpecification() );
        spec.setSerializationType(SerializationType.JSON);
        spec.setRegionName("jsonStreamEvents");

        GeodeClientPublisherTransport transport = new GeodeClientPublisherTransport(spec);
        transport.initialize();
        assertTrue(transport.isConnected());

        List<StreamEvent> evts = createSingleSymbolMarketDataEventSet(10000, 10);
        transport.publish(evts);

        assertEquals( 10000, getCount(transport, "select count(*) from /jsonStreamEvents"));
    }

    @Test
    public void publishPdxEvents() throws Exception {
        GeodePublisherSpecification spec = new GeodePublisherSpecification();
        spec.setEndpointSpecification(getEndpointSpecification() );
        spec.setSerializationType(SerializationType.PDX);
        spec.setRegionName("pdxStreamEvents");

        GeodeClientPublisherTransport transport = new GeodeClientPublisherTransport(spec);
        transport.initialize();
        assertTrue(transport.isConnected());

        List<StreamEvent> evts = createSingleSymbolMarketDataEventSet(10000, 10);
        transport.publish(evts);
        assertEquals(10000, getCount(transport, "select * from /pdxStreamEvents.size"));
    }

    @Test
    public void publishDataEvents() throws Exception {
        GeodePublisherSpecification spec = new GeodePublisherSpecification();
        spec.setEndpointSpecification(getEndpointSpecification() );
        spec.setSerializationType(SerializationType.DATA);
        spec.setRegionName("dataStreamEvents");

        GeodeClientPublisherTransport transport = new GeodeClientPublisherTransport(spec);
        transport.initialize();
        assertTrue(transport.isConnected());

        List<StreamEvent> evts = createSingleSymbolMarketDataEventSet(10000, 10);
        transport.publish(evts);
        assertEquals(10000, getCount(transport, "select * from /dataStreamEvents.size"));
    }

}
