/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.influxdb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DataType;
import com.fractalworks.streams.sdk.formatters.AbstractFormatterSpecification;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import org.slf4j.LoggerFactory;


import java.util.*;

/**
 * InfluxDB data formatter
 *
 * @author Lyndon Adams
 */
public class InfluxDBFormatter extends AbstractFormatterSpecification {

    private boolean useEventTime = true;
    private List<String> tags;
    private Map<String, DataType> measurements;

    public InfluxDBFormatter() {
        contentType = "application/octet-stream";
        encoding = "base64";
        logger = LoggerFactory.getLogger(InfluxDBPublisherTransport.class);
    }

    public InfluxDBFormatter(boolean useEventTime) {
        this();
        this.useEventTime = useEventTime;
        logger = LoggerFactory.getLogger(InfluxDBPublisherTransport.class);
    }

    @Override
    public Object format(StreamEvent event) {
        Point point = new Point(event.getEventType());
        point.time((useEventTime) ? event.getEventTime() : event.getIngestTime(), WritePrecision.MS);
        if( tags!= null){
            tags.forEach(tag-> point.addTag(tag, event.getValue(tag).toString()));
        }
        measurements.forEach((measurement,type) ->  addField( point, measurement, event.getValue(measurement), type));
        return point;
    }

    @Override
    public Object format(Collection<StreamEvent> events) {
        Collection<Point> points = new ArrayList<>();
        for (StreamEvent e : events) {
            points.add((Point) format(e));
        }
        return points;
    }

    /**
     * Adds a field with the given key and value to the specified Point.
     *
     * @param point the Point object to add the field to
     * @param key the key of the field
     * @param value the value of the field
     * @param type the data type of the field
     */
    private void addField(final Point point, final String key, final Object value, DataType type) {
        if( value == null && logger.isWarnEnabled()) {
            logger.warn(String.format("Skipping key: Missing value for field Key: %s, Type: %s", key, type.name()));
            return;
        }

        if (value instanceof Boolean bool) {
            point.addField(key, (Boolean.TRUE.equals(bool) ? 1L : 0L));
        } else if (value instanceof String strValue && type.equals(DataType.STRING)) {
            point.addField(key, strValue);
        } else if (value instanceof Number number) {
            switch (type) {
                case DOUBLE:
                    point.addField(key, number.doubleValue());
                    break;
                case FLOAT:
                    point.addField(key, number.floatValue());
                    break;
                case INTEGER:
                    point.addField(key, number.intValue());
                    break;
                case LONG:
                    point.addField(key, number.longValue());
                    break;
                case SHORT:
                    point.addField(key, number.shortValue());
                    break;
                case BYTE:
                    point.addField(key, number.byteValue());
                    break;
                default: break;
            }
        } else if (value != null &&  value.getClass().isInstance(type)) {
            point.addField(key, (Number)value);
        }
    }

    public Map<String, DataType> getMeasurements() {
        return measurements;
    }

    @JsonProperty(value = "measurements", required = true)
    public void setMeasurements(Map<String, DataType> measurements) {
        this.measurements = measurements;
    }

    public List<String> getTags() {
        return tags;
    }

    @JsonProperty(value = "useEventTime")
    public void setUseEventTime(boolean useEventTime) {
        this.useEventTime = useEventTime;
    }

    public boolean isUseEventTime() {
        return useEventTime;
    }

    @JsonProperty(value = "tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(measurements == null || measurements.isEmpty()){
            throw new InvalidSpecificationException("measurements are required");
        }
    }

    @Override
    public Class<? extends AbstractFormatterSpecification> getComponentClass() {
        return InfluxDBFormatter.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfluxDBFormatter that = (InfluxDBFormatter) o;
        return useEventTime == that.useEventTime && Objects.equals(tags, that.tags) && Objects.equals(measurements, that.measurements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(useEventTime, tags, measurements);
    }

    @Override
    public String toString() {
        return "InfluxDBFormatter{" +
                "useEventTime=" + useEventTime +
                ", tags=" + tags +
                ", measurements=" + measurements +
                "} " + super.toString();
    }
}
