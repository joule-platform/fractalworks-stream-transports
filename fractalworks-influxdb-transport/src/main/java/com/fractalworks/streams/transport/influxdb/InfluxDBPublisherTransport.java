/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.influxdb;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.google.common.base.Objects;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.Bucket;
import com.influxdb.client.domain.BucketRetentionRules;
import com.influxdb.client.write.Point;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * InfluxDB publisher transport
 *
 * @author Lyndon Adams
 */
public class InfluxDBPublisherTransport extends AbstractPublisherTransport {

    private InfluxDBClient influxDB;

    public InfluxDBPublisherTransport(){
        super();
        logger = LoggerFactory.getLogger(InfluxDBPublisherTransport.class);
    }

    public InfluxDBPublisherTransport(InfluxDBSpecification specification) {
        super(specification);
        logger = LoggerFactory.getLogger(InfluxDBPublisherTransport.class);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        InfluxDBSpecification spec = (InfluxDBSpecification) specification;
        formatterSpecification = spec.getFormatter();

        if( spec.getUsername() != null && spec.getPassword()!= null) {
            influxDB = InfluxDBClientFactory.createV1(spec.getUrl(), spec.getUsername(), spec.getPassword().toCharArray(), spec.getBucket(), "");
        } else {
            influxDB = InfluxDBClientFactory.create(spec.getUrl(), spec.getToken().toCharArray(), spec.getOrganisation(), spec.getBucket());
        }

        // Create the bucket
        Bucket bucket = influxDB.getBucketsApi().findBucketByName( spec.getBucket() );
        if( bucket == null ) {
            createBucket(spec);
        }

        if (influxDB == null) {
            throw new TransportInitializationException("Fatal error occurred during InfluxDB client initialization.");
        }

        if(spec.isEnableGzip()){
            influxDB.enableGzip();
        }

        influxDB.setLogLevel( spec.getLogLevel());

        // Now set the database
        if (logger.isInfoEnabled()) {
            logger.info("InfluxDB ready for writes.");
        }
    }

    private Bucket createBucket(InfluxDBSpecification spec){
        BucketRetentionRules retention = new BucketRetentionRules();
        retention.setEverySeconds( spec.getRetentionTimeSeconds() );
        return influxDB.getBucketsApi().createBucket( spec.getBucket(), retention, spec.getOrganisation());
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        try {
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Publishing %d events to influxDB.", events.size()));
                }
                try (WriteApi writeApi = influxDB.makeWriteApi()) {
                    events.forEach(evt -> {
                        writeApi.writePoint((Point) formatterSpecification.format(evt));
                        metrics.incrementMetric(Metric.PROCESSED);
                    });
                }
            }
        } catch (Exception e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            logger.error("Error during event publishing to InfluxDB.", e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    public boolean isConnected() {
        return ( influxDB!= null && influxDB.ping());
    }

    @Override
    public void shutdown() {
        super.shutdown();
        influxDB.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        InfluxDBPublisherTransport that = (InfluxDBPublisherTransport) o;
        return Objects.equal(influxDB, that.influxDB);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), influxDB);
    }
}
