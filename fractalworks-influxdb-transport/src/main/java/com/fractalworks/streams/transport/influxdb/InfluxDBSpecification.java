/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.influxdb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.influxdb.LogLevel;


import java.util.Objects;

/**
 * InfluxDB publishing specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "influxdb")
public class InfluxDBSpecification extends AbstractTransportSpecification {

    private String url;
    private String username;
    private String password;

    private String token;
    private String organisation;
    private String bucket;
    private int retentionTimeSeconds = 3600;
    private boolean enableGzip = true;


    private LogLevel logLevel = LogLevel.BASIC;

    private InfluxDBFormatter formatter;

    public InfluxDBSpecification() {
        // Required for serialization
    }

    public InfluxDBSpecification(String name) {
        super(name);
    }

    public String getUrl() {
        return url;
    }

    @JsonProperty(value = "url", required = true)
    public void setUrl(String url) {
        this.url = url;
    }

    public String getToken() {
        return token;
    }

    @JsonProperty(value = "authToken", required = true)
    public void setToken(String token) {
        this.token = token;
    }


    public String getOrganisation() {
        return organisation;
    }

    @JsonProperty(value = "organisation", required = true)
    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getBucket() {
        return bucket;
    }

    @JsonProperty(value = "bucket", required = true)
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getUsername() {
        return username;
    }

    @JsonProperty(value = "username", required = true)
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @JsonProperty(value = "password", required = true)
    public void setPassword(String password) {
        this.password = password;
    }

    public int getRetentionTimeSeconds() {
        return retentionTimeSeconds;
    }

    @JsonProperty(value = "retentionTime")
    public void setRetentionTimeSeconds(int retentionTimeSeconds) {
        this.retentionTimeSeconds = retentionTimeSeconds;
    }

    public boolean isEnableGzip() {
        return enableGzip;
    }

    @JsonProperty(value = "enableGzip")
    public void setEnableGzip(boolean enableGzip) {
        this.enableGzip = enableGzip;
    }

    public InfluxDBFormatter getFormatter() {
        return formatter;
    }

    @JsonProperty(value = "formatter", required = true)
    public void setFormatter(InfluxDBFormatter formatter) {
        this.formatter = formatter;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    @JsonProperty(value = "logLevel", required = false)
    public void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (url.isEmpty()) {
            throw new InvalidSpecificationException("url must be provided.");
        }

        formatter.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InfluxDBSpecification that)) return false;
        if (!super.equals(o)) return false;
        return retentionTimeSeconds == that.retentionTimeSeconds && enableGzip == that.enableGzip && Objects.equals(url, that.url) && Objects.equals(token, that.token) && Objects.equals(organisation, that.organisation) && Objects.equals(bucket, that.bucket) && Objects.equals(formatter, that.formatter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), url, username, password, token, organisation, bucket, retentionTimeSeconds, enableGzip, formatter);
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return InfluxDBPublisherTransport.class;
    }
}
