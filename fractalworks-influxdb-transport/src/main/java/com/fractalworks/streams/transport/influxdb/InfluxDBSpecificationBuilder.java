/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.influxdb;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DataType;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;


import java.util.List;
import java.util.Map;

/**
 * InfluxDB publishing specification builder
 *
 * @author Lyndon Adams
 */
public class InfluxDBSpecificationBuilder extends AbstractSpecificationBuilder<InfluxDBSpecification> {

    protected int batchSize = 1024;
    protected long timeout = 25;
    private String address;
    private int port;
    private String database;
    private String username;
    private String password;
    private String token;
    private String organisation;

    private boolean useEventTime = true;
    private List<String> tags;
    private Map<String, DataType> measurements;


    public InfluxDBSpecificationBuilder() {
        specificationClass = InfluxDBSpecification.class;
    }

    public InfluxDBSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public InfluxDBSpecificationBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    public InfluxDBSpecificationBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    public InfluxDBSpecificationBuilder setDatabase(String database) {
        this.database = database;
        return this;
    }

    public InfluxDBSpecificationBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public InfluxDBSpecificationBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public InfluxDBSpecificationBuilder setToken(String token) {
        this.token = token;
        return this;
    }

    public InfluxDBSpecificationBuilder setOrganisation(String organisation) {
        this.organisation = organisation;
        return this;
    }

    // Formatter
    public InfluxDBSpecificationBuilder setUseEventTime(boolean useEventTime) {
        this.useEventTime = useEventTime;
        return this;
    }

    public InfluxDBSpecificationBuilder setTags(List<String> tags) {
        this.tags = tags;
        return this;
    }

    public InfluxDBSpecificationBuilder setMeasurements(Map<String, DataType> measurements) {
        this.measurements = measurements;
        return this;
    }

    @Override
    public InfluxDBSpecification build() throws InvalidSpecificationException {

        InfluxDBSpecification spec = new InfluxDBSpecification(name);
        spec.setUrl(String.format("http://%s:%d", address, port));
        spec.setUsername( username);
        spec.setPassword( password);
        spec.setBucket( database);
        spec.setToken( token);
        spec.setOrganisation(organisation);
        spec.setSendTimeout(timeout);
        spec.setBatchSize(batchSize);


        InfluxDBFormatter formatter = new InfluxDBFormatter();
        formatter.setUseEventTime( useEventTime);
        if( tags != null ) formatter.setTags( tags);
        formatter.setMeasurements( measurements);

        spec.setFormatter( formatter);
        spec.validate();
        return spec;
    }
}
