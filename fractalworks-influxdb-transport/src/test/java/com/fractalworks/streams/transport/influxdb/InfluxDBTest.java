/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.influxdb;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DataType;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.util.*;

import static org.junit.Assert.assertTrue;

/**
 * InfluxDB database tests
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class InfluxDBTest {

    public static final String[] SYMBOLS = {"VMW", "EMC", "IBM", "MSFT", "ORCL"};

    private static InfluxDBSpecification createInfluxDBSpecification() throws InvalidSpecificationException {

        Map<String, DataType> measurements = new HashMap<>();
        measurements.put("symbol", DataType.DOUBLE);

        InfluxDBSpecificationBuilder builder = new InfluxDBSpecificationBuilder();
        builder
                .setAddress("localhost")
                .setPort(8086)
                .setDatabase("testBucket")
                .setToken("Z_sm5ZAS6RPXScd9mkliNqW_shOFlUPGyOStRjI5t6Bh2-5U_rz0m03cZDop4r8w091sNFnjjS6vDw91qkw28w==")
                .setOrganisation("8f7f3f92481cf72b")
                .setName("testEventDatabaseInfluxDB")
                .setMeasurements( measurements);

        return builder.build();
    }

    /**
     * Generate set of market tick events
     *
     * @param numberOfEvents
     * @param deltaSpacing
     * @return
     */
    public static List<StreamEvent> createSingleSymbolMarketDataEventSet(int numberOfEvents, long deltaSpacing) {
        Random random = new Random(System.currentTimeMillis());
        UUID uuid = UUID.randomUUID();

        List<StreamEvent> events = new ArrayList<>();
        long eventTime = System.currentTimeMillis();

        Map<String, Double> prices = new HashMap<>();
        prices.put("VMW", 100.94);
        prices.put("EMC", 32.46);
        prices.put("IBM", 91.10);
        prices.put("MSFT", 135.19);
        prices.put("ORCL", 44.43);

        for (int i = 0; i < numberOfEvents; i++) {
            StreamEvent event = new StreamEvent("marketData", eventTime);

            String symbol = SYMBOLS[Math.abs(random.nextInt() % SYMBOLS.length)];

            event.addValue(uuid, "symbol", symbol);
            event.addValue(uuid, "volume", (Math.abs(random.nextInt() % 5000)));

            double px = prices.computeIfPresent(symbol, (key, val) -> {
                if (random.nextBoolean()) {
                    val += (random.nextDouble() % 5) / 10;
                } else {
                    val -= (random.nextDouble() % 3) / 10;
                }
                return val;
            });
            event.addValue(uuid, "price", px);
            events.add(event);
            eventTime += Math.abs(random.nextInt() % deltaSpacing);
        }
        return events;
    }

    @Test
    public void testDatabaseCreation() throws InvalidSpecificationException, TransportException {
        InfluxDBSpecification spec = createInfluxDBSpecification();
        InfluxDBPublisherTransport transport = new InfluxDBPublisherTransport(spec);
        transport.initialize();
        assertTrue(transport.isConnected());
    }


    @Test
    public void testDatabaseReuse() throws InvalidSpecificationException, TransportException {
        InfluxDBSpecification spec = createInfluxDBSpecification();
        InfluxDBPublisherTransport transport1 = new InfluxDBPublisherTransport(spec);
        transport1.initialize();
        assertTrue(transport1.isConnected());

        InfluxDBPublisherTransport transport2 = new InfluxDBPublisherTransport(spec);
        transport2.initialize();
        assertTrue(transport2.isConnected());
    }

    @Test
    public void testStreamData() throws InvalidSpecificationException, TransportException {
        InfluxDBSpecification spec = createInfluxDBSpecification();
        InfluxDBPublisherTransport transport1 = new InfluxDBPublisherTransport(spec);
        transport1.initialize();
        assertTrue(transport1.isConnected());
        List<StreamEvent> evts = createSingleSymbolMarketDataEventSet(5, 100);
        transport1.publish(evts);
    }
}
