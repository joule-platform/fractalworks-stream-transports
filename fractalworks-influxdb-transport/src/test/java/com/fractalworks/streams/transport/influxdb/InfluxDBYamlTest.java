/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.influxdb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.core.dsl.specifications.DataPublishingSpecification;
import com.fractalworks.streams.core.management.SystemSymbolTable;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

/**
 * @author Lyndon Adams
 */
public class InfluxDBYamlTest {

    @Test
    public void validDataPublisherYamlTest() throws InvalidSpecificationException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("validInfluxPublishingSpec.yaml").getFile());

        ObjectMapper readMapper = SystemSymbolTable.getInstance().getSystemObjectMapper("yaml");
        DataPublishingSpecification spec = readMapper.readValue(file, DataPublishingSpecification.class);
        assertNotNull(spec);
    }

}
