/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.errors.InvalidTopicException;
import org.apache.kafka.common.errors.OffsetMetadataTooLarge;
import org.apache.kafka.common.errors.RecordBatchTooLargeException;
import org.apache.kafka.common.errors.UnknownServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Stream event callback to inform user of send failures
 *
 * @author Lyndon Adams
 */
public class EventSendCallback implements Callback {

    private final Logger logger = LoggerFactory.getLogger(EventSendCallback.class);

    private final StreamEvent sentEvent;

    public EventSendCallback(final StreamEvent sentEvent) {
        this.sentEvent = sentEvent;
    }

    /**
     * A callback method the user can implement to provide asynchronous handling of request completion. This method will
     * be called when the record sent to the server has been acknowledged. Exactly one of the arguments will be
     * non-null.
     *
     * @param metadata  The metadata for the record that was sent (i.e. the partition and offset). Null if an error
     *                  occurred.
     * @param exception The exception thrown during processing of this record. Null if no error occurred.
     *                  Possible thrown exceptions include:
     *                  <p>
     *                  Non-Retriable exceptions (fatal, the message will never be sent):
     *                  <p>
     *                  InvalidTopicException
     *                  OffsetMetadataTooLargeException
     *                  RecordBatchTooLargeException
     *                  RecordTooLargeException
     *                  UnknownServerException
     *                  <p>
     *                  Retriable exceptions (transient, may be covered by increasing #.retries):
     *                  <p>
     *                  CorruptRecordException
     *                  InvalidMetadataException
     *                  NotEnoughReplicasAfterAppendException
     *                  NotEnoughReplicasException
     *                  OffsetOutOfRangeException
     *                  TimeoutException
     */
    @Override
    public void onCompletion(RecordMetadata metadata, Exception exception) {
        if (exception!=null) {
            logger.error("Error occurred during send event.", exception);
            if (exception instanceof InvalidTopicException || exception instanceof OffsetMetadataTooLarge || exception instanceof RecordBatchTooLargeException || exception instanceof UnknownServerException) {
                logger.error("Event is being discarded due to unrecoverable error.", exception);
            } else {
                logger.info("Event is being resent.", exception);
            }
        } else {
            if(logger.isDebugEnabled()) {
                logger.debug(String.format("Timestamp:%d Topic:%s partition:%d serializedValueSize:%d",metadata.timestamp(), metadata.topic(), metadata.partition(), metadata.serializedValueSize()));
            }
        }
    }

    /**
     * Get event passed to callback
     *
     * @return the <code>StreamEvent</code>
     */
    public StreamEvent getSentEvent() {
        return sentEvent;
    }
}
