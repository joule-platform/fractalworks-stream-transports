/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.StreamEventParser;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.transport.kafka.serializers.KafkaDeserializerSpecification;
import com.fractalworks.streams.transport.kafka.specification.KafkaConsumerSpecification;
import com.fractalworks.streams.transport.kafka.serializers.avro.KafkaAvroDeserializer;
import org.apache.avro.data.TimeConversions;
import org.apache.avro.specific.SpecificData;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.FencedInstanceIdException;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Kafka consumer transport process.
 *
 * @author Lyndon Adams
 */
public class KafkaConsumerTransport extends AbstractConsumerTransport implements Runnable {

    private final AtomicBoolean closed = new AtomicBoolean(false);
    private KafkaConsumer<?,?> consumer;
    private List<String> topics;
    private StreamEventParser parser;

    private Properties runtimeProperties;

    public KafkaConsumerTransport() {
        super();
        logger = LoggerFactory.getLogger(KafkaConsumerTransport.class);
    }

    public KafkaConsumerTransport(KafkaConsumerSpecification specification) {
        super(specification);
        logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        KafkaConsumerSpecification spec = (KafkaConsumerSpecification)specification;
        topics = spec.getTopics();
        spec.assignClientId();
        SpecificData.get().addLogicalTypeConversion( new TimeConversions.DateConversion());
        Properties properties = spec.getProperties();

        if(spec.getDeserializerSpecification()!=null) {
            properties.putAll(spec.getDeserializerSpecification().getProperties());
            initialiseDeserialization(spec.getDeserializerSpecification(),properties);
        }

        if(logger.isDebugEnabled()) {
            logger.debug("Applying Kafka Consumer Properties:");
            for (Map.Entry<Object, Object> e : properties.entrySet()) {
                logger.debug(String.format("%s-%s",e.getKey(), e.getValue()));
            }
        }
        connect(properties);
        runtimeProperties = properties;
    }

    /**
     * Connects to Kafka using the provided properties.
     *
     * @param properties the properties to be used for connecting to Kafka
     */
    private void connect(Properties properties){
        consumer = new KafkaConsumer<>(properties);
        // Partitions can change over time - Enable this feature
        subscribeAndAssignDynamicPartitions(consumer);
        if (logger.isInfoEnabled()) {
            logger.info("Successfully subscribed to Kafka topics.");
        }
    }

    private void initialiseDeserialization(final KafkaDeserializerSpecification deserializerSpecification, Properties properties) throws TransportInitializationException {
        if (deserializerSpecification.getParser() != null) {
            try {
                parser = deserializerSpecification.getParser().getConstructor().newInstance();

                // Avro custom mapping feature
                if (parser instanceof KafkaAvroDeserializer avroParser) {
                    avroParser.setSchemaFilename(deserializerSpecification.getAvroSpecification().getSchemaFilePath());
                    avroParser.setMapping(deserializerSpecification.getAvroSpecification().getMapping());
                    if (!properties.contains("schema.registry.url")) {
                        properties.put("local.schema.path",deserializerSpecification.getAvroSpecification().getSchemaFilePath());
                    }
                }
                parser.initialise();
            } catch (InvocationTargetException | InstantiationException |
                     IllegalAccessException | NoSuchMethodException e) {
                throw new TransportInitializationException(String.format("Failed to create parser class due to: %s",e.getMessage()), e);
            } catch (StreamsException e) {
                throw new TransportInitializationException(String.format("Failed to initialise parser due to: %s",e.getMessage()), e);
            }
        }
    }

    /**
     * Dynamic partitioning
     */
    private void subscribeAndAssignDynamicPartitions(KafkaConsumer<?,?> c) {
        c.subscribe(topics, new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                if( logger.isInfoEnabled()) {
                    for (TopicPartition p : partitions) {
                        logger.info(String.format("Revoked %d partition for topic{%s}.", p.partition(), p.topic()));
                    }
                }
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                if(logger.isInfoEnabled()) {
                    for (TopicPartition p : partitions) {
                        logger.info(String.format("Assigned %d partition for topic{%s}.", p.partition(), p.topic()));
                    }
                }
            }
        });
    }

    /**
     * Perform batch processing of received records.
     *
     * @param records from Kafka
     * @return collection of StreamEvents events with associated key within a Tuple
     */
    private Collection<Tuple<Object, Collection<StreamEvent>>> performBatchProcessing(final ConsumerRecords records) {
        Collection<Tuple<Object, Collection<StreamEvent>>> eventCollection = new ArrayList<>();
        topics.forEach(topic -> {
            for (final ConsumerRecord<?, ?> consumerRecord : (Iterable<ConsumerRecord>) records.records(topic)) {
                Object key = consumerRecord.key();
                Object value = consumerRecord.value();
                if (value instanceof StreamEvent streamEvent) {
                    Tuple<Object, Collection<StreamEvent>> event = new Tuple<>(key, List.of(streamEvent));
                    eventCollection.add(event);
                    metrics.incrementMetric(Metric.PROCESSED);
                } else if (value instanceof Collection<?> collection) {
                    collection.forEach(e -> {
                        eventCollection.add(new Tuple<>(key, Collections.singletonList((StreamEvent) e)));
                        metrics.incrementMetric(Metric.PROCESSED);
                    });
                } else if (parser != null) {
                    parseRecords(topic,key,value,eventCollection);
                } else {
                    metrics.incrementMetric(Metric.DISCARDED);
                    if(logger.isErrorEnabled())
                        logger.error(String.format("Unknown data type, discarding event from consuming topic %s.", topic));
                }
            }
        });
        return eventCollection;
    }

    /**
     * Parses the records received from Kafka and adds the parsed events to the event collection.
     *
     * @param topic           the topic from which the records are received
     * @param key             the key of the record
     * @param value           the value of the record
     * @param eventCollection the collection to which the parsed events will be added
     */
    private void parseRecords(String topic, Object key, Object value, Collection<Tuple<Object, Collection<StreamEvent>>> eventCollection) {
        try {
            Tuple<Object, Collection<StreamEvent>> parsedEvent = new Tuple<>(key, parser.translate(value));
            eventCollection.add(parsedEvent);
            metrics.incrementMetric(Metric.PROCESSED);
        } catch (TranslationException e) {
            if(logger.isErrorEnabled())
                logger.error(String.format("Failed to parse user data type from topic %s.", topic), e);
        }
    }

    @Override
    public void run() {
        final long timeout = ((KafkaConsumerSpecification) specification).getPollingTimeout();
        try {
            // Read from the beginning of the topic
            while (!closed.get()) {
                // Block indefinitely until new records become available.
                ConsumerRecords<?, ?> records = consumer.poll(Duration.ofMillis(timeout));
                if (!records.isEmpty()) {
                    Collection<Tuple<Object, Collection<StreamEvent>>> events = performBatchProcessing(records);
                    events.forEach(event -> transportListener.onEvent(event.getY()));
                    commitEvents();
                }
            }
        } catch (KafkaException ke) {
            closed.set(true);
            if(logger.isErrorEnabled())
                logger.error("Kafka Consumer - Critical error occurred.", ke);
        }  finally {
            shutdown();
        }
    }

    private void commitEvents() {
        try {
            consumer.commitAsync();
        } catch (FencedInstanceIdException fe) {
            if(logger.isErrorEnabled())
                logger.error("Kafka Consumer - Critical error occurred during commit callback.", fe);
            resetConnection();
        }
    }

    /**
     * Reset the connection to the Kafka cluster.
     * <p>
     * This method tries to reset the connection to the Kafka cluster by performing the following steps:
     * 1. Logs an error message if enabled.
     * 2. Calls the shutdown() method to close the current connection.
     * 3. Calls the connect() method to establish a new connection using the provided properties.
     * <p>
     * If any exception occurs during the process, the closed flag is set to true and an error message is logged.
     */
    private void resetConnection(){
        try {
            if (logger.isErrorEnabled())
                logger.error("Kafka Consumer - Trying to reset cluster connection");
            shutdown();
            connect(runtimeProperties);
        } catch(Exception ex){
            closed.set(true);
            if (logger.isErrorEnabled())
                logger.error("Kafka Consumer - Failed to reset connection.", ex);
        }
    }

    @Override
    public void shutdown() {
        if(consumer!=null) {
            consumer.wakeup();
            consumer.close();
        }
        if(logger.isInfoEnabled()) {
            logger.info("Kafka consumer shut down.");
        }
    }
}