/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;
import com.fractalworks.streams.sdk.codec.avro.AvroDomainSerializer;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.sdk.util.resolvers.KeyResolver;
import com.fractalworks.streams.transport.kafka.serializers.KafkaSerializerSpecification;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.serializers.avro.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.errors.TimeoutException;
import org.slf4j.LoggerFactory;


import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Kafka producer transport process.
 *
 * @author Lyndon Adams
 */
public class KafkaPublisherTransport extends AbstractPublisherTransport {

    private KafkaProducer<Object, Object> producer;
    private KeyResolver<?> keyResolver;
    private String clusterAddress;
    private String topic;
    private int partitions = 0;

    private CustomTransformer<?> transformer;

    public KafkaPublisherTransport() {
        super();
        logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    public KafkaPublisherTransport(KafkaPublisherSpecification specification) {
        super(specification);
        logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) specification;
        topic = spec.getTopic();
        keyResolver = spec.getKeyResolver();
        clusterAddress = spec.getClusterAddress();

        Properties properties = spec.getProperties();

        var serializerSpecification = spec.getSerializerSpecification();
        if( serializerSpecification!= null) {
            properties.putAll( serializerSpecification.getProperties());
            if (serializerSpecification.getTransform() != null) {
                initialiseSerialization(serializerSpecification, properties);
            }
        }
        if(logger.isDebugEnabled()) {
            logger.debug("Applying Kafka Publisher Properties:");
            for (Map.Entry<Object, Object> e : properties.entrySet()) {
                logger.debug(String.format("%s-%s",e.getKey(), e.getValue()));
            }
        }
        producer = new KafkaProducer<>(properties);
        List<PartitionInfo> result = producer.partitionsFor(topic);
        partitions = (result!=null) ? result.size() : 1;
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Successfully initialised Kafka publisher for topic [%s].", topic));
        }
    }

    /**
     * Initializes the serialization process based on the given serializer specification and properties.
     *
     * @param serializerSpecification The serializer specification that defines the key and value serializers.
     * @param properties              The properties to be used for initialization.
     * @throws TransportException If there is an exception during the initialization process.
     */
    private void initialiseSerialization(final KafkaSerializerSpecification serializerSpecification, final Properties properties) throws TransportException {
        if (serializerSpecification.getTransform() != null) {
            try {
                transformer = serializerSpecification.getTransform().getConstructor().newInstance();

                // Apply Avro transform feature
                if(transformer instanceof KafkaAvroSerializer avroTransformer){
                    // Need to switch base on if register exists
                    avroTransformer.setSchemaFilename(serializerSpecification.getAvroSpecification().getSchemaFilePath());
                    avroTransformer.setMapping(serializerSpecification.getAvroSpecification().getMapping());
                    avroTransformer.initialise();
                    properties.put("value.serializer", KafkaAvroSerializer.class.getName());
                }

            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException | StreamsException e) {
                throw new TransportInitializationException("Failed to create transformer instance.", e);
            }
        } else if(serializerSpecification.getValueSerializer()!=null && serializerSpecification.getAvroSpecification()!=null) {
            try {
                AvroDomainSerializer avroTransformer = new AvroDomainSerializer();
                avroTransformer.setSchemaFilename(serializerSpecification.getAvroSpecification().getSchemaFilePath());
                avroTransformer.initialise();
                properties.put("value.serializer", KafkaAvroSerializer.class.getName());

                // Use this to perform the event to schema mapping
                transformer = avroTransformer;
                if(logger.isDebugEnabled()){
                    logger.debug("Initialised internal avro serializer using provided schema");
                }
            } catch (StreamsException e) {
                throw new TransportException("Failed to initialise serializer",e);
            }
        }
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        try {
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                events.forEach(evt -> {
                    try {
                        Object key = keyResolver.getKey(evt);
                        Object value = (transformer!= null) ? transformer.transform(evt) : evt;
                        if (key != null && value != null) {
                            ProducerRecord<Object, Object> producerRecord;
                            // Send event and let the serializer do the formatting work
                            if(partitions > 0) {
                                var v = Math.abs ((key instanceof Number number) ? number.intValue() : key.hashCode());
                                producerRecord = new ProducerRecord<>(topic, Math.floorMod(v, partitions), evt.getIngestTime(), key, value);
                            } else {
                                producerRecord = new ProducerRecord<>(topic, key, value);
                            }
                            producer.send(producerRecord, new EventSendCallback( (logger.isDebugEnabled() || logger.isTraceEnabled()) ? evt : null));
                        } else {
                            metrics.incrementMetric(Metric.PROCESS_FAILED);
                            logger.error(String.format("Failed to create record for topic %s.", topic));
                        }
                        metrics.incrementMetric(Metric.PROCESSED);
                    } catch (TranslationException e) {
                        metrics.incrementMetric(Metric.PROCESS_FAILED);
                        if(logger.isErrorEnabled())
                            logger.error(String.format("Error during event translation process for topic %s.", topic), e);
                    } catch(SerializationException e){
                        metrics.incrementMetric(Metric.PROCESS_FAILED);
                        if(logger.isErrorEnabled())
                            logger.error(String.format("Failed to serialize record for topic %s.", topic), e);
                    } catch (TimeoutException e) {
                        metrics.incrementMetric(Metric.PROCESS_FAILED);
                        if(logger.isErrorEnabled())
                            logger.error(String.format("Error during publishing to topic %s. Check cluster connection %s is correct", topic, clusterAddress), e);
                    }
                });
            }
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    @Override
    public void shutdown() {
        producer.flush();
        producer.close();
        super.shutdown();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KafkaPublisherTransport that)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        if (!Objects.equals(topic, that.topic)) {
            return false;
        }
        return Objects.equals(keyResolver, that.keyResolver);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (keyResolver != null ? keyResolver.hashCode() : 0);
        return result;
    }
}
