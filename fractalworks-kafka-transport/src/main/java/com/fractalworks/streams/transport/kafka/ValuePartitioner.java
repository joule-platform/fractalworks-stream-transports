/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.util.helpers.ObjectCascadeSearchHelper;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Kafka value partitioner which uses the hashcode of the value to partition the event
 *
 * @author Lyndon Adams
 */
public class ValuePartitioner implements Partitioner {

    public static final String VALUE_ATTRIBUTE_LIST = "value.attribute.list";
    private List<String> attributes;

    public ValuePartitioner() {
    }

    public ValuePartitioner(List<String> attributes) {
        this.attributes = attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    /**
     * Compute the partition for the given record.
     *
     * @param topic      The topic name
     * @param key        The key to partition on (or null if no key)
     * @param keyBytes   The serialized key to partition on( or null if no key)
     * @param value      The value to partition on or null
     * @param valueBytes The serialized value to partition on or null
     * @param cluster    The current cluster metadata
     */
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {

        AtomicInteger vkey = new AtomicInteger(0);

        if (attributes == null || attributes.isEmpty()) {
            vkey.set(value.hashCode());
        } else {
            attributes.forEach(attribute -> {
                Object v = ObjectCascadeSearchHelper.findValue((StreamEvent) value, Arrays.asList(attribute.split("\\.")));
                if (v != null) {
                    vkey.addAndGet(v.hashCode());
                }
            });
        }
        return vkey.get() % cluster.partitionCountForTopic(topic);
    }

    /**
     * Configure this class with the given key-value pairs
     *
     * @param configs
     */
    @Override
    public void configure(Map<String, ?> configs) {
        if (configs.containsKey(VALUE_ATTRIBUTE_LIST)) {
            attributes = (List<String>) configs.get(VALUE_ATTRIBUTE_LIST);
        }
    }

    @Override
    public void close() {
        // Default implementation
    }
}
