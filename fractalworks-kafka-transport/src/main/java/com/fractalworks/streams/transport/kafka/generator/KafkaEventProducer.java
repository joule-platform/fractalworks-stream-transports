/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.generator;

import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.sdk.util.resolvers.CompositePartitionKeyResolver;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.KafkaPublisherTransport;
import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Generic event producer which publishes events on to a Kafka topic. Useful for developing and testing use cases
 *
 * @author Lyndon Adams
 */
public class KafkaEventProducer {

    @Option(names = {"-c", "--cluster"}, description = "cluster address", required = true)
    private String clusterAddress;

    @Option(names = {"-t", "--topic"}, description = "topic", required = true)
    private String topic;

    @Option(names = {"-k", "--key"}, description = "partition key", required = true)
    private String partitionKey;

    @Option(names = {"-r", "--properties"}, description = "kafka properties path", required = true)
    private String propertiesPath;

    @Option(names = {"-s", "--scheme"}, description = "scheme file i.e. avro", required = false)
    private String scheme;

    @Option(names = {"-f", "--file"}, description = "test file", required = false)
    private String file;

    @Option(names = {"-g", "--generator"}, description = "class type", required = true)
    private String clazz;

    private KafkaPublisherTransport publisher;
    private final ScheduledExecutorService scheduledPublishingService = Executors.newScheduledThreadPool(2);

    public static void main(String[] args) throws TransportException {
        KafkaEventProducer producer = new KafkaEventProducer();
        new CommandLine(producer).parseArgs(args);
        producer.init();
        producer.start();
    }

    public void init() throws TransportException {
        KafkaPublisherSpecification specification = new KafkaPublisherSpecification("outbound");
        specification.setClusterAddress(clusterAddress);
        specification.setTopic(topic);
        specification.setKeyResolver(new CompositePartitionKeyResolver(Collections.singletonList(partitionKey)));

        try (InputStream input = new FileInputStream( propertiesPath)) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);
            specification.setProperties( prop );
        } catch (IOException e) {
            throw new TransportInitializationException("Failure to load properties file", e);
        }

        publisher = new KafkaPublisherTransport(specification);
        publisher.initialize();
        scheduledPublishingService.scheduleAtFixedRate(publisher, specification.getSendTimeout(), specification.getSendTimeout(), TimeUnit.MILLISECONDS);
    }

    public void start() {
        try {
            Class<?> c = Class.forName(clazz);
            EventGenerator eventGenerator = (EventGenerator) c.getDeclaredConstructor().newInstance();
            eventGenerator.setTransport(publisher);
            eventGenerator.setFile(file);

            Thread t = new Thread(eventGenerator);
            t.start();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new KafkaEventProducerException("Failure during startup", e);
        }
    }
}
