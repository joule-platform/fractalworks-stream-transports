/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.serializers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.transport.kafka.serializers.avro.AvroSpecification;
import com.fractalworks.streams.transport.kafka.serializers.avro.KafkaAvroDeserializer;
import org.apache.kafka.common.serialization.Deserializer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Properties;

import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;

/**
 * Kafka deserializer DSL specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "deserializer")
public class KafkaDeserializerSpecification extends DeserializerSpecification {

    // Serialization
    private Class<? extends Deserializer<?>> keyDeserializer;
    private Class<? extends Deserializer<?>> valueDeserializer;

    private AvroSpecification avroSpecification;

    public KafkaDeserializerSpecification() {
        properties = new Properties();
    }

    /**
     * Set the key deserializer for the KafkaDeserializerSpecification.
     *
     * @param keyDeserializer The fully qualified class name of the key deserializer.
     * @throws StreamsException If there is an error finding or creating the key deserializer class.
     */
    @JsonProperty(value = "key deserializer")
    public void setKeyDeserializer(String keyDeserializer) throws StreamsException {
        try {
            Class<?> clazz = Class.forName(keyDeserializer);
            Constructor<?> constructor = clazz.getConstructor();
            Object obj = constructor.newInstance();

            if (obj instanceof Deserializer) {
                this.keyDeserializer = (Class<? extends Deserializer<?>>)clazz;
                this.properties.put(KEY_DESERIALIZER_CLASS_CONFIG, clazz.getName());
            }else {
                throw new StreamsException("Incompatible key deserializer");
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new StreamsException(String.format("Cannot find/create %s key serializer class.", keyDeserializer),e);
        }
    }

    /**
     * Sets the value deserializer for the KafkaDeserializerSpecification.
     *
     * @param valueDeserializer The fully qualified class name of the value deserializer.
     * @throws StreamsException If there is an error finding or creating the value deserializer class.
     */
    @JsonProperty(value = "value deserializer")
    public void setValueDeserializer(String valueDeserializer) throws StreamsException {
        try {
            Class<?> clazz = Class.forName(valueDeserializer);
            Constructor<?> constructor = clazz.getConstructor();
            Object obj = constructor.newInstance();
            if (obj instanceof Deserializer) {
                this.valueDeserializer = (Class<? extends Deserializer<?>>)clazz;
                this.properties.put("value.deserializer", clazz.getName());
            }else {
                throw new StreamsException("Incompatible value deserializer");
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new StreamsException(String.format("Cannot find/create %s value serializer class.", valueDeserializer),e);
        }
    }

    /**
     * Sets the Avro specification for the Kafka deserializer.
     *
     * @param avroSpecification the AvroSpecification object containing the schema file path and field mapping
     */
    @JsonProperty(value = "avro setting")
    public void setAvroSpecification(AvroSpecification avroSpecification){
        this.avroSpecification = avroSpecification;
        this.valueDeserializer = KafkaAvroDeserializer.class;
        this.parser = KafkaAvroDeserializer.class;
        this.properties.put(VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
    }

    /**
     * Retrieves the Avro specification used for deserialization.
     *
     * @return The AvroSpecification object.
     */
    public AvroSpecification getAvroSpecification() {
        return avroSpecification;
    }

    /**
     * Retrieves the key deserializer class used for deserializing the keys in Kafka messages.
     *
     * @return The key deserializer class.
     */
    public Class<? extends Deserializer<?>> getKeyDeserializer() {
        return keyDeserializer;
    }

    /**
     * Sets the key serializer for the KafkaDeserializer.
     *
     * @param keySerializer the key serializer to set
     */
    public void setKeySerializer(Class<? extends Deserializer<?>> keySerializer) {
        this.keyDeserializer = keySerializer;
        this.properties.put(KEY_DESERIALIZER_CLASS_CONFIG, keySerializer.getName());
    }

    /**
     * Retrieves the value deserializer for the KafkaDeserializerSpecification.
     *
     * @return The value deserializer.
     */
    public Class<? extends Deserializer<?>> getValueDeserializer() {
        return valueDeserializer;
    }

    /**
     * Sets the value deserializer class for KafkaDeserializerSpecification.
     * This method updates the valueDeserializer field of KafkaDeserializerSpecification and puts the value deserializer class name into the properties map.
     *
     * @param valueDeserializer The class object of the value deserializer. It should extend Deserializer<?>.
     */
    public void setValueDeserializer(Class<? extends Deserializer<?>> valueDeserializer) {
        this.valueDeserializer = valueDeserializer;
        this.properties.put(VALUE_DESERIALIZER_CLASS_CONFIG, valueDeserializer.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        KafkaDeserializerSpecification that = (KafkaDeserializerSpecification) o;
        return Objects.equals(keyDeserializer, that.keyDeserializer) && Objects.equals(valueDeserializer, that.valueDeserializer) && Objects.equals(avroSpecification, that.avroSpecification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), keyDeserializer, valueDeserializer, avroSpecification);
    }

    /**
     * Validates the specification of the KafkaDeserializer.
     *
     * @throws InvalidSpecificationException if the specification is invalid
     */
    public void validate() throws InvalidSpecificationException {
        if(keyDeserializer == null)
            logger.info("Setting default configuration for key.deserializer");

        if(valueDeserializer == null && avroSpecification == null)
            logger.info("Using default configuration for value.deserializer");

        if(avroSpecification!=null){
            avroSpecification.validate();
        }
    }
}
