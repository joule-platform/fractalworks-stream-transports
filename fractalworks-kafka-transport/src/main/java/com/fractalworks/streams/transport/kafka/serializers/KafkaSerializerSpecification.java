/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.serializers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.transport.kafka.serializers.avro.AvroSpecification;
import com.fractalworks.streams.transport.kafka.serializers.avro.KafkaAvroSerializer;
import org.apache.kafka.common.serialization.Serializer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

/**
 * Kafka serializer DSL specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "serializer")
public class KafkaSerializerSpecification extends SerializerSpecification {

    private Class<? extends Serializer<?>> keySerializer;
    private Class<? extends Serializer<?>> valueSerializer;
    private AvroSpecification avroSpecification;

    public KafkaSerializerSpecification() {
        properties = new Properties();
    }

    @JsonProperty(value = "key serializer")
    public void setKeySerializer(String keySerializer) throws StreamsException {
        try {
            Class<?> clazz = Class.forName(keySerializer);
            Constructor<?> constructor = clazz.getConstructor();
            Object obj = constructor.newInstance();

            if (obj instanceof Serializer) {
                this.keySerializer = (Class<? extends Serializer<?>>)clazz;

                properties.put(KEY_SERIALIZER_CLASS_CONFIG, clazz.getName());
            }else {
                throw new StreamsException("Incompatible value serializer");
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new StreamsException(String.format("Cannot find/create %s key serializer class.", keySerializer), e);
        }
    }

    @JsonProperty(value = "value serializer")
    public void setValueSerializer(String valueSerializer) throws StreamsException {
        try {
            Class<?> clazz = Class.forName(valueSerializer);
            Constructor<?> constructor = clazz.getConstructor();
            Object obj = constructor.newInstance();
            if (obj instanceof Serializer) {
                this.valueSerializer = (Class<? extends Serializer<?>>)clazz;
                properties.put(VALUE_SERIALIZER_CLASS_CONFIG, clazz.getName());

            }else {
                throw new StreamsException("Incompatible key serializer");
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new StreamsException(String.format("Cannot create %s value serializer class.", valueSerializer), e);
        }
    }

    @JsonProperty(value = "avro setting")
    public void setAvroSpecification(AvroSpecification avroSpecification){
        this.avroSpecification = avroSpecification;
        this.valueSerializer = KafkaAvroSerializer.class;
        this.transformer = KafkaAvroSerializer.class;
        this.properties.put(VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
    }

    public AvroSpecification getAvroSpecification() {
        return avroSpecification;
    }

    public Class<? extends Serializer<?>> getKeySerializer() {
        return keySerializer;
    }

    public void setKeySerializer(Class<? extends Serializer<?>> keySerializer) {
        this.keySerializer = keySerializer;
        properties.put(KEY_SERIALIZER_CLASS_CONFIG, keySerializer.getName());
    }

    public Class<? extends Serializer<?>> getValueSerializer() {
        return valueSerializer;
    }

    public void setValueSerializer(Class<? extends Serializer<?>> valueSerializer) {
        this.valueSerializer = valueSerializer;
        properties.put(VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer.getName());
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if(keySerializer == null)
            logger.info("Setting default configuration for key.serializer");

        if(valueSerializer == null && avroSpecification == null)
            logger.info("Using default configuration for value.serializer");

        if(avroSpecification!=null){
            avroSpecification.validate();
            properties.put("local.schema.path",avroSpecification.getSchemaFilePath());
        } else {
            // Validate base setting
            super.validate();
        }
    }
}
