package com.fractalworks.streams.transport.kafka.serializers.avro;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import java.util.Map;

/**
 * Avro setting specification
 *
 * @author Lyndon Adams
 */
public class AvroSpecification {
    private String schemaFilePath;
    private Map<String,String> mapping;

    public AvroSpecification() {
        // REQUIRED
    }

    /**
     * Retrieves the file path of the Avro schema.
     *
     * @return The file path of the Avro schema.
     */
    public String getSchemaFilePath() {
        return schemaFilePath;
    }

    /**
     * Sets the file path of the Avro schema.
     *
     * @param schemaFilePath the file path of the Avro schema
     * @return the AvroSpecification object
     */
    @JsonProperty(value = "local schema", required = false)
    public AvroSpecification setSchemaFilePath(String schemaFilePath) {
        this.schemaFilePath = schemaFilePath;
        return this;
    }

    /**
     * Retrieves the mapping used for Avro serialization.
     *
     * @return The mapping used for Avro serialization.
     */
    public Map<String, String> getMapping() {
        return mapping;
    }

    /**
     * Sets the field mapping for the Avro specification.
     *
     * @param mapping the field mapping to set
     * @return the AvroSpecification object with the updated field mapping
     */
    @JsonProperty(value = "field mapping", required = false)
    public AvroSpecification setMapping(Map<String, String> mapping) {
        this.mapping = mapping;
        return this;
    }

    /**
     * Validates the AvroSpecification by checking if the schemaFilePath is provided.
     *
     * @throws InvalidSpecificationException if the schemaFilePath is null
     */
    public void validate() throws InvalidSpecificationException {
        if(schemaFilePath == null)
            throw new InvalidSpecificationException("Schema filepath must be provided.");
    }
}
