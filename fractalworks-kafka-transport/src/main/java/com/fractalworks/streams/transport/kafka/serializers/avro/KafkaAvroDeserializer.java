/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.serializers.avro;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.codec.avro.AvroDomainDeserializer;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.StreamsRuntimeException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Avro deserializer map a byte array to <code>StreamEvent</code> using a supplied <code>Schema</code> object
 *
 * @author Lyndon Adams
 */
public class KafkaAvroDeserializer extends AvroDomainDeserializer implements Deserializer<Collection<StreamEvent>> {

    public KafkaAvroDeserializer() {
        super();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        if(configs.containsKey("local.schema.path")){
            try {
                schema = getLocalSchema((String)configs.get("local.schema.path"));
            } catch (StreamsException e) {
                throw new StreamsRuntimeException(e);
            }
        }
    }

    @Override
    public Collection<StreamEvent> deserialize(String topic, byte[] byteData) {
        Collection<StreamEvent> events = new ArrayList<>();
        try {
            events = deserialise(byteData);
        } catch (IOException e) {
            if(logger.isErrorEnabled())
                logger.error("Avro deserialization failed", e);
        }
        return events;
    }

    @Override
    public void close() {
        // No implementation required
    }
}
