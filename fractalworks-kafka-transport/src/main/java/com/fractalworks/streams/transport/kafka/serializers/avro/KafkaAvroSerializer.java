/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.serializers.avro;

import com.fractalworks.streams.sdk.codec.avro.AvroDomainSerializer;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.kafka.common.serialization.Serializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * Avro serializer to map a <code>StreamEvent</code> to a Arvo byte array using a supplied <code>Schema</code> object.
 * This class will first transform the <code>StreamEvent</code> to a <code>GenericRecord</code> ready for the serialization
 * function to convert to a byte array.
 *
 * @author Lyndon Adams
 */
public class KafkaAvroSerializer extends AvroDomainSerializer implements Serializer<GenericRecord> {

    private transient BinaryEncoder encoder;

    public KafkaAvroSerializer() {
        super();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        // Not implemented
    }

    @Override
    public byte[] serialize(String topic, GenericRecord datum) {
        byte[] serializedObject = new byte[0];
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()){
            var datumWriter = new SpecificDatumWriter<GenericRecord>(datum.getSchema());
            encoder = EncoderFactory.get().binaryEncoder(os, encoder);
            datumWriter.write(datum, encoder);
            encoder.flush();
            serializedObject = os.toByteArray();
        } catch (IOException e) {
            if(logger.isErrorEnabled())
                logger.error("Avro serialization failed", e);
        }
        return serializedObject;
    }

    @Override
    public void close() {
        // Not implemented
    }
}
