/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.serializers.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

/**
 * Deserializer map a byte array, json, Kafka message to a <code>StreamEvent</code>
 *
 * @author Lyndon Adams
 */
public class StreamEventJsonDeserializer implements Deserializer<StreamEvent> {

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Default constructor needed by Kafka
     */
    public StreamEventJsonDeserializer() {
        // Required
    }

    @Override
    public void configure(Map<String, ?> props, boolean isKey) {
        // Default implementation
    }

    @Override
    public StreamEvent deserialize(String topic, byte[] bytes) {
        StreamEvent value = null;
        if (bytes != null) {
            try {
                value = objectMapper.readValue(bytes, StreamEvent.class);
            } catch (Exception e) {
                throw new SerializationException(e);
            }
        }
        return value;
    }

    @Override
    public void close() {
        objectMapper = null;
    }
}
