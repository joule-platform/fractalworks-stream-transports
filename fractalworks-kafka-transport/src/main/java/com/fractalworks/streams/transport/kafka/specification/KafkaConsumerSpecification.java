/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.kafka.KafkaConsumerTransport;
import com.fractalworks.streams.transport.kafka.serializers.KafkaDeserializerSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Kafka consumer DSL specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "kafkaConsumer")
public class KafkaConsumerSpecification extends AbstractTransportSpecification {

    @JsonIgnore
    private final Logger logger = LoggerFactory.getLogger(KafkaConsumerSpecification.class);

    private String clusterAddress;
    private String consumerGroupId;
    private List<String> topics;
    private int consumerThreads = 1;
    private long pollingTimeout = 250;
    private Properties properties = new Properties();
    static final String CLIENT_ID = "client.id";

    private KafkaDeserializerSpecification deserializerSpecification;

    public KafkaConsumerSpecification() {
        super();
        loadDefaultProperties();
    }

    public KafkaConsumerSpecification(String name) {
        super(name);
        loadDefaultProperties();
    }

    public KafkaConsumerSpecification(String name,Properties properties) {
        this(name);
        this.properties.putAll(properties);
    }

    private void loadDefaultProperties() {
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream("config/default-kafka-consumer.properties")) {
            if (stream == null)
                throw new FileNotFoundException("default-kafka-consumer.properties");
            properties.load(stream);
        } catch (IOException e) {
            logger.error("Unable to load default default-kafka-consumer.properties");
        }
    }

    /**
     * Set a unique kafka client.id property
     */
    public void assignClientId(){
        if (!this.properties.contains(CLIENT_ID)) {
            var uniqueId = name;
            try {
                uniqueId = UUID.fromString(name).toString();
            }catch(IllegalArgumentException e){
                uniqueId = name + "-" + UUID.randomUUID();
            }
            this.properties.put(CLIENT_ID, uniqueId);
        }
    }

    public String getClusterAddress() {
        return clusterAddress;
    }

    @JsonProperty(value = "cluster address", required = true)
    public void setClusterAddress(String clusterAddress) {
        this.clusterAddress = clusterAddress;
        this.properties.put("bootstrap.servers", clusterAddress);
    }

    public String getConsumerGroupId() {
        return consumerGroupId;
    }

    @JsonProperty(value = "consumerGroupId", required = true)
    public void setConsumerGroupId(String consumerGroupId) {
        this.consumerGroupId = consumerGroupId;
        this.properties.put("group.id", consumerGroupId);
    }

    public List<String> getTopics() {
        return topics;
    }

    @JsonProperty(value = "topics", required = true)
    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    public void addTopic(String topic) {
        if (this.topics == null) {
            this.topics = new ArrayList<>();
        }
        this.topics.add(topic);
    }

    public int getConsumerThreads() {
        return consumerThreads;
    }

    @JsonProperty(value = "consumerThreads", required = false)
    public void setConsumerThreads(int consumerThreads) {
        this.consumerThreads = consumerThreads;
    }


    public long getPollingTimeout() {
        return pollingTimeout;
    }

    @JsonProperty(value = "pollingTimeout", required = false)
    public void setPollingTimeout(long pollingTimeout) {
        this.pollingTimeout = pollingTimeout;
    }

    public Properties getProperties() {
        if( deserializerSpecification != null){
            properties.putAll( deserializerSpecification.getProperties());
        }
        return properties;
    }

    @JsonProperty(value = "properties", required = false)
    public void setProperties(Properties properties) {
        this.properties.putAll( properties);
    }


    public KafkaDeserializerSpecification getDeserializerSpecification() {
        return deserializerSpecification;
    }

    @JsonProperty(value = "deserializer", required = false)
    public void setDeserializerSpecification(KafkaDeserializerSpecification deserializerSpecification) {
        this.deserializerSpecification = deserializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();

        if (clusterAddress == null || clusterAddress.isEmpty()) {
            throw new InvalidSpecificationException("clusterAddress must be provided.");
        }

        if (consumerGroupId == null || consumerGroupId.isEmpty()) {
            throw new InvalidSpecificationException("consumerGroupId must be provided.");
        }

        if (topics == null || topics.isEmpty()) {
            throw new InvalidSpecificationException("Need to provide a list of one ot more topics.");
        }

        if (consumerThreads < 1) {
            throw new InvalidSpecificationException("consumerThreads needs to be greater than zero.");
        }

        if (pollingTimeout <= 0) {
            throw new InvalidSpecificationException("pollingTimeout needs to be greater than zero.");
        }

        if(deserializerSpecification!=null){
            deserializerSpecification.validate();
            properties.putAll( deserializerSpecification.getProperties());
        }
    }

    @JsonIgnore
    @Override
    public Class<? extends Transport> getComponentClass() {
        return KafkaConsumerTransport.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KafkaConsumerSpecification that)) return false;
        if (!super.equals(o)) return false;

        if (pollingTimeout != that.pollingTimeout) {
            return false;
        }
        if (!Objects.equals(clusterAddress, that.clusterAddress)) {
            return false;
        }
        if (!Objects.equals(consumerGroupId, that.consumerGroupId)) {
            return false;
        }
        if (!Objects.equals(topics, that.topics)) {
            return false;
        }
        return (!Objects.equals(properties, that.properties));
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (clusterAddress != null ? clusterAddress.hashCode() : 0);
        result = 31 * result + (consumerGroupId != null ? consumerGroupId.hashCode() : 0);
        result = 31 * result + (topics != null ? topics.hashCode() : 0);
        result = 31 * result + (int) (pollingTimeout ^ (pollingTimeout >>> 32));
        result = 31 * result + (properties != null ? properties.hashCode() : 0);
        return result;
    }
}
