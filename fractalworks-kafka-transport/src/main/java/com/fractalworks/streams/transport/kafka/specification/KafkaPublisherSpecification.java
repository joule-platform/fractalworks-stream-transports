/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.sdk.util.resolvers.CompositePartitionKeyResolver;
import com.fractalworks.streams.sdk.util.resolvers.KeyResolver;
import com.fractalworks.streams.transport.kafka.KafkaPublisherTransport;
import com.fractalworks.streams.transport.kafka.KeyPartitioner;
import com.fractalworks.streams.transport.kafka.serializers.KafkaSerializerSpecification;
import org.apache.kafka.clients.producer.Partitioner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Kafka publisher DSL specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "kafkaPublisher")
public class KafkaPublisherSpecification extends AbstractTransportSpecification {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private Properties properties = new Properties();
    private String clusterAddress;
    private String topic;

    // Parallel processing
    private Class<? extends Partitioner> partitioner = KeyPartitioner.class;
    private KeyResolver<?> keyResolver;

    private KafkaSerializerSpecification serializerSpecification;

    static final String CLIENT_ID = "client.id";

    /**
     * Default constructor for serialization.
     */
    public KafkaPublisherSpecification() {
        loadDefaultProperties();
    }

    public KafkaPublisherSpecification(String name) {
        super(name);
        loadDefaultProperties();
    }

    public KafkaPublisherSpecification(String name, Properties properties) {
        this(name);
        this.properties.putAll(properties);
        if (!this.properties.contains(CLIENT_ID)) {
            this.properties.put(CLIENT_ID, name);
        }
    }

    private void loadDefaultProperties(){
        try (InputStream stream = KafkaPublisherSpecification.class.getClassLoader().getResourceAsStream("config/default-kafka-producer.properties")) {
            if (stream == null)
                throw new FileNotFoundException("default-kafka-producer.properties not found");
            properties.load(stream);
            properties.put(CLIENT_ID, name);
        } catch (IOException e) {
            logger.error("Unable to load default-kafka-producer.properties");
        }
    }

    public Properties getProperties() {
        return properties;
    }

    @JsonProperty(value = "properties")
    public void setProperties(Properties properties) {
        this.properties.putAll( properties);
    }

    public String getTopic() {
        return topic;
    }

    @JsonProperty(value = "topic", required = true)
    public void setTopic(String topic) {
        this.topic = topic;
    }

    public KeyResolver<?> getKeyResolver() {
        return keyResolver;
    }

    public void setKeyResolver(KeyResolver<?> keyResolver) {
        this.keyResolver = keyResolver;
    }

    @JsonProperty(value = "partitionKeys", required = true)
    public void setPartitionKeys(List<String> attributes) {
        setKeyResolver(new CompositePartitionKeyResolver(attributes));
    }

    public String getClusterAddress() {
        return clusterAddress;
    }

    @JsonProperty(value = "cluster address", required = true)
    public void setClusterAddress(String clusterAddress) {
        this.clusterAddress = clusterAddress;
        properties.put("bootstrap.servers",clusterAddress);
    }

    @Override
    @JsonProperty(value = "memBufferSize")
    public void setMemBufferSize(long memBufferSize) {
        super.setMemBufferSize(memBufferSize);
        properties.put("buffer.memory", memBufferSize);
    }

    public Class<? extends Partitioner> getPartitioner() {
        return partitioner;
    }

    public void setPartitioner(Class<? extends Partitioner> partitioner) {
        this.partitioner = partitioner;
    }

    public KafkaSerializerSpecification getSerializerSpecification() {
        return serializerSpecification;
    }

    @JsonProperty(value = "serializer")
    public void setSerializerSpecification(KafkaSerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if (clusterAddress == null || clusterAddress.isEmpty()) {
            throw new InvalidSpecificationException("clusterAddress must be provided.");
        }
        if (topic == null || topic.isEmpty()) {
            throw new InvalidSpecificationException("Need to provide a topic.");
        }
        if (keyResolver == null) {
            throw new InvalidSpecificationException("partitionKeys cannot be null.");
        }

        if( batchSize < 0 ){
            throw new InvalidSpecificationException("batchSize cannot less less than 0.");
        }

        if( serializerSpecification!=null){
            serializerSpecification.validate();
            properties.putAll( serializerSpecification.getProperties());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        KafkaPublisherSpecification that = (KafkaPublisherSpecification) o;
        return Objects.equals(clusterAddress, that.clusterAddress) &&
                Objects.equals(properties, that.properties) &&
                Objects.equals(topic, that.topic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), clusterAddress, properties, topic);
    }

    @Override
    public String toString() {
        return "KafkaPublisherSpecification{" +
                "clusterAddress='" + clusterAddress + '\'' +
                ", properties=" + properties +
                ", topic='" + topic + '\'' +
                ", partitioner=" + partitioner +
                ", keyResolver=" + keyResolver +
                "} ";
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return KafkaPublisherTransport.class;
    }
}
