/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.kafka.serializers.KafkaDeserializerSpecification;

import java.util.List;
import java.util.Properties;

/**
 * DSL builder for Kafka consumer specification
 *
 * @author Lyndon Adams
 */
public class KakfaConsumerSpecificationBuilder extends AbstractSpecificationBuilder<KafkaConsumerSpecification> {

    private String clusterAddress;
    private String consumerGroupId;
    private List<String> topics;
    private int consumerThreads = 1;
    private long pollingTimeout = 100;
    private int numProcessingThreads = 1;
    private Properties properties;
    private KafkaDeserializerSpecification deserializerSpecification;


    public KakfaConsumerSpecificationBuilder() {
        specificationClass = KafkaConsumerSpecification.class;
    }

    public KakfaConsumerSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public KakfaConsumerSpecificationBuilder enable() {
        this.enabled = true;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setClusterAddress(String clusterAddress) {
        this.clusterAddress = clusterAddress;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setConsumerGroupId(String consumerGroupId) {
        this.consumerGroupId = consumerGroupId;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setTopics(List<String> topics) {
        this.topics = topics;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setConsumerThreads(int consumerThreads) {
        this.consumerThreads = consumerThreads;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setKafkaDeserializerSpecification(KafkaDeserializerSpecification deserializerSpecification) {
        this.deserializerSpecification = deserializerSpecification;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setProperties(Properties properties) {
        this.properties = properties;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setPollingTimeout(long pollingTimeout) {
        this.pollingTimeout = pollingTimeout;
        return this;
    }

    public KakfaConsumerSpecificationBuilder setProcessingThreads(int numParsingThreads) {
        this.numProcessingThreads = numParsingThreads;
        return this;
    }

    /**
     * Build specification from user defined variables
     *
     * @return
     * @throws InvalidSpecificationException
     */
    public KafkaConsumerSpecification build() throws InvalidSpecificationException {
        KafkaConsumerSpecification spec = new KafkaConsumerSpecification(name);
        spec.setEnabled(enabled);
        spec.setClusterAddress(clusterAddress);
        spec.setDeserializerSpecification(deserializerSpecification);
        spec.setProperties(properties);
        spec.setTopics(topics);
        spec.setConsumerGroupId(consumerGroupId);
        spec.setConsumerThreads(consumerThreads);
        spec.setPollingTimeout(pollingTimeout);
        spec.setProcessingThreads(numProcessingThreads);
        spec.validate();

        return spec;
    }
}
