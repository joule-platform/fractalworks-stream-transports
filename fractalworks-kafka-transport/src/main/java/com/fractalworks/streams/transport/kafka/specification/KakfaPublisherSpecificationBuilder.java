/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.util.resolvers.KeyResolver;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.kafka.KeyPartitioner;
import com.fractalworks.streams.transport.kafka.serializers.KafkaSerializerSpecification;
import org.apache.kafka.clients.producer.Partitioner;

import java.util.List;
import java.util.Properties;

/**
 * DSL builder for Kafka consumer specification
 *
 * @author Lyndon Adams
 */
public class KakfaPublisherSpecificationBuilder extends AbstractSpecificationBuilder<KafkaPublisherSpecification> {

    private String clusterAddress;
    private String topic;
    private Properties properties = new Properties();
    protected int batchSize = 1024;
    protected long memBufferSize = 33554432;

    private KafkaSerializerSpecification serializerSpecification;

    // Parallel processing
    private Class<? extends Partitioner> partitioner = KeyPartitioner.class;
    private KeyResolver<?> keyResolver;

    private List<String> partitionKeys;

    public KakfaPublisherSpecificationBuilder() {
        specificationClass = KafkaPublisherSpecification.class;
    }

    public KakfaPublisherSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public KakfaPublisherSpecificationBuilder enable() {
        this.enabled = true;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setProperties(Properties properties) {
        this.properties = properties;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setKeyResolver(KeyResolver<?> keyResolver) {
        this.keyResolver = keyResolver;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setPartitionKeys(List<String> attributes) {
        this.partitionKeys = attributes;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setClusterAddress(String clusterAddress) {
        this.clusterAddress = clusterAddress;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setsSerializerSpecification(KafkaSerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setMemBufferSize(long memBufferSize) {
        this.memBufferSize = memBufferSize;
        return this;
    }

    public KakfaPublisherSpecificationBuilder setPartitioner(Class<? extends Partitioner> partitioner) {
        this.partitioner = partitioner;
        return this;
    }

    public KafkaPublisherSpecification build() throws InvalidSpecificationException {
        KafkaPublisherSpecification spec = new KafkaPublisherSpecification(name);
        spec.setEnabled(enabled);
        spec.setClusterAddress(clusterAddress);
        spec.setProperties(properties);
        spec.setTopic(topic);
        spec.setKeyResolver(keyResolver);
        spec.setPartitionKeys(partitionKeys);

        spec.setSerializerSpecification( serializerSpecification);
        spec.setPartitioner(partitioner);
        spec.setBatchSize(batchSize);
        spec.setMemBufferSize(memBufferSize);

        spec.validate();

        return spec;
    }
}
