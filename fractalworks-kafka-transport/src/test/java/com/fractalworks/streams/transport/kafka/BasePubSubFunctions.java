/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.kafka.specification.KafkaConsumerSpecification;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.specification.KakfaConsumerSpecificationBuilder;
import com.fractalworks.streams.transport.kafka.specification.KakfaPublisherSpecificationBuilder;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Disabled("Comment out when running locally")
public class BasePubSubFunctions {

    private static final int EVENT_COUNT = 10;
    public static Collection<StreamEvent> events;

    @BeforeClass
    public static void createCustomerStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT; i++){
            StreamEvent evt = new StreamEvent();
            evt.addValue("Id", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("age", 37);
            events.add( evt);
        }
    }

    public static List<StreamEvent> createQuoteStreamEvents(){
        var events = new ArrayList<StreamEvent>();
        for(int i=0; i<EVENT_COUNT; i++){
            StreamEvent evt = new StreamEvent();
            evt.addValue("symbol", "IBM");
            evt.addValue("mid", 101.5d);
            evt.addValue("bid", 102.1d);
            evt.addValue("ask", 101.1d);
            evt.addValue("volume", 101101L);
            evt.addValue("volatility", 10011.1d);
            evt.addValue("time", System.currentTimeMillis());
            evt.addValue("date", LocalDate.now());
            events.add( evt);
        }
        return events;
    }

    public void publishStreamEvents(String resource, Collection<StreamEvent> passedEvents) throws InvalidSpecificationException, TransportException {
        // Publisher
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(resource).getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        assertNotNull(spec);

        KafkaPublisherTransport transport = new KafkaPublisherTransport( spec);
        transport.initialize();

        // Publish data
        transport.publish( passedEvents);
        transport.shutdown();
    }

    public void consumeStreamEvents(String resource, int eventCount) throws InvalidSpecificationException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(resource).getFile());
        KakfaConsumerSpecificationBuilder consumerBuilder = new KakfaConsumerSpecificationBuilder();
        KafkaConsumerSpecification consumerSpec = (KafkaConsumerSpecification) consumerBuilder.build(file);
        assertNotNull(consumerSpec);

        final AtomicInteger counter = new AtomicInteger();
        KafkaConsumerTransport consumerTransport = new KafkaConsumerTransport( consumerSpec);
        consumerTransport.getForwardingChannel().subscribe((event, context) -> {
            counter.incrementAndGet();
            System.out.println(event);
        });

        consumerTransport.initialize();

        Thread t = new Thread(consumerTransport);
        t.start();

        while( counter.get() < EVENT_COUNT) {
            Thread.onSpinWait();
        }
        consumerTransport.shutdown();
        assertEquals(eventCount, counter.get());
    }
}
