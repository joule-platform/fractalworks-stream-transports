/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.kafka.specification.KafkaConsumerSpecification;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.specification.KakfaConsumerSpecificationBuilder;
import com.fractalworks.streams.transport.kafka.specification.KakfaPublisherSpecificationBuilder;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

@Disabled("Comment out when running locally")
public class KafkaLocalAvroTest extends BasePubSubFunctions {

    private static final int EVENT_COUNT = 10;
    public static Collection<StreamEvent> events;

    @BeforeClass
    public static void createStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT; i++){
            StreamEvent evt = new StreamEvent();
            evt.addValue("Id", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("age", 37);
            events.add( evt);
        }
    }

    @Test
    public void publishAndConsumeCustomerEventsUsingAvro() throws InvalidSpecificationException, TransportException {
        // Publisher
        publishStreamEvents("localKafkaClusterTests/validPublishingKafkaUserDefinedAvroEvents.yaml", events);
        consumeStreamEvents("localKafkaClusterTests/validConsumerKafkaAvroEvents.yaml", EVENT_COUNT);
        assertTrue(true);
    }

    @Test
    public void publishAndConsumeQuoteEventsUsingAvro() throws InvalidSpecificationException, TransportException {
        publishStreamEvents("localKafkaClusterTests/validPublishingKafkaQuoteAvroEvents.yaml", createQuoteStreamEvents());
        consumeStreamEvents("localKafkaClusterTests/validConsumerQuoteAvroEvents.yaml", EVENT_COUNT);
        assertTrue(true);
    }
}
