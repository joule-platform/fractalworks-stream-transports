/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.specification.KakfaPublisherSpecificationBuilder;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

@Disabled("Comment out when running locally")
public class KafkaLocalPublisherTest {

    private static final int EVENT_COUNT = 10;
    public static Collection<StreamEvent> events;

    @BeforeClass
    public static void createStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT; i++){
            StreamEvent evt = new StreamEvent();
            evt.addValue("id", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("age", 37);
            events.add( evt);
        }
    }

    @ParameterizedTest
    @ValueSource(strings =
            {"localKafkaClusterTests/validPublishingKafkaJsonEvents.yaml",
            "localKafkaClusterTests/validPublishingKafkaUserDefinedEvents.yaml",
            "localKafkaClusterTests/validPublishingKafkaAvroEventsUsingRegistry.yaml"})
    void publishStreamEventsTest(String filename) throws InvalidSpecificationException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        Assertions.assertNotNull(spec);

        KafkaPublisherTransport transport = new KafkaPublisherTransport( spec);
        transport.initialize();
        transport.publish(events);
        transport.shutdown();
    }
}