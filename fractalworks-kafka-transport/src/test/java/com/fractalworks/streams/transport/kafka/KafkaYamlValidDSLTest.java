/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.kafka;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.kafka.specification.KafkaConsumerSpecification;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.specification.KakfaConsumerSpecificationBuilder;
import com.fractalworks.streams.transport.kafka.specification.KakfaPublisherSpecificationBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;


/**
 * Kafka transport unit test for valid and invalid yaml files
 *
 * @author Lyndon Adams
 */
class KafkaYamlValidDSLTest {

    @ParameterizedTest
    @ValueSource(strings = {"dsl/valid/validConsumerKafkaEvents.yaml","dsl/valid/validConsumerParsingKafkaEvents.yaml"})
    void validKafkaConsumerDSLTests(String filename)throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        KakfaConsumerSpecificationBuilder builder = new KakfaConsumerSpecificationBuilder();
        KafkaConsumerSpecification spec = (KafkaConsumerSpecification) builder.build(file);
        Assertions.assertNotNull(spec);
    }

    @ParameterizedTest
    @ValueSource(strings = {"dsl/valid/validPublishingKafkaEvents.yaml", "dsl/valid/validPublishEventsUsingCustomTransform.yaml"})
    void validKafkaPublishingDSLTests(String filename)throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        Assertions.assertNotNull(spec);
    }
}
