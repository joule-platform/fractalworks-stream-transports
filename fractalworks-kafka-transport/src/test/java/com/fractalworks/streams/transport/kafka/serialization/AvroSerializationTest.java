package com.fractalworks.streams.transport.kafka.serialization;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.transport.kafka.serializers.KafkaSerializerSpecification;
import com.fractalworks.streams.transport.kafka.serializers.avro.AvroSpecification;
import org.junit.Test;

import java.io.File;

/**
 * Tests to validate avro serialisation configuration
 */
public class AvroSerializationTest {

    @Test
    public void validateAvroSerializationSpecification() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("avro/alert.avsc").getFile());

        var avrospec = new AvroSpecification();
        avrospec.setSchemaFilePath(file.getPath());

        var spec = new KafkaSerializerSpecification();
        spec.setAvroSpecification( avrospec);
        spec.validate();
    }

    @Test
    public void serializeCustomerRecord() throws StreamsException {

        StreamEvent event = new StreamEvent();
        event.addValue("age", 37);
        event.addValue("firstname", "Fred");
        event.addValue("surname", "Blogs");


        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("avro/customer.avsc").getFile());

        var avrospec = new AvroSpecification();
        avrospec.setSchemaFilePath(file.getPath());

        var spec = new KafkaSerializerSpecification();
        spec.setAvroSpecification( avrospec);
        spec.validate();
    }
}
