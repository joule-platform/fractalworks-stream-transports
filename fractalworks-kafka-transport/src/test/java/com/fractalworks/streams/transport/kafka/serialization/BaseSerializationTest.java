package com.fractalworks.streams.transport.kafka.serialization;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.kafka.serializers.json.StreamEventJsonSerializer;
import com.fractalworks.streams.transport.kafka.serializers.object.ObjectSerializer;
import com.fractalworks.streams.transport.kafka.specification.KafkaPublisherSpecification;
import com.fractalworks.streams.transport.kafka.specification.KakfaPublisherSpecificationBuilder;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Test;

import java.io.File;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * Tests to validate serialisation configuration
 */
public class BaseSerializationTest {

    @Test
    public void validatePublishUsingDefaultSerializationSpec() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/serialization/validPublishingUsingDefaultSerialization.yaml").getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        assertNotNull(spec);

        Properties props =  spec.getProperties();
        assertEquals(props.getProperty("key.serializer"), IntegerSerializer.class.getName());
        assertEquals(props.getProperty("value.serializer"), StringSerializer.class.getName());
    }

    @Test
    public void validatePublishJsonSerialization() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/serialization/validPublishingJsonEvents.yaml").getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        assertNotNull(spec);

        Properties props =  spec.getProperties();
        assertEquals(props.getProperty("key.serializer"), IntegerSerializer.class.getName());
        assertEquals(props.getProperty("value.serializer"), StreamEventJsonSerializer.class.getName());
    }

    @Test
    public void validatePublishObjectSerialization() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/serialization/validPublishingObjectEvents.yaml").getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        assertNotNull(spec);

        Properties props =  spec.getProperties();
        assertEquals(props.getProperty("key.serializer"), IntegerSerializer.class.getName());
        assertEquals(props.getProperty("value.serializer"), ObjectSerializer.class.getName());
    }

    @Test
    public void validatePublishAvroSerialization() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/valid/serialization/validPublishingObjectEvents.yaml").getFile());

        KakfaPublisherSpecificationBuilder builder = new KakfaPublisherSpecificationBuilder();
        KafkaPublisherSpecification spec = (KafkaPublisherSpecification) builder.build(file);
        assertNotNull(spec);

        Properties props =  spec.getProperties();
        assertEquals(props.getProperty("key.serializer"), IntegerSerializer.class.getName());
        assertEquals(props.getProperty("value.serializer"), ObjectSerializer.class.getName());
    }
}
