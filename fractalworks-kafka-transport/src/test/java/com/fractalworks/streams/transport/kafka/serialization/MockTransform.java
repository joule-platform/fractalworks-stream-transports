package com.fractalworks.streams.transport.kafka.serialization;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;

import java.util.Collection;

/**
 * Mock Transformer for testing
 */
public class MockTransform implements CustomTransformer<Object> {

    public MockTransform() {
        // REQUIRED
    }

    @Override
    public Collection<Object> transform(Collection<StreamEvent> collection) throws TranslationException {
        return null;
    }

    @Override
    public Object transform(StreamEvent streamEvent) throws TranslationException {
        return null;
    }
}
