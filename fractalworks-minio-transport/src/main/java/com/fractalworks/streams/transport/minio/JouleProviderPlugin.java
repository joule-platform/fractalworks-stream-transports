package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.sdk.functions.CustomPlugin;
import io.minio.credentials.Provider;

/**
 * Custom user plugin interface used for dynamic class loading
 *
 * @author Lyndon Adams
 */
public interface JouleProviderPlugin extends CustomPlugin {

    /**
     * Provide an instance of a <code>io.minio.credentials.Provider</code>
     * @return <code>io.minio.credentials.Provider</code>
     */
    Provider getProvider();
}
