package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.sdk.referencedata.S3EventType;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.transport.minio.internal.MinioSessionFactory;
import com.fractalworks.streams.transport.minio.specification.MinioConsumerSpecification;
import com.fractalworks.streams.transport.minio.specification.serializers.MinioDeserializerSpecification;
import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.Event;
import io.minio.messages.VersioningConfiguration;
import org.apache.arrow.dataset.file.FileFormat;
import org.apache.arrow.dataset.file.FileSystemDatasetFactory;
import org.apache.arrow.dataset.jni.NativeMemoryPool;
import org.apache.arrow.dataset.scanner.ScanOptions;
import org.apache.arrow.dataset.scanner.Scanner;
import org.apache.arrow.dataset.source.Dataset;
import org.apache.arrow.dataset.source.DatasetFactory;
import org.apache.arrow.memory.BufferAllocator;
import org.apache.arrow.memory.RootAllocator;
import org.apache.arrow.vector.FieldVector;
import org.apache.arrow.vector.VectorSchemaRoot;
import org.apache.arrow.vector.ipc.ArrowReader;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * Minio S3 consumer using configured bucket notifications
 *
 * @author Lyndon Adams
 */
public class MinioConsumerTransport extends AbstractConsumerTransport {

    private UUID sessionId;

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private String bucketId;
    private S3EventType[] notificationTypes;

    private final ConcurrentLinkedQueue<Event> minioEvents = new ConcurrentLinkedQueue<>();

    private String region;

    private String eventType;
    private String downloadDirectory;
    private boolean versioning = false;
    private String[] projection;
    private FileFormat fileFormat;
    private String targetProcessedDirectory;

    private int downloadTimeout;

    public MinioConsumerTransport(){
        // Required
        logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    public MinioConsumerTransport(AbstractTransportSpecification specification) {
        super(specification);
        logger = LoggerFactory.getLogger(this.getClass().getName());
        MinioConsumerSpecification spec = (MinioConsumerSpecification) specification;
        batchSize = spec.getBatchSize();
        eventType = spec.getBucketSpecification().getBucketId();
        downloadDirectory = spec.getLocalDownloadDirectory();
        notificationTypes = spec.getBucketSpecification().getNotifications();
        region = spec.getEndpointSpecification().getRegion();
        bucketId = spec.getBucketSpecification().getBucketId();
        targetProcessedDirectory = spec.getTargetProcessedDirectory();
        versioning = spec.getBucketSpecification().getVersioningStatus().equals(VersioningConfiguration.Status.ENABLED);
        downloadTimeout = spec.getDownloadTimeout();
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        MinioConsumerSpecification spec = (MinioConsumerSpecification) specification;
        try {
            // Create client session
            sessionId = MinioSessionFactory.createSession(spec.getEndpointSpecification(),downloadDirectory).getId();

            var processingRate = spec.getProcessingRate();

            // Setup queue listener
            executorService.scheduleAtFixedRate( new ProcessFileEvents(), processingRate, processingRate, TimeUnit.SECONDS);

            if(spec.getDeserializerSpecification()!=null) {
                initializeDeserialiser(spec.getDeserializerSpecification());
            }
        } catch (StreamsException e) {
            logger.error("Error during initialization process", e);
            metrics.incrementMetric(Metric.PROCESS_FAILED);
        } finally {
            if(logger.isDebugEnabled())
                logger.debug("Initialization successful");
        }
    }

    private void initializeDeserialiser(MinioDeserializerSpecification spec) throws TransportInitializationException, StreamsException {
        projection = spec.getProjection();
        fileFormat = spec.getFileFormat();
        if(spec.getParser()!=null) {
            try {
                streamEventParser = spec.getParser().getConstructor().newInstance();
                streamEventParser.initialise();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new TransportInitializationException("Failed to create parser class", e);
            }
        }
    }

    @Override
    public void start() throws TransportException {
        super.start();
        var minioSession = MinioSessionFactory.getSession(sessionId);
        // Subscribe to S3 bucket creation notifications for given file format
        minioSession.registerForBucketNotifications(fileFormat, bucketId,targetProcessedDirectory, region,notificationTypes, minioEvents);
    }

    @Override
    public void shutdown() {
        MinioSessionFactory.closeSession(sessionId);
    }


    private class ProcessFileEvents implements Runnable {

        @Override
        public void run() {
            if (!minioEvents.isEmpty()) {
                Event event = minioEvents.remove();

                if(logger.isDebugEnabled()){
                    logger.debug(String.format("Received event %s occurred at %s for %s/%s",event.eventType(), event.eventTime(),event.bucketName(),event.objectName()));
                }
                try {
                    // Download file
                    var file = downloadObject(event);
                    if(file.exists())
                        processLocalFile(event.objectName(), file.getAbsolutePath(), batchSize);

                } catch (StreamsException e) {
                    logger.error("Failed during waiting for file",e);
                    metrics.incrementMetric(Metric.PROCESS_FAILED);
                }
            }
        }


        private File downloadObject(Event event) throws StreamsException{
            var objectTokens = event.objectName().split("/");
            String filename = objectTokens[objectTokens.length-1];
            File file  = new File(downloadDirectory +filename);
            var minioSession = MinioSessionFactory.getSession(sessionId);
            var client = minioSession.getClient();
            try {
                var builder =  DownloadObjectArgs.builder()
                        .bucket(event.bucketName())
                        .object(event.objectName())
                        .filename(filename)
                        .region(region);

                if(versioning){
                    builder.versionId(event.objectVersionId());
                }
                client.downloadObject(builder.build());

                // Block until file has completed download
                waitUntilFileDownloaded(file);

            } catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException |
                    InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException |
                    XmlParserException e){
                throw new StreamsException("Error during File downloading process", e);
            }
            return file;
        }

        private void waitUntilFileDownloaded(File file) throws StreamsException {
            boolean fileExists = false;
            var timeoutInMilli = downloadTimeout * 1000;
            var sleepTimeout = 250;
            var countdown = 0;
            do {
                synchronized (this) {
                    try {
                        wait(sleepTimeout);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
                if(file.exists()){
                    fileExists = true;
                    break;
                }
                countdown += sleepTimeout;
            }while(countdown < timeoutInMilli);

            if(!fileExists)
                throw new StreamsException("File not downloaded due to timeout condition");
            else if(!file.isFile() )
                throw new StreamsException(String.format("File downloaded is not a valid file. Review %s", file.getAbsoluteFile().getName()));
            else if(!file.getAbsoluteFile().canRead())
                throw new StreamsException(String.format("File downloaded cannot be read. Review %s", file.getAbsoluteFile().getName()));
            else if(FileUtils.sizeOf(file) < 1)
                throw new StreamsException(String.format("File downloaded is an invalid size. Review file %s", file.getAbsoluteFile().getName()));
        }

        /**
         * Process file using arrow stream processing
         *
         * @param fileUri Location of S3 file
         * @param batchSize Number of events to process
         */
        private void processLocalFile(String filename, String fileUri, long batchSize) {
            ScanOptions options = new ScanOptions(batchSize, (projection!=null) ? Optional.of(projection) : Optional.empty());
            try (
                    BufferAllocator allocator = new RootAllocator();
                    DatasetFactory datasetFactory = new FileSystemDatasetFactory(allocator, NativeMemoryPool.getDefault(), fileFormat, "file:" +fileUri);
                    Dataset dataset = datasetFactory.finish();
                    Scanner scanner = dataset.newScan(options);
                    ArrowReader reader = scanner.scanBatches()
            ) {
                while (reader.loadNextBatch()) {
                    try (VectorSchemaRoot root = reader.getVectorSchemaRoot()) {
                        transportListener.onEvent(parseBatch(eventType,root));
                    }
                }
            } catch (Exception e) {
                logger.error(String.format("Processing of %s failed", fileUri),e);
            } finally {
                try {
                    if (targetProcessedDirectory != null)
                        FileUtils.moveFile(new File(fileUri), new File(targetProcessedDirectory,filename));
                    else
                        FileUtils.deleteQuietly(new File(fileUri));
                }catch (IOException e){
                    logger.error(String.format("Failed to move processed file to target directory %s", targetProcessedDirectory), e);
                    metrics.incrementMetric(Metric.PROCESS_FAILED);
                }
            }
        }

        private Collection<StreamEvent> parseBatch(String type,VectorSchemaRoot root){
            Collection<StreamEvent> streamEvents = new ArrayList<>();
            var vectors = root.getFieldVectors();
            for(int r=0; r<root.getRowCount(); r++) {
                StreamEvent event = new StreamEvent(type);
                for (FieldVector fv : vectors) {
                    event.addValue(fv.getName(), fv.getObject(r));
                }
                streamEvents.add(event);
            }
            return streamEvents;
        }
    }
}

