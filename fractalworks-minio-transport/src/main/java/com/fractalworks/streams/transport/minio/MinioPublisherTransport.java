/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.formatters.CSVStreamEventFormatter;
import com.fractalworks.streams.sdk.formatters.JsonStreamEventFormatter;
import com.fractalworks.streams.sdk.formatters.ParquetStreamEventFormatter;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.util.RetryCommand;
import com.fractalworks.streams.transport.minio.internal.MinioSessionFactory;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecification;
import io.minio.*;
import io.minio.errors.*;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetWriter;
import org.apache.parquet.hadoop.ParquetFileWriter;
import org.apache.parquet.hadoop.ParquetWriter;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static io.minio.ObjectWriteArgs.MAX_PART_SIZE;

/**
 * MinIO S3 publisher supports CSV, Json and Parquet formats
 *
 * @author Lyndon Adams
 */
public class MinioPublisherTransport extends AbstractPublisherTransport {

    private Map<String,String> headers = new HashMap<>();
    private Map<String,String> userMetadata = new HashMap<>();

    private UUID sessionId;

    private boolean readyToPublish = false;
    private DateFormat dateFormatter;
    private String contentType;
    private String region;

    static final String ERROR_PUBLISHING_MSG = "Error during event publishing to MinIO.";

    private final Configuration conf = new Configuration();

    private boolean isDatedFolders = true;
    private String customDirectory;

    public MinioPublisherTransport() {
        super();
    }

    public MinioPublisherTransport(AbstractTransportSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        logger = LoggerFactory.getLogger(this.getClass().getName());
        MinioPublisherSpecification spec = (MinioPublisherSpecification) specification;
        formatterSpecification = spec.getSerializerSpecification().getFormatter();
        region = spec.getEndpointSpecification().getRegion();
        isDatedFolders = spec.getBucketSpecification().isPartitionByDate();
        headers = spec.getBucketSpecification().getHeaders();
        userMetadata = spec.getBucketSpecification().getUserMetadata();

        if(!isDatedFolders){
            customDirectory = spec.getBucketSpecification().getCustomDirectory();
        }

        try {
            // Create session
            sessionId = MinioSessionFactory.createSession(spec.getEndpointSpecification(),null).getId();

            // Create bucket if required
            var minioSession = MinioSessionFactory.getSession(sessionId);
            readyToPublish = minioSession.createBucket(spec.getBucketSpecification(), region);

            if (spec.getBucketSpecification().getDateFormat() != null) {
                dateFormatter = new SimpleDateFormat(spec.getBucketSpecification().getDateFormat());
            }

            formatterSpecification.initialise();
            contentType = formatterSpecification.getContentType();

        } catch (Exception e) {
            logger.error("Error during initialization process", e);
            metrics.incrementMetric(Metric.PROCESS_FAILED);
        }
    }


    @Override
    public void publish(Collection<StreamEvent> events) {
        try {
            if (readyToPublish && !events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                MinioPublisherSpecification spec = (MinioPublisherSpecification) specification;
                Map<String, String> headerMap = new HashMap<>();
                headerMap.put("Content-Type", contentType);
                if(headers!=null){
                    headerMap.putAll(headers);
                }

                String fullObjectName;
                long time = System.currentTimeMillis();
                String dateFolder = null;
                if (isDatedFolders) {
                    dateFolder = dateFormatter.format(new Date());
                    fullObjectName = String.format("%s/%s-%d.%s", dateFolder, spec.getBucketSpecification().getObjectName(), time, formatterSpecification.getFileExtension());
                } else if(customDirectory!= null){
                    fullObjectName = String.format("%s/%s-%d.%s",customDirectory, spec.getBucketSpecification().getObjectName(), time, formatterSpecification.getFileExtension());
                } else {
                    fullObjectName = String.format("%s-%d.%s", spec.getBucketSpecification().getObjectName(), time, formatterSpecification.getFileExtension());
                }

                // Write formatted data to S3
                if (formatterSpecification instanceof CSVStreamEventFormatter || formatterSpecification instanceof JsonStreamEventFormatter) {
                    publishClearTextFile(events, spec.getBucketSpecification().getBucketId(), fullObjectName, headerMap);
                } else if (formatterSpecification instanceof ParquetStreamEventFormatter) {
                    publishParquetFile(events, spec.getBucketSpecification().getBucketId(), dateFolder, fullObjectName, time, headerMap);
                }
                metrics.incrementMetric(Metric.PROCESSED, events.size());
            }
        } catch (Exception e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            logger.error(ERROR_PUBLISHING_MSG, e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }


    /**
     * Publish csv / json formatted file
     *
     * @param events
     * @param bucketId
     * @param objectName
     * @param headerMap
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws InvalidKeyException
     * @throws ErrorResponseException
     * @throws InternalException
     * @throws InsufficientDataException
     * @throws InvalidResponseException
     * @throws ServerException
     * @throws XmlParserException
     */
    private void publishClearTextFile(Collection<StreamEvent> events, String bucketId, String objectName, Map<String, String> headerMap)
            throws NoSuchAlgorithmException, IOException,
            InvalidKeyException, ErrorResponseException,
            InternalException, InsufficientDataException, InvalidResponseException, ServerException, XmlParserException {
        Object formattedResponse = formatterSpecification.format(events);
        var minioSession = MinioSessionFactory.getSession(sessionId);
        var client = minioSession.getClient();
        try (ByteArrayInputStream bis = new ByteArrayInputStream(formattedResponse.toString().getBytes(StandardCharsets.UTF_8))) {

            PutObjectArgs.Builder builder = PutObjectArgs.builder();
            builder
               .region(region)
               .bucket(bucketId)
               .object(objectName)
               .contentType(contentType)
               .stream(bis, -1, MAX_PART_SIZE);

            if(!headerMap.isEmpty()){
                builder.headers(headerMap);
            }
            if(!userMetadata.isEmpty()){
                builder.userMetadata(userMetadata);
            }

            client.putObject(builder.build());
        }
    }

    /**
     * Publish parquet formatted file
     *
     * @param events
     * @param bucketId
     * @param dateFolder
     * @param fullObjectName
     * @param time
     * @param headerMap
     * @throws Exception
     */
    private void publishParquetFile(Collection<StreamEvent> events, String bucketId, String dateFolder, String fullObjectName, long time, Map<String, String> headerMap)
            throws IOException {
        MinioPublisherSpecification spec = (MinioPublisherSpecification) specification;
        String[] files = buildParquetFile(events, dateFolder, fullObjectName, time);

        var minioSession = MinioSessionFactory.getSession(sessionId);
        var client = minioSession.getClient();

        // Perform file publishing using a retry command
        new RetryCommand<Boolean>(spec.getBucketSpecification().getRetries(), logger).run(() -> {
            boolean published = true;
            File datafile = new File(files[0]);
            File crcfile = new File(files[1]);
            try {
                UploadObjectArgs.Builder builder = UploadObjectArgs.builder()
                        .region(region)
                        .bucket(bucketId)
                        .object(fullObjectName)
                        .filename(datafile.getAbsolutePath())
                        .headers(headerMap)
                        .userMetadata(userMetadata)
                        .contentType(contentType);

                // Send data file
                client.uploadObject(
                        builder
                            .object(fullObjectName)
                            .filename(datafile.getAbsolutePath())
                            .build());

                if (!((ParquetStreamEventFormatter) formatterSpecification).isDisableCrcFileCreation()) {
                    // Send CRC file
                    client.uploadObject(
                            builder
                               .object(files[2])
                               .filename(crcfile.getAbsolutePath())
                               .build());
                }

            } catch (Exception e) {
                logger.error(ERROR_PUBLISHING_MSG, e);
                published = false;
            } finally {
                // Clean up tmp files
                FileUtils.deleteQuietly(datafile);
                FileUtils.deleteQuietly(crcfile);
            }
            return published;
        });
    }

    /**
     * Build local parquet file read to be stored
     *
     * @param events
     * @param folder
     * @param fullObjectName
     * @param time
     * @return String[]
     * @throws IOException
     */
    private String[] buildParquetFile(Collection<StreamEvent> events, String folder, String fullObjectName, long time) throws IOException {
        ParquetStreamEventFormatter formatter = (ParquetStreamEventFormatter) formatterSpecification;
        var objectName = ((MinioPublisherSpecification) specification).getBucketSpecification().getObjectName();
        String filepath = String.format("%s/%s.parquet", formatter.getTmpFileDir(), fullObjectName);

        try (ParquetWriter<GenericRecord> writer = AvroParquetWriter.<GenericRecord>builder(new Path(filepath))
                .withSchema(formatter.getAvroSchema())
                .withConf(conf)
                .withCompressionCodec(formatter.getCompressionCodecName())
                .withWriteMode(ParquetFileWriter.Mode.CREATE)
                .build()) {

            // Format and write records
            for (StreamEvent e : events)
                writer.write((GenericRecord) formatter.format(e));
        }

        // Send back both the data and meta data crc files
        String crcFilePath = String.format("%s/%s.parquet.crc", formatter.getTmpFileDir(), fullObjectName.replaceFirst(objectName, String.format(".%s", objectName)));
        return new String[]{filepath, crcFilePath, String.format("%s/.%s-%d.crc", folder, objectName, time)};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MinioPublisherTransport that = (MinioPublisherTransport) o;
        return isDatedFolders == that.isDatedFolders && Objects.equals(headers, that.headers) && Objects.equals(userMetadata, that.userMetadata) && Objects.equals(dateFormatter, that.dateFormatter) && Objects.equals(contentType, that.contentType) && Objects.equals(region, that.region);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), headers, userMetadata, dateFormatter, contentType, region, isDatedFolders);
    }

    @Override
    public void shutdown() {
        MinioSessionFactory.closeSession(sessionId);
    }
}