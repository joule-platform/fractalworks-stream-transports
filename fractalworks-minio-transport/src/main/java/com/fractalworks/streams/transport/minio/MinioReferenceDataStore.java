package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.management.listeners.EventHandler;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.fractalworks.streams.sdk.referencedata.S3EventType;
import com.fractalworks.streams.sdk.referencedata.S3ReferenceDataEvent;
import com.fractalworks.streams.sdk.storage.AbstractStore;
import com.fractalworks.streams.sdk.storage.StoreType;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreMountPointSpecification;
import com.fractalworks.streams.transport.minio.internal.MinioSessionFactory;
import com.fractalworks.streams.transport.minio.specification.referencedata.MinioDataStoreMountPointSpecification;
import io.minio.messages.Event;
import io.minio.messages.EventType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;
import java.util.concurrent.*;

/**
 * S3 reference data bucket store implementation
 *
 * @author Lyndon Adams
 */
public class MinioReferenceDataStore extends AbstractStore<ReferenceDataObject> {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    private UUID sessionId;

    private String bucket;
    private String directory;
    private String initialVersionId;
    private final Map<String,EventHandler> eventListeners = new ConcurrentHashMap<>();

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private static final long NOTIFIER_INITIAL_DELAY = 1;
    private static final long NOTIFIER_CYCLE_PERIOD = 1;

    private static final String VERSIONID = "versionId";
    private static final String EMPTY_STRING = "";

    final ConcurrentLinkedQueue<Event> eventQueue = new ConcurrentLinkedQueue<>();

    public MinioReferenceDataStore() {
        // Required
    }

    public MinioReferenceDataStore(DataStoreMountPointSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        MinioDataStoreMountPointSpecification spec = (MinioDataStoreMountPointSpecification) specification;
        sessionId = spec.getEndpointSpecification().getId();
        bucket = spec.getBucketId();
        directory = (spec.getCustomDirectory()!=null) ? spec.getCustomDirectory() : EMPTY_STRING;
        initialVersionId = spec.getInitialVersionId();

        sessionId = MinioSessionFactory.createSession(spec.getEndpointSpecification(), spec.getLocalDownloadDirectory()).getId();
        scheduler.scheduleAtFixedRate(new EventHandlerNotifier(), NOTIFIER_INITIAL_DELAY, NOTIFIER_CYCLE_PERIOD, TimeUnit.SECONDS);
    }

    /**
     * Retrieves the initial image of a reference data object with the specified objectName.
     *
     * @param objectName the name of the reference data object
     * @return an optional containing the initial image of the reference data object, or empty if it doesn't exist
     * @throws StorageException if there is an error retrieving the initial image
     */
    @Override
    public Optional<ReferenceDataObject> getInitialImage(String objectName) throws StorageException {
        return query(
                objectName,
                (initialVersionId!=null) ? Collections.singletonMap(VERSIONID, initialVersionId) : Collections.emptyMap()
        );
    }

    @Override
    public StoreType getStoreType() {
        return StoreType.S3;
    }

    @Override
    public Optional<ReferenceDataObject> query(Object objectName) {
        return query((String)objectName, Collections.emptyMap());
    }

    @Override
    public Optional<ReferenceDataObject> query(String objectName, Map params) {
        ReferenceDataObject response = new ReferenceDataObject();
        try {
            var minioSession = MinioSessionFactory.getSession(sessionId);
            var region = minioSession.getRegion();
            var fullyQualifiedObjectNameWithPath = (objectName.contains("/")) ? objectName : directory + objectName ;

            String versionId = (params!=null && params.containsKey(VERSIONID)) ? (String) params.get(VERSIONID) : null;
            response.setKey(objectName);
            response.setData(minioSession.getLocalObject(region, bucket, fullyQualifiedObjectNameWithPath, versionId));
            response.put("region", (region!=null)? region : EMPTY_STRING);
            response.put(VERSIONID, (versionId!=null) ? versionId : EMPTY_STRING);

            metrics.incrementMetric(Metric.READ_STORE);

        } catch (TransportException e) {
            logger.error(String.format("Failed to get object %s", objectName), e);
            metrics.incrementMetric(Metric.PROCESS_FAILED);
        }
        return Optional.of(response);
    }

    /**
     * Subscribe to S3 bucket "s3:ObjectCreated:*" notifications
     *
     * @param handler <code>EventHandler</code>
     */
    @Override
    public String registerUpdateListener(final EventHandler handler) {
        MinioDataStoreMountPointSpecification spec = (MinioDataStoreMountPointSpecification) specification;
        String[] prefixAndFormat = spec.getObjectName().split("\\.");
        String format = (prefixAndFormat.length > 1) ? prefixAndFormat[1] : EMPTY_STRING;
        var minioSession = MinioSessionFactory.getSession(sessionId);
        var region = minioSession.getRegion();

        var id = minioSession.registerForBucketFileNotifications(
                region,
                spec.getBucketId(),
                spec.getCustomDirectory(),
                prefixAndFormat[0],
                format,
                spec.getNotifications(),
                eventQueue
        );
        eventListeners.put(id,handler);
        return id;
    }

    @Override
    public void unregisterUpdateListener(String id) {
        eventListeners.remove(id);
    }


    private class EventHandlerNotifier implements Runnable {
        public void run() {
            // Drain queue if no listeners
            if(eventListeners.isEmpty()) {
                eventQueue.clear();
                return;
            }

            for (int i = 0; i < eventQueue.size(); i++) {
                Event e = eventQueue.remove();

                var referenceDataEvent = new S3ReferenceDataEvent(convertType(e.eventType()));
                referenceDataEvent.setBucket(e.bucketName());
                referenceDataEvent.setObjectname(e.objectName());
                referenceDataEvent.setRegion(e.region());
                referenceDataEvent.setVersionId(e.objectVersionId());
                eventListeners.values().stream().parallel().forEach(handler
                        -> handler.onEvent(referenceDataEvent, new Context()));
            }
        }

        /**
         * Converts an EventType to an S3EventType.
         *
         * @param eventType the EventType to be converted
         * @return the corresponding S3EventType, or null if no match is found
         */
        private S3EventType convertType(EventType eventType){
            S3EventType type = null;
            switch (eventType) {
                case OBJECT_CREATED_ANY -> type = S3EventType.OBJECT_CREATED_ANY;
                case OBJECT_CREATED_PUT -> type = S3EventType.OBJECT_CREATED_PUT;
                case OBJECT_CREATED_POST -> type = S3EventType.OBJECT_CREATED_POST;
                case OBJECT_CREATED_COPY -> type = S3EventType.OBJECT_CREATED_COPY;
                case OBJECT_CREATED_COMPLETE_MULTIPART_UPLOAD -> type = S3EventType.OBJECT_CREATED_COMPLETE_MULTIPART_UPLOAD;
                case OBJECT_ACCESSED_GET -> type = S3EventType.OBJECT_ACCESSED_GET;
                case OBJECT_ACCESSED_HEAD -> type = S3EventType.OBJECT_ACCESSED_HEAD;
                case OBJECT_ACCESSED_ANY -> type = S3EventType.OBJECT_ACCESSED_ANY;
                case OBJECT_REMOVED_ANY -> type = S3EventType.OBJECT_REMOVED_ANY;
                case OBJECT_REMOVED_DELETE -> type = S3EventType.OBJECT_REMOVED_DELETE;
                case OBJECT_REMOVED_DELETED_MARKER_CREATED -> type = S3EventType.OBJECT_REMOVED_DELETED_MARKER_CREATED;
                case REDUCED_REDUNDANCY_LOST_OBJECT -> type = S3EventType.REDUCED_REDUNDANCY_LOST_OBJECT;
                case BUCKET_CREATED -> type = S3EventType.BUCKET_CREATED;
                case BUCKET_REMOVED -> type = S3EventType.BUCKET_REMOVED;
            }
            return type;
        }
    }

    @Override
    public void shutdown() {
        MinioSessionFactory.closeSession(sessionId);
    }
}
