package com.fractalworks.streams.transport.minio.internal;

import io.minio.CloseableIterator;
import io.minio.ListenBucketNotificationArgs;
import io.minio.MinioClient;
import io.minio.Result;
import io.minio.messages.Event;
import io.minio.messages.NotificationRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Minio bucket change notification handler
 *
 * @author Lyndon Adams
 */
public class BucketNotificationHandler implements Runnable {

    private static final AtomicLong uniqueId = new AtomicLong();
    private final Logger logger;

    private final MinioClient client;
    private final ListenBucketNotificationArgs args;
    private final Collection<Event> notificationQueue;

    private boolean isAlive = true;

    public BucketNotificationHandler(MinioClient client, ListenBucketNotificationArgs args, Collection<Event> notificationQueue){
        logger = LoggerFactory.getLogger("BucketNotificationHandler:"+ uniqueId.incrementAndGet());
        this.client = client;
        this.args = args;
        this.notificationQueue = notificationQueue;
    }

    @Override
    public void run() {
        try (CloseableIterator<Result<NotificationRecords>> ci = client.listenBucketNotification(args)) {
            while (ci.hasNext() && isAlive) {
                NotificationRecords records = ci.next().get();
                notificationQueue.addAll(records.events());
            }
        } catch (Exception e) {
            logger.error("Bucket notification error: ", e);
        }
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}