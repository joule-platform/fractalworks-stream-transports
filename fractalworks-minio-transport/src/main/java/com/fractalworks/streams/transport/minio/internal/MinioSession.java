package com.fractalworks.streams.transport.minio.internal;

import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.S3EventType;
import com.fractalworks.streams.transport.minio.JouleProviderPlugin;
import com.fractalworks.streams.transport.minio.specification.BucketSpecification;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;
import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.Event;
import io.minio.messages.VersioningConfiguration;
import org.apache.arrow.dataset.file.FileFormat;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Minio Session with supporting functions
 *
 * @author Lyndon Adams
 */
public class MinioSession {

    private static final AtomicLong uniqueId = new AtomicLong();

    private final EndpointSpecification endpointSpecification;
    private MinioClient client;

    private String localDownloadDirectory = "./tmp/";

    private Map<String, Tuple<Thread,BucketNotificationHandler>> notificationThreads = new HashMap<>();
    private final Logger logger;

    /**
     * Creates a new instance of MinioSession using the provided EndpointSpecification.
     *
     * @param endpointSpecification the specification of the MinIO endpoint
     */
    public MinioSession(EndpointSpecification endpointSpecification) {
        logger = LoggerFactory.getLogger("MinioSession:" + endpointSpecification.getId());
        this.endpointSpecification = endpointSpecification;
    }

    /**
     * Create a MinioSession object with the specified endpoint specification and local download directory.
     *
     * @param endpointSpecification  The endpoint specification for the MinIO connection.
     * @param localDownloadDirectory The local directory where downloaded files will be saved.
     */
    public MinioSession(EndpointSpecification endpointSpecification, String localDownloadDirectory) {
        logger = LoggerFactory.getLogger("MinioSession:" + endpointSpecification.getId());
        this.endpointSpecification = endpointSpecification;
        this.localDownloadDirectory = localDownloadDirectory;
    }

    /**
     * Retrieves the MinioClient object associated with the MinioSession.
     *
     * @return The MinioClient object.
     */
    public MinioClient getClient() {
        return client;
    }

    /**
     * Retrieves the ID of the MinioSession.
     *
     * @return The ID of the MinioSession.
     */
    public UUID getId(){
        return endpointSpecification.getId();
    }

    /**
     * Retrieves the EndpointSpecification object associated with the MinioSession.
     *
     * @return The EndpointSpecification object.
     */
    public EndpointSpecification getEndpointSpecification() {
        return endpointSpecification;
    }

    /**
     * Creates a MinioClient instance based on the given endpoint specification.
     *
     * @return The MinioClient instance.
     * @throws TransportException If an error occurs during the creation of the client.
     */
    public MinioClient createClient() throws TransportException {
        var clientBuilder = MinioClient.builder();

        if( endpointSpecification.getUrl()!=null){
            clientBuilder.endpoint(endpointSpecification.getUrl());
        } else if(endpointSpecification.getHttpUrl()!=null){
            clientBuilder.endpoint(endpointSpecification.getHttpUrl());
        } else {
            clientBuilder.endpoint(endpointSpecification.getEndpoint(), endpointSpecification.getPort(), endpointSpecification.isUseTLS());
        }

        if(endpointSpecification.getCredentialsSpecification().getProviderPlugin() != null){
            try {
                Class<? extends JouleProviderPlugin> providerPlugin = endpointSpecification.getCredentialsSpecification().getProviderPlugin();
                JouleProviderPlugin providerPluginInstance = providerPlugin.getDeclaredConstructor().newInstance();
                providerPluginInstance.validate();
                providerPluginInstance.initialize();
                clientBuilder
                    .credentialsProvider( providerPluginInstance.getProvider())
                    .region(endpointSpecification.getRegion());

            } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
                throw new TransportException("Failed to create custom provider instance",e);
            } catch (CustomPluginException e) {
                throw new TransportException("Failed to initialize custom provider instance",e);
            } catch (InvalidSpecificationException e) {
                throw new TransportException("Failed to validate custom provider instance",e);
            }

        } else {
            clientBuilder
                .region(endpointSpecification.getRegion())
                .credentials(endpointSpecification.getCredentialsSpecification().getAccessKey(), endpointSpecification.getCredentialsSpecification().getSecretKey());
        }
        client = clientBuilder.build();

        if(localDownloadDirectory!=null) {
            try {
                FileUtils.forceMkdir(new File(localDownloadDirectory));
            } catch (IOException e) {
                throw new TransportException("Unable to create local download directory", e);
            }
        }
        return client;
    }

    /**
     * Creates a bucket with the specified specifications in the given region.
     *
     * @param spec   The specification of the bucket to be created.
     * @param region The region where the bucket should be created.
     * @return True if the bucket creation is successful, false otherwise.
     */
    public boolean createBucket(BucketSpecification spec, String region){
        var bucketCreated = true;
        try {
            var bucketExistsArgs = BucketExistsArgs.builder().bucket(spec.getBucketId()).region(region).build();
            if(!client.bucketExists(bucketExistsArgs)){
                var makeBucketArgs = MakeBucketArgs.builder()
                        .bucket(spec.getBucketId())
                        .region(region)
                        .objectLock(false)
                        .build();

                client.makeBucket(makeBucketArgs);
                bucketCreated = true;

                var bucketVersionArgs = SetBucketVersioningArgs.builder()
                        .bucket(spec.getBucketId())
                        .region(region)
                        .config(new VersioningConfiguration(spec.getVersioningStatus(), false))
                        .build();

                client.setBucketVersioning(bucketVersionArgs);
                if (spec.getBucketPolicy() != null) {
                    client.setBucketPolicy( createBucketPolicyArgs(spec, region) );
                }
                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Created %s bucket successfully", spec.getBucketId()));
                }
            } else if (logger.isInfoEnabled()) {
                logger.info(String.format("Bucket %s exists", spec.getBucketId()));
            }
        } catch(IOException | NoSuchAlgorithmException | InvalidKeyException | URISyntaxException e){
            logger.error("Failed to create bucket", e);
            bucketCreated = false;
        } catch (MinioException e) {
            logger.error(e.httpTrace(), e);
            bucketCreated = false;
        }
        return bucketCreated;
    }

    /**
     * Creates a SetBucketPolicyArgs object from the given BucketSpecification and region.
     *
     * @param spec   The BucketSpecification object containing the bucket policy information.
     * @param region The region where the bucket is located.
     * @return The SetBucketPolicyArgs object with the specified bucket ID, region, and policy configuration.
     * @throws IOException        If there is an I/O error while reading the bucket policy file.
     * @throws URISyntaxException If the resource URI of the bucket policy file is invalid.
     */
    private SetBucketPolicyArgs createBucketPolicyArgs(BucketSpecification spec, String region) throws IOException, URISyntaxException {
        File file = new File(Objects.requireNonNull(getClass().getClassLoader().getResource(spec.getBucketPolicy())).toURI());
        var policyConfig = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        return SetBucketPolicyArgs.builder()
                .bucket(spec.getBucketId())
                .region(region)
                .config(policyConfig)
                .build();
    }
    /**
     * Registers for bucket notifications.
     *
     * @param fileFormat        the file format to monitor
     * @param bucketId          the ID of the bucket to monitor
     * @param directory         the directory within the bucket to monitor (optional)
     * @param region            the region of the bucket
     * @param notificationTypes the types of notification events to listen for
     * @param eventQueue        the collection to store the received notification events
     * @return a unique key associated with the registered notification
     */
    public String registerForBucketNotifications(FileFormat fileFormat, String bucketId, String directory, String region, S3EventType[] notificationTypes, Collection<Event> eventQueue) {
        var suffix = switch (fileFormat){
            case PARQUET, NONE ->  "parquet";
            case CSV -> "csv";
            case ORC -> "orc";
            case ARROW_IPC -> "arrow";
            case JSON -> "json";
        };
        return registerForBucketFileNotifications(region, bucketId, directory, "", suffix, notificationTypes, eventQueue);
    }

    /**
     * Registers for bucket file notifications.
     *
     * @param region            the region of the bucket
     * @param bucketId          the ID of the bucket
     * @param directory         the directory within the bucket to monitor (optional)
     * @param prefix            the prefix of the file names to monitor
     * @param suffix            the suffix of the file names to monitor
     * @param notificationTypes the types of notification events to listen for
     * @param eventQueue        the collection to store the received notification events
     * @return a unique key associated with the registered notification
     */
    public String registerForBucketFileNotifications(String region, String bucketId, String directory,
                                                     String prefix, String suffix,
                                                     S3EventType[] notificationTypes, Collection<Event> eventQueue) {

        var key = String.format("%s:%s:%s:%s:%d",region, bucketId,prefix,suffix, uniqueId.incrementAndGet());
        var fullPrefix = (directory!=null) ? directory + prefix : prefix;
        String[] subTypes = new String[notificationTypes.length];
        for(int i=0; i<notificationTypes.length; i++) {
            subTypes[i]=notificationTypes[i].getValue();
        }

        var bucketNotificationArgs = ListenBucketNotificationArgs.builder()
            .bucket(bucketId)
            .prefix(fullPrefix)
            .suffix(suffix)
            .events(subTypes)
            .region(region);

        var task = new BucketNotificationHandler(client, bucketNotificationArgs.build(), eventQueue);
        Thread notificationThread = new Thread(task);
        notificationThread.start();
        notificationThreads.put(key, new Tuple<>(notificationThread, task));

        if(logger.isInfoEnabled()){
            logger.info(String.format("Registered for bucket notifications using key %s", key));
        }
        return key;
    }

    /**
     * Retrieves an object from a MinIO bucket as an InputStream.
     *
     * @param region     the region of the MinIO bucket
     * @param bucket     the name of the MinIO bucket
     * @param objectName the name of the object in the MinIO bucket
     * @param versionId  the version ID of the object (optional)
     * @return the object as an InputStream
     * @throws ServerException             if an error occurs on the server side
     * @throws InsufficientDataException   if there is not enough data
     * @throws ErrorResponseException      if an error response is received
     * @throws IOException                 if an I/O error occurs
     * @throws NoSuchAlgorithmException   if the algorithm used is not available
     * @throws InvalidKeyException         if the key is invalid
     * @throws InvalidResponseException    if the response is invalid
     * @throws XmlParserException          if there is an error in parsing XML
     * @throws InternalException           if an internal server error occurs
     */
    public InputStream getObjectAsInputStream(String region, String bucket, String objectName, String versionId)
            throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        var builder = GetObjectArgs.builder();
        builder
                .region(region)
                .bucket(bucket)
                .object(objectName)
                .versionId(versionId);
        return client.getObject(builder.build());
    }

    /**
     * Retrieves a local file object from a MinIO bucket.
     *
     * @param region     the region of the MinIO bucket
     * @param bucket     the name of the MinIO bucket
     * @param objectName the name of the object in the MinIO bucket
     * @param versionId  the version ID of the object (optional)
     * @return the local file object downloaded from the MinIO bucket
     * @throws TransportException if an error occurs during the download
     */
    public File getLocalObject(String region, String bucket, String objectName, String versionId) throws TransportException {
        var objectTokens = objectName.split("/");
        String filename = objectTokens[objectTokens.length - 1];
        var modifiedFilename = localDownloadDirectory + filename + "_" + System.currentTimeMillis();

        try {
            FileUtils.forceMkdir(new File(localDownloadDirectory));
            var builder = DownloadObjectArgs.builder()
                    .bucket(bucket)
                    .object(objectName)
                    .filename(modifiedFilename)
                    .region(region)
                    .versionId(versionId);

            client.downloadObject(builder.build());

        } catch ( ErrorResponseException | InsufficientDataException | InternalException |
                 InvalidKeyException |
                 InvalidResponseException | IOException | NoSuchAlgorithmException | ServerException |
                 XmlParserException e) {
            throw new TransportException("Failure getting local object",e);
        }
        return new File(modifiedFilename);
    }

    /**
     * Close client gracefully by shutting down notification tasks
     */
    public void close(){
        notificationThreads.values().parallelStream().forEach(t -> t.getY().setAlive(false));
        notificationThreads = null;
    }

    /**
     * Retrieves the region from the endpoint specification.
     *
     * @return The region specified in the endpoint specification.
     */
    public String getRegion() {
        return endpointSpecification.getRegion();
    }
}
