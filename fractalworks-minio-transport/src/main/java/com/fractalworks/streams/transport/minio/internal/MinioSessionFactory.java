package com.fractalworks.streams.transport.minio.internal;

import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Minio Session factory which will create and cache session for central reuse
 *
 * @author Lyndon Adams
 */
public class MinioSessionFactory {

    protected static final Map<UUID,MinioSession> sessionCache = new HashMap<>();
    private MinioSessionFactory(){}

    /**
     * Creates a MinioSession object and caches it for central reuse.
     *
     * @param endpointSpecification The specifications for the Minio endpoint.
     * @param localDownloadDirectory The local directory where objects will be downloaded.
     * @return A MinioSession object.
     * @throws TransportException If an error occurs during transport.
     */
    public static MinioSession createSession(EndpointSpecification endpointSpecification, String localDownloadDirectory) throws TransportException {
        if(!sessionCache.containsKey(endpointSpecification.getId())) {
            var session = new MinioSession(endpointSpecification, localDownloadDirectory);
            session.createClient();
            sessionCache.put(endpointSpecification.getId(),session);
        }
        return sessionCache.get(endpointSpecification.getId());
    }

    /**
     * Retrieves a MinioSession object from the session cache based on the specified ID.
     *
     * @param id The ID of the MinioSession to retrieve.
     * @return The MinioSession object associated with the specified ID, or null if not found.
     */
    public static MinioSession getSession(UUID id) {
        return sessionCache.get(id);
    }

    /**
     * Close the session with the specified ID. If the session is present in the session cache,
     * it is removed from the cache and closed gracefully by shutting down the notification tasks.
     *
     * @param id The ID of the session to close.
     */
    public static void closeSession(UUID id){
        if(!sessionCache.containsKey(id)) {
            var session = sessionCache.remove(id);
            session.close();
        }
    }

    /**
     * Closes a session based on the provided EndpointSpecification object.
     * If a session with the specified ID exists in the session cache,
     * it is removed from the cache and the session is closed gracefully.
     *
     * @param endpointSpecification The EndpointSpecification object representing the session to be closed.
     */
    public static void closeSession(EndpointSpecification endpointSpecification){
        if(!sessionCache.containsKey(endpointSpecification.getId())) {
            var session = sessionCache.remove(endpointSpecification.getId());
            session.close();
        }
    }

    /**
     * Closes all sessions that have been created using the MinioSessionFactory.
     */
    public static void closeAllSessions(){
        sessionCache.values().parallelStream().forEach(MinioSession::close);
    }
}
