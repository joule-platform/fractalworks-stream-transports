/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;

import java.util.Objects;

/**
 * Base MinIO transport specification
 *
 * @author Lyndon Adams
 */
public abstract class AbstractMinIOSpecification extends AbstractTransportSpecification {

    protected EndpointSpecification endpointSpecification;
    protected BucketSpecification bucketSpecification;

    public EndpointSpecification getEndpointSpecification() {
        return endpointSpecification;
    }

    @JsonProperty(value = "connection", required = true)
    public void setEndpointSpecification(EndpointSpecification endpointSpecification) {
        this.endpointSpecification = endpointSpecification;
    }

    public BucketSpecification getBucketSpecification() {
        return bucketSpecification;
    }

    @JsonProperty(value = "bucket", required = true)
    public void setBucketSpecification(BucketSpecification bucketSpecification) {
        this.bucketSpecification = bucketSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if (endpointSpecification == null) {
            throw new InvalidSpecificationException("Connection endpoint configuration must be provided.");
        }
        if (bucketSpecification == null) {
            throw new InvalidSpecificationException("Bucket configuration must be provided.");
        }
        endpointSpecification.validate();
        bucketSpecification.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractMinIOSpecification that = (AbstractMinIOSpecification) o;
        return Objects.equals(endpointSpecification, that.endpointSpecification) && Objects.equals(bucketSpecification, that.bucketSpecification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), endpointSpecification, bucketSpecification);
    }
}
