package com.fractalworks.streams.transport.minio.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.StreamEventParser;
import com.fractalworks.streams.sdk.referencedata.S3EventType;
import io.minio.messages.VersioningConfiguration;

import java.util.Collections;
import java.util.Map;

/**
 * MinIO bucket specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "bucket")
public class BucketSpecification {

    protected Map<String,String> headers = Collections.emptyMap();
    protected Map<String,String> userMetadata = Collections.emptyMap();
    protected String bucketId;
    protected String objectName;
    protected VersioningConfiguration.Status versioning = VersioningConfiguration.Status.ENABLED;

    protected S3EventType[] notifications = new S3EventType[]{S3EventType.OBJECT_CREATED_ANY};
    protected int retries = 3;
    protected boolean partitionByDate = true;
    protected String dateFormat = "yyyyMMdd";
    protected String customDirectory;
    protected String bucketPolicy;

    protected StreamEventParser streamEventParser;

    public BucketSpecification(){
        // Required
    }

    public String getBucketId() {
        return bucketId;
    }

    @JsonProperty(value = "bucketId", required = true)
    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String getObjectName() {
        return objectName;
    }

    @JsonProperty(value = "object name")
    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public S3EventType[] getNotifications() {
        return notifications;
    }

    @JsonProperty(value = "notifications")
    public void setNotifications(S3EventType[] notifications) {
        this.notifications = notifications;
    }


    public VersioningConfiguration.Status getVersioningStatus() {
        return versioning;
    }

    @JsonProperty(value = "versioning")
    public void setVersioningStatus(VersioningConfiguration.Status versioning) {
        this.versioning = versioning;
    }

    public int getRetries() {
        return retries;
    }

    @JsonProperty(value = "retries")
    public void setRetries(int retries) {
        this.retries = retries;
    }

    public boolean isPartitionByDate() {
        return partitionByDate;
    }

    @JsonProperty(value = "partition by date")
    public void setPartitionByDate(boolean partitionByDate) {
        this.partitionByDate = partitionByDate;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    @JsonProperty(value = "date format")
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
        this.partitionByDate = true;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    @JsonProperty(value = "headers")
    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, String> getUserMetadata() {
        return userMetadata;
    }

    @JsonProperty(value = "user metadata")
    public void setUserMetadata(Map<String, String> userMetadata) {
        this.userMetadata = userMetadata;
    }

    public String getCustomDirectory() {
        return customDirectory;
    }

    @JsonProperty(value = "custom directory")
    public BucketSpecification setCustomDirectory(String customDirectory) {
        this.customDirectory = (customDirectory!=null && !customDirectory.isBlank())?customDirectory : null;
        return this;
    }

    public String getBucketPolicy() {
        return bucketPolicy;
    }

    @JsonProperty(value = "bucket policy")
    public void setBucketPolicy(String bucketPolicy) {
        this.bucketPolicy = bucketPolicy;
    }

    public void validate() throws InvalidSpecificationException {
        if (bucketId == null || bucketId.isEmpty()) {
            throw new InvalidSpecificationException("bucketId cannot be null or empty.");
        }
        if (retries < 0) {
            throw new InvalidSpecificationException("retries cannot be less than 0");
        }
        if(versioning.equals(VersioningConfiguration.Status.OFF)){
            throw new InvalidSpecificationException("Versioning must be either ENABLED or SUSPENDED");
        }
    }
}