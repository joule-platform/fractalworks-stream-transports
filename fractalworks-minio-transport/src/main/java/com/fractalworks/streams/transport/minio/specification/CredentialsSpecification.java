package com.fractalworks.streams.transport.minio.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.minio.JouleProviderPlugin;

/**
 * MinIO credentials specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "credentials")
public class CredentialsSpecification {

    private String accessKey;
    private String secretKey;
    protected Class<? extends JouleProviderPlugin> providerPlugin;

    public CredentialsSpecification(){
        // Required
    }

    public String getAccessKey() {
        return accessKey;
    }

    @JsonProperty(value = "access key", required = false)
    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    @JsonProperty(value = "secret key", required = false)
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Class<? extends JouleProviderPlugin> getProviderPlugin() {
        return providerPlugin;
    }

    @JsonProperty(value = "provider plugin", required = false)
    public void setProvidePluginr(Class<? extends JouleProviderPlugin> provider) {
        this.providerPlugin = provider;
    }

    public void validate() throws InvalidSpecificationException {
        if (providerPlugin != null && accessKey == null && secretKey == null) {
            return;
        }
        if (accessKey == null || secretKey == null) {
            throw new InvalidSpecificationException("access and security keys must be provided.");
        }
    }
}
