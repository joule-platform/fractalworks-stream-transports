package com.fractalworks.streams.transport.minio.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.UUID;

/**
 * MinIO connection specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "connection")
public class EndpointSpecification {

    protected String region;
    protected String endpoint = "https://localhost";
    protected int port = 9000;
    protected URL url;
    protected HttpUrl httpUrl;
    protected OkHttpClient customHttpClient;
    protected boolean useTLS = false;

    protected CredentialsSpecification credentialsSpecification;

    private final UUID id = UUID.randomUUID();

    public EndpointSpecification(){
        credentialsSpecification = new CredentialsSpecification();
    }

    public String getRegion() {
        return region;
    }

    @JsonProperty(value = "region", required = false)
    public void setRegion(String region) {
        this.region = region;
    }

    public String getEndpoint() {
        return endpoint;
    }

    @JsonProperty(value = "endpoint", required = false)
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public int getPort() {
        return port;
    }

    @JsonProperty(value = "port", required = false)
    public void setPort(int port) {
        this.port = port;
    }

    public URL getUrl() {
        return url;
    }

    @JsonProperty(value = "url", required = false)
    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public HttpUrl getHttpUrl() {
        return httpUrl;
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = HttpUrl.parse(httpUrl);
    }

    public OkHttpClient getCustomHttpClient() {
        return customHttpClient;
    }

    public void setCustomHttpClient(OkHttpClient customHttpClient) {
        this.customHttpClient = customHttpClient;
    }

    public CredentialsSpecification getCredentialsSpecification() {
        return credentialsSpecification;
    }

    public boolean isUseTLS() {
        return useTLS;
    }

    @JsonProperty(value = "tls", required = false)
    public void setUseTLS(boolean useTLS) {
        this.useTLS = useTLS;
    }

    @JsonProperty(value = "credentials", required = true)
    public void setCredentialsSpecification(CredentialsSpecification credentialsSpecification) {
        this.credentialsSpecification = credentialsSpecification;
    }

    public UUID getId() {
        return id;
    }

    public void validate() throws InvalidSpecificationException {
        if (url == null && httpUrl==null && (endpoint == null || endpoint.isEmpty()))
            throw new InvalidSpecificationException("Either url, http or endpoint must be provided.");

        if(credentialsSpecification == null)
            throw new InvalidSpecificationException("Credentials section must be provided.");

        credentialsSpecification.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EndpointSpecification that = (EndpointSpecification) o;
        return port == that.port && useTLS == that.useTLS && Objects.equals(region, that.region) && Objects.equals(endpoint, that.endpoint) && Objects.equals(url, that.url) && Objects.equals(httpUrl, that.httpUrl) && Objects.equals(customHttpClient, that.customHttpClient) && Objects.equals(credentialsSpecification, that.credentialsSpecification) && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(region, endpoint, port, url, httpUrl, customHttpClient, useTLS, credentialsSpecification, id);
    }
}

