/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.minio.MinioConsumerTransport;
import com.fractalworks.streams.transport.minio.specification.serializers.MinioDeserializerSpecification;

import java.util.Objects;

/**
 * MinIO publisher specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "minioConsumer")
public class MinioConsumerSpecification extends AbstractMinIOSpecification {

    private long processingRate = 60;  // In seconds

    private int downloadTimeout = 60;

    private String targetProcessedDirectory;
    private String localDownloadDirectory = "./tmp/";

    protected MinioDeserializerSpecification deserializerSpecification;

    public MinioConsumerSpecification() {
        // Required
    }

    public static MinioPublisherSpecificationBuilder builder(){
        return new MinioPublisherSpecificationBuilder();
    }

    public long getProcessingRate() {
        return processingRate;
    }

    @JsonProperty(value = "processing rate", required = false)
    public void setProcessingRate(long processingRate) {
        this.processingRate = processingRate;
    }

    public String getTargetProcessedDirectory() {
        return targetProcessedDirectory;
    }

    @JsonProperty(value = "processed dir", required = false)
    public MinioConsumerSpecification setTargetProcessedDirectory(String targetProcessedDirectory) {
        this.targetProcessedDirectory = targetProcessedDirectory;
        return this;
    }

    public String getLocalDownloadDirectory() {
        return localDownloadDirectory;
    }

    @JsonProperty(value = "download dir", required = false)
    public MinioConsumerSpecification setLocalDownloadDirectory(String localDownloadDirectory) {
        this.localDownloadDirectory = localDownloadDirectory;
        return this;
    }

    public int getDownloadTimeout() {
        return downloadTimeout;
    }

    @JsonProperty(value = "download timeout", required = false)
    public void setDownloadTimeout(int downloadTimeout) {
        this.downloadTimeout = downloadTimeout;
    }

    public MinioDeserializerSpecification getDeserializerSpecification() {
        return deserializerSpecification;
    }

    @JsonProperty(value = "deserializer")
    public void setDeserializerSpecification(MinioDeserializerSpecification deserializerSpecification) {
        this.deserializerSpecification = deserializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(deserializerSpecification == null)
            throw new InvalidSpecificationException("Bucket deserializer must be provided.");

        if(bucketSpecification.getNotifications() == null)
            throw new InvalidSpecificationException("Bucket notifications must be provided");

        if (bucketSpecification.getObjectName() == null || bucketSpecification.getObjectName().isEmpty())
            throw new InvalidSpecificationException("object name cannot be null or empty.");

        if(processingRate < 1)
            throw new InvalidSpecificationException("processing rate must be greater than 0");

        if(downloadTimeout < 1)
            throw new InvalidSpecificationException("processing rate must be greater than 0");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MinioConsumerSpecification that = (MinioConsumerSpecification) o;
        return processingRate == that.processingRate && Objects.equals(targetProcessedDirectory, that.targetProcessedDirectory) && Objects.equals(localDownloadDirectory, that.localDownloadDirectory) && Objects.equals(deserializerSpecification, that.deserializerSpecification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), processingRate, targetProcessedDirectory, localDownloadDirectory, deserializerSpecification);
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return MinioConsumerTransport.class;
    }


}
