/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.minio.JouleProviderPlugin;
import com.fractalworks.streams.transport.minio.specification.serializers.MinioDeserializerSpecification;
import io.minio.messages.VersioningConfiguration;
import okhttp3.OkHttpClient;
import org.apache.arrow.dataset.file.FileFormat;

import java.net.MalformedURLException;
import java.util.UUID;

/**
 * MinIO publisher builder
 * <p>
 * defaults:
 * - endpoint localhost:9000
 * - dated folders using yyyyMMdd format
 * - publishing retries set to 3
 * - CSV formatted files with a batch size of 100k
 *
 * @author Lyndon Adams
 */
public class MinioConsumerSpecificationBuilder extends AbstractSpecificationBuilder<MinioConsumerSpecification>{

    private final EndpointSpecification endpointSpecification = new EndpointSpecification();
    private final BucketSpecification bucketSpecification = new BucketSpecification();

    private final MinioDeserializerSpecification deserializerSpecification = new MinioDeserializerSpecification();

    protected VersioningConfiguration.Status versioning = VersioningConfiguration.Status.ENABLED;

    private int batchSize = 100000;
    private long processingRate = 60;

    private String targetProcessedDirectory;

    private String localDownloadDirectory = "./tmp/";

    public MinioConsumerSpecificationBuilder() {
        specificationClass = MinioConsumerSpecification.class;
    }

    public MinioConsumerSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public MinioConsumerSpecificationBuilder setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public MinioConsumerSpecificationBuilder setEndpoint(String endpoint) {
        endpointSpecification.setEndpoint(endpoint);
        return this;
    }

    public MinioConsumerSpecificationBuilder setUrl(String url) throws MalformedURLException {
        endpointSpecification.setUrl(url);
        return this;
    }

    public MinioConsumerSpecificationBuilder setHttpUrl(String httpUrl) {
        endpointSpecification.setHttpUrl(httpUrl);
        return this;
    }

    public MinioConsumerSpecificationBuilder setCustomHttpClient(OkHttpClient customHttpClient) {
        endpointSpecification.setCustomHttpClient(customHttpClient);
        return this;
    }


    public MinioConsumerSpecificationBuilder setPort(int port) {
        endpointSpecification.setPort(port);
        return this;
    }

    public MinioConsumerSpecificationBuilder setUseTLS(boolean usetls) {
        endpointSpecification.setUseTLS(usetls);
        return this;
    }

    public MinioConsumerSpecificationBuilder setAccessKey(String accessKey) {
        endpointSpecification.credentialsSpecification.setAccessKey(accessKey);
        return this;
    }

    public MinioConsumerSpecificationBuilder setSecretKey(String secretKey) {
        endpointSpecification.credentialsSpecification.setSecretKey(secretKey);
        return this;
    }

    public MinioConsumerSpecificationBuilder setRegion(String region) {
        endpointSpecification.region = region;
        return this;
    }

    public MinioConsumerSpecificationBuilder setBucketId(String bucketId) {
        bucketSpecification.bucketId = bucketId;
        return this;
    }

    public MinioConsumerSpecificationBuilder setVersioning(VersioningConfiguration.Status versioning) {
        this.versioning = versioning;
        return this;
    }

    public MinioConsumerSpecificationBuilder setBucketPolicy(String bucketPolicy) {
        bucketSpecification.bucketPolicy = bucketPolicy;
        return this;
    }

    public MinioConsumerSpecificationBuilder setDatedFolders(boolean datedFolders) {
        bucketSpecification.partitionByDate = datedFolders;
        return this;
    }

    public MinioConsumerSpecificationBuilder setDateFormat(String dateFormat) {
        bucketSpecification.dateFormat = dateFormat;
        if(dateFormat!= null && !dateFormat.isBlank()){
            bucketSpecification.partitionByDate = true;
        }
        return this;
    }

    public MinioConsumerSpecificationBuilder setRetries(int retries) {
        bucketSpecification.retries = retries;
        return this;
    }

    public MinioConsumerSpecificationBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public MinioConsumerSpecificationBuilder setObjectName(String objectName) {
        bucketSpecification.objectName = objectName;
        return this;
    }

    public MinioConsumerSpecificationBuilder setProvider(Class<? extends JouleProviderPlugin> provider) {
        endpointSpecification.credentialsSpecification.setProvidePluginr(provider);
        return this;
    }

    public MinioConsumerSpecificationBuilder setProcessingRate(long processingRate) {
        this.processingRate = processingRate;
        return this;
    }

    public MinioConsumerSpecificationBuilder setFileFormat(FileFormat fileFormat) {
        deserializerSpecification.setFileFormat(fileFormat);
        return this;
    }

    public MinioConsumerSpecificationBuilder setProjection(String[] projection) {
        deserializerSpecification.setProjection(projection);
        return this;
    }

    public MinioConsumerSpecificationBuilder setTargetProcessedDirectory(String targetProcessedDirectory) {
        this.targetProcessedDirectory = targetProcessedDirectory;
        return this;
    }

    public MinioConsumerSpecificationBuilder setLocalDownloadDirectory(String localDownloadDirectory) {
        this.localDownloadDirectory = localDownloadDirectory;
        return this;
    }

    @Override
    public MinioConsumerSpecification build() throws InvalidSpecificationException {
        MinioConsumerSpecification spec =  new MinioConsumerSpecification();
        spec.setName((name == null || name.isBlank())? UUID.randomUUID().toString() : name);
        spec.setEnabled(enabled);
        spec.setEndpointSpecification(endpointSpecification);
        spec.setBucketSpecification(bucketSpecification);
        spec.setDeserializerSpecification(deserializerSpecification);
        spec.setBatchSize(batchSize);
        spec.setTargetProcessedDirectory(targetProcessedDirectory);
        spec.setProcessingRate(processingRate);
        spec.setLocalDownloadDirectory(localDownloadDirectory);
        spec.validate();

        return spec;
    }
}