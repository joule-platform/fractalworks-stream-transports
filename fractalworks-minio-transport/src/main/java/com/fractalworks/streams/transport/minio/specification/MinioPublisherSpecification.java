/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.minio.MinioPublisherTransport;

import java.util.Objects;

/**
 * MinIO publisher specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "minioPublisher")
public class MinioPublisherSpecification extends AbstractMinIOSpecification {

    private SerializerSpecification serializerSpecification;

    public MinioPublisherSpecification() {
        // Required
        batchSize = 100000;
    }

    public SerializerSpecification getSerializerSpecification() {
        return serializerSpecification;
    }

    @JsonProperty(value = "serializer")
    public void setSerializerSpecification(SerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
    }

    public static MinioPublisherSpecificationBuilder builder(){
        return new MinioPublisherSpecificationBuilder();
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(serializerSpecification ==null)
            throw new InvalidSpecificationException("Bucket serializer must be provided.");
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MinioPublisherSpecification that = (MinioPublisherSpecification) o;
        return Objects.equals(serializerSpecification, that.serializerSpecification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), serializerSpecification);
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return MinioPublisherTransport.class;
    }

}
