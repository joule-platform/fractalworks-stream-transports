/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.minio.JouleProviderPlugin;
import io.minio.messages.VersioningConfiguration;
import okhttp3.OkHttpClient;

import java.net.MalformedURLException;
import java.util.UUID;

/**
 * MinIO publisher builder
 * <p>
 * defaults:
 * - endpoint localhost:9000
 * - dated folders using yyyyMMdd format
 * - publishing retries set to 3
 * - CSV formatted files with a batch size of 100k
 *
 * @author Lyndon Adams
 */
public class MinioPublisherSpecificationBuilder extends AbstractSpecificationBuilder<MinioPublisherSpecification> {

    private final EndpointSpecification endpointSpecification = new EndpointSpecification();
    private final BucketSpecification bucketSpecification = new BucketSpecification();

    private SerializerSpecification serializerSpecification;

    protected VersioningConfiguration.Status versioning = VersioningConfiguration.Status.ENABLED;

    private int batchSize = 100000;

    public MinioPublisherSpecificationBuilder() {
        specificationClass = MinioPublisherSpecification.class;
    }

    public MinioPublisherSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public MinioPublisherSpecificationBuilder setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public MinioPublisherSpecificationBuilder setEndpoint(String endpoint) {
        endpointSpecification.setEndpoint(endpoint);
        return this;
    }

    public MinioPublisherSpecificationBuilder setUrl(String url) throws MalformedURLException {
        endpointSpecification.setUrl(url);
        return this;
    }

    public MinioPublisherSpecificationBuilder setHttpUrl(String httpUrl) {
        endpointSpecification.setHttpUrl(httpUrl);
        return this;
    }

    public MinioPublisherSpecificationBuilder setCustomHttpClient(OkHttpClient customHttpClient) {
        endpointSpecification.setCustomHttpClient(customHttpClient);
        return this;
    }


    public MinioPublisherSpecificationBuilder setPort(int port) {
        endpointSpecification.setPort(port);
        return this;
    }

    public MinioPublisherSpecificationBuilder setUseTLS(boolean usetls) {
        endpointSpecification.setUseTLS(usetls);
        return this;
    }

    public MinioPublisherSpecificationBuilder setAccessKey(String accessKey) {
        endpointSpecification.credentialsSpecification.setAccessKey(accessKey);
        return this;
    }

    public MinioPublisherSpecificationBuilder setSecretKey(String secretKey) {
        endpointSpecification.credentialsSpecification.setSecretKey(secretKey);
        return this;
    }

    public MinioPublisherSpecificationBuilder setRegion(String region) {
        endpointSpecification.region = region;
        return this;
    }

    public MinioPublisherSpecificationBuilder setBucketId(String bucketId) {
        bucketSpecification.bucketId = bucketId;
        return this;
    }

    public MinioPublisherSpecificationBuilder setVersioning(VersioningConfiguration.Status versioning) {
        this.versioning = versioning;
        return this;
    }

    public MinioPublisherSpecificationBuilder setBucketPolicy(String bucketPolicy) {
        bucketSpecification.bucketPolicy = bucketPolicy;
        return this;
    }

    public MinioPublisherSpecificationBuilder setDatedFolders(boolean datedFolders) {
        bucketSpecification.partitionByDate = datedFolders;
        return this;
    }

    public MinioPublisherSpecificationBuilder setDateFormat(String dateFormat) {
        bucketSpecification.dateFormat = dateFormat;
        if(dateFormat!= null && !dateFormat.isBlank()){
            bucketSpecification.partitionByDate = true;
        }
        return this;
    }

    public MinioPublisherSpecificationBuilder setRetries(int retries) {
        bucketSpecification.retries = retries;
        return this;
    }

    public MinioPublisherSpecificationBuilder setSerializer(SerializerSpecification serializer) {
        this.serializerSpecification = serializer;
        return this;
    }

    public MinioPublisherSpecificationBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public MinioPublisherSpecificationBuilder setObjectName(String objectName) {
        bucketSpecification.objectName = objectName;
        return this;
    }

    public MinioPublisherSpecificationBuilder setProvider(Class<? extends JouleProviderPlugin> provider) {
        endpointSpecification.credentialsSpecification.setProvidePluginr(provider);
        return this;
    }

    @Override
    public MinioPublisherSpecification build() throws InvalidSpecificationException {
        MinioPublisherSpecification spec = new MinioPublisherSpecification();
        spec.setName((name == null || name.isBlank())? UUID.randomUUID().toString() : name);
        spec.setEnabled(enabled);
        spec.setEndpointSpecification(endpointSpecification);
        spec.setBucketSpecification(bucketSpecification);
        spec.setSerializerSpecification(serializerSpecification);
        spec.setBatchSize(batchSize);
        spec.validate();

        return spec;
    }
}