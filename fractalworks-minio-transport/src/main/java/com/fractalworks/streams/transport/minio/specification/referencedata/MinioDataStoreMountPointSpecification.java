package com.fractalworks.streams.transport.minio.specification.referencedata;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.sdk.storage.Store;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreMountPointSpecification;
import com.fractalworks.streams.transport.minio.MinioReferenceDataStore;
import com.fractalworks.streams.transport.minio.specification.BucketSpecification;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;

/**
 * Minio reference data specification using S3 buckets
 *
 * @author Lyndon Adams
 */
public class MinioDataStoreMountPointSpecification extends BucketSpecification implements DataStoreMountPointSpecification {

    private String name;

    private EndpointSpecification endpointSpecification;

    private String initialVersionId;

    private String localDownloadDirectory = "./tmp/";


    public static MinioDataStoreMountPointSpecificationBuilder getBuilder(){
        return new MinioDataStoreMountPointSpecificationBuilder();
    }

    public MinioDataStoreMountPointSpecification() {
        super();
    }

    public void setEndpointSpecification(EndpointSpecification specification){
        this.endpointSpecification = specification;
    }

    public EndpointSpecification getEndpointSpecification() {
        return endpointSpecification;
    }

    public String getInitialVersionId() {
        return initialVersionId;
    }

    @JsonProperty(value = "initial version Id", required = false)
    public void setInitialVersionId(String initialVersionId) {
        this.initialVersionId = initialVersionId;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getLocalDownloadDirectory() {
        return localDownloadDirectory;
    }

    @JsonProperty(value = "download dir", required = false)
    public void setLocalDownloadDirectory(String localDownloadDirectory) {
        this.localDownloadDirectory = localDownloadDirectory;
    }

    public Class<? extends Store> getComponentClass(){
        return MinioReferenceDataStore.class;
    }

}

