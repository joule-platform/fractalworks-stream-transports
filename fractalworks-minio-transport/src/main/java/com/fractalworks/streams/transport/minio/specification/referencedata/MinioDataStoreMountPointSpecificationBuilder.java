package com.fractalworks.streams.transport.minio.specification.referencedata;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.minio.specification.BucketSpecification;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;

/**
 * MinIO Reference data specification builder
 * <p>
 * Connection defaults:
 * - endpoint localhost:9000
 *
 * @author Lyndon Adams
 */
public class MinioDataStoreMountPointSpecificationBuilder extends AbstractSpecificationBuilder<MinioDataStoreMountPointSpecification> {

    private EndpointSpecification endpointSpecification;
    private BucketSpecification bucketSpecification = new BucketSpecification();
    private String initialVersionId;
    private String localDownloadDirectory = "./tmp/";

    @Override
    public MinioDataStoreMountPointSpecification build() throws InvalidSpecificationException {
        MinioDataStoreMountPointSpecification spec = new MinioDataStoreMountPointSpecification();
        spec.setName(name);
        spec.setEndpointSpecification(endpointSpecification);
        spec.setBucketId(bucketSpecification.getBucketId());
        spec.setInitialVersionId(initialVersionId);
        spec.setObjectName(bucketSpecification.getObjectName());
        spec.setCustomDirectory(bucketSpecification.getCustomDirectory());
        spec.setLocalDownloadDirectory(localDownloadDirectory);
        spec.validate();
        return spec;
    }

    public EndpointSpecification getEndpointSpecification() {
        return endpointSpecification;
    }

    public MinioDataStoreMountPointSpecificationBuilder setEndpointSpecification(EndpointSpecification endpointSpecification) {
        this.endpointSpecification = endpointSpecification;
        return this;
    }

    public MinioDataStoreMountPointSpecificationBuilder setBucketId(String bucket){
        bucketSpecification.setBucketId(bucket);
        return this;
    }

    public MinioDataStoreMountPointSpecificationBuilder setDirectory(String directory){
        bucketSpecification.setCustomDirectory(directory);
        return this;
    }

    public MinioDataStoreMountPointSpecificationBuilder setObjectName(String objectName){
        bucketSpecification.setObjectName(objectName);
        return this;
    }

    public MinioDataStoreMountPointSpecificationBuilder setInitialVersionId(String version){
        initialVersionId = version;
        return this;
    }

    public MinioDataStoreMountPointSpecificationBuilder setName(String name){
        this.name = name;
        return this;
    }

    public MinioDataStoreMountPointSpecificationBuilder setLocalDownloadDirectory(String localDownloadDirectory) {
        this.localDownloadDirectory = localDownloadDirectory;
        return this;
    }
}
