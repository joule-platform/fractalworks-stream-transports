package com.fractalworks.streams.transport.minio.specification.referencedata;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.referencedata.specifications.DataStoreSpecification;
import com.fractalworks.streams.transport.minio.MinioReferenceDataStore;
import com.fractalworks.streams.transport.minio.internal.MinioSession;
import com.fractalworks.streams.transport.minio.internal.MinioSessionFactory;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;

import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "minio stores")
public class MinioDataStoreSpecification extends AbstractTransportSpecification implements DataStoreSpecification {

    private EndpointSpecification endpoint;

    private MinioSession session;

    private Map<String, MinioDataStoreMountPointSpecification> stores;

    private String localDownloadDirectory = "./tmp/";

    public MinioDataStoreSpecification() {
        // Required
    }

    public static MinioDataStoreSpecificationBuilder getBuilder(){
        return new MinioDataStoreSpecificationBuilder();
    }

    @Override
    public void startup() throws TransportException {
        session = MinioSessionFactory.createSession(endpoint, localDownloadDirectory);
        stores.values().parallelStream().forEach(e-> e.setEndpointSpecification(endpoint));
    }

    @Override
    public void close() {
        if(session!=null)
            session.close();
    }


    @JsonProperty(value = "connection", required = true)
    public void setEndpoint(EndpointSpecification endpoint) {
        this.endpoint = endpoint;
    }

    @JsonProperty(value = "stores", required = true)
    public void setStores(Map<String, MinioDataStoreMountPointSpecification> stores) {
        this.stores = stores;
    }

    @Override
    public Map<String,MinioDataStoreMountPointSpecification> getStores() {
       return stores;
    }

    public void setLocalDownloadDirectory(String localDownloadDirectory) {
        this.localDownloadDirectory = localDownloadDirectory;
    }

    @Override
    public Class<MinioReferenceDataStore> getComponentClass() {
        return MinioReferenceDataStore.class;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if(name == null || name.isBlank())
            throw new InvalidSpecificationException("Name must be provided for the logical grouping of data stores");

        if(endpoint == null)
            throw new InvalidSpecificationException("Endpoint configuration must be provided.");

        if(stores == null || stores.isEmpty())
            throw new InvalidSpecificationException("At least one data store must be provided.");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MinioDataStoreSpecification that = (MinioDataStoreSpecification) o;
        return Objects.equals(endpoint, that.endpoint) && Objects.equals(session, that.session) && Objects.equals(stores, that.stores) && Objects.equals(localDownloadDirectory, that.localDownloadDirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), endpoint, session, stores, localDownloadDirectory);
    }
}
