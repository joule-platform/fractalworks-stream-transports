package com.fractalworks.streams.transport.minio.specification.referencedata;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.minio.specification.EndpointSpecification;

import java.util.Map;

/**
 * Minio data store builder
 *
 * @author Lyndon Adams
 */
public class MinioDataStoreSpecificationBuilder extends AbstractSpecificationBuilder<MinioDataStoreSpecification> {

    private EndpointSpecification endpointSpecification;

    private Map<String, MinioDataStoreMountPointSpecification> stores;

    private String localDownloadDirectory = "./tmp/";

    public MinioDataStoreSpecificationBuilder() {
        specificationClass = MinioDataStoreSpecification.class;
    }


    public EndpointSpecification getEndpointSpecification() {
        return endpointSpecification;
    }

    public MinioDataStoreSpecificationBuilder setEndpointSpecification(EndpointSpecification endpointSpecification) {
        this.endpointSpecification = endpointSpecification;
        return this;
    }

    public Map<String, MinioDataStoreMountPointSpecification> getStores() {
        return stores;
    }

    public MinioDataStoreSpecificationBuilder setStores(Map<String, MinioDataStoreMountPointSpecification> stores) {
        this.stores = stores;
        return this;
    }

    public MinioDataStoreSpecificationBuilder setLocalDownloadDirectory(String localDownloadDirectory) {
        this.localDownloadDirectory = localDownloadDirectory;
        return this;
    }

    @Override
    public MinioDataStoreSpecification build() throws InvalidSpecificationException {
        MinioDataStoreSpecification spec = new MinioDataStoreSpecification();
        spec.setEndpoint(endpointSpecification);
        spec.setStores(stores);
        spec.setLocalDownloadDirectory(localDownloadDirectory);
        spec.validate();
        return spec;
    }
}
