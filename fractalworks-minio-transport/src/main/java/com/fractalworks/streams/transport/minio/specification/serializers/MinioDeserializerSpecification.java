package com.fractalworks.streams.transport.minio.specification.serializers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;
import org.apache.arrow.dataset.file.FileFormat;

import java.util.Arrays;
import java.util.Objects;

/**
 * Minio deserialization specification that defines key attributes to read files from S3 buckets
 *
 * @author Lyndon Adams
 */
public class MinioDeserializerSpecification extends DeserializerSpecification {

    private String[] projection;
    private FileFormat fileFormat = FileFormat.PARQUET;

    public MinioDeserializerSpecification() {
        super();
    }

    public String[] getProjection() {
        return projection;
    }

    @JsonProperty(value = "projection", required = false)
    public void setProjection(String[] projection) {
        this.projection = projection;
    }

    public FileFormat getFileFormat() {
        return fileFormat;
    }

    @JsonProperty(value = "format", required = false)
    public void setFileFormat(FileFormat fileFormat) {
        this.fileFormat = fileFormat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MinioDeserializerSpecification that = (MinioDeserializerSpecification) o;
        return Arrays.equals(projection, that.projection) && fileFormat == that.fileFormat;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), fileFormat);
        result = 31 * result + Arrays.hashCode(projection);
        return result;
    }
}
