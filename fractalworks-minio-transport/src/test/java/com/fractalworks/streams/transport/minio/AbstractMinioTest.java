/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.formatters.Formatter;
import com.fractalworks.streams.transport.minio.specification.*;
import com.fractalworks.streams.transport.minio.specification.referencedata.MinioDataStoreMountPointSpecification;

import java.util.Random;

public abstract class AbstractMinioTest {

    static final String[] symbols = {"IBM", "VMW", "MSFT", "PVT"};
    static final int BATCHSIZE = 200000;

    public MinioDataStoreMountPointSpecification getMinioReferenceDataSpecification(String bucket, String directory, String objectName) throws InvalidSpecificationException {

        CredentialsSpecification cs = new CredentialsSpecification();
        cs.setAccessKey("AKIAIOSFODNN7");
        cs.setSecretKey("wJalrXUtnFEMIK7MDENGbPxRfiCY");

        EndpointSpecification es = new EndpointSpecification();
        es.setEndpoint("https://127.0.0.1");
        es.setPort(9000);
        es.setRegion("fernvilla");
        es.setCredentialsSpecification(cs);

        var builder = MinioDataStoreMountPointSpecification.getBuilder();
        builder
            .setEndpointSpecification(es)
            .setBucketId(bucket)
            .setObjectName(objectName)
            .setDirectory(directory);

        return builder.build();
    }

    public MinioPublisherSpecification getMinioSpecification(Formatter formatter) throws InvalidSpecificationException {

        SerializerSpecification serializerSpecification = new SerializerSpecification();
        serializerSpecification.setFormatter(formatter);

        MinioPublisherSpecificationBuilder builder = MinioPublisherSpecification.builder();
        builder
            .setAccessKey("AKIAIOSFODNN7")
            .setSecretKey("wJalrXUtnFEMIK7MDENGbPxRfiCY")
            .setEndpoint("https://127.0.0.1").setPort(9000)
            .setRegion("fernvilla")
            .setBucketId("marketdata")
            .setObjectName("prices")
            .setSerializer(new SerializerSpecification())
            .setDateFormat("yyyyMMdd/HH")
            .setUseTLS(false)
            .setSerializer(serializerSpecification)
            .setEnabled(true)
            .setBatchSize(BATCHSIZE);

        return builder.build();
    }

    public MinioPublisherSpecification getMinioSpecificationWithTLSSecurity() throws InvalidSpecificationException {
        MinioPublisherSpecificationBuilder builder = MinioPublisherSpecification.builder();
        builder
            .setAccessKey("AKIAIOSFODNN7")
            .setSecretKey("wJalrXUtnFEMIK7MDENGbPxRfiCY")
            .setEndpoint("https://127.0.0.1").setPort(9000)
            .setRegion("fernvilla")
            .setBucketId("marketdata")
            .setObjectName("prices")
            .setSerializer(new SerializerSpecification())
            .setDateFormat("yyyyMMdd/HH")
            .setUseTLS(true)                // TLS enabled
            .setBatchSize(BATCHSIZE);

        return builder.build();
    }


    public void publishEvents(MinioPublisherTransport transport, int numEvents) throws InterruptedException {
        Random r = new Random();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < numEvents; i++) {
            StreamEvent e = new StreamEvent("marketdata", startTime++);
            e.addValue("symbol", symbols[i % symbols.length]);
            e.addValue("price", r.nextDouble());
            e.addValue("vol", r.nextInt(10000));
            e.addValue("bestprice", r.nextDouble());
            transport.onEvent(e, null);
        }
    }
}
