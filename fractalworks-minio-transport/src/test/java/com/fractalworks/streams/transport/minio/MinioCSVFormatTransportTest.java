/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.sdk.formatters.CSVStreamEventFormatter;
import org.junit.jupiter.api.Disabled;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * MinIO CSV format publishing test
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class MinioCSVFormatTransportTest extends AbstractMinioTest {

    @Test
    public void publishCSVDataTest() throws Exception {
        var formatter = new CSVStreamEventFormatter();
        MinioPublisherTransport transport = new MinioPublisherTransport(getMinioSpecification(formatter));
        transport.initialize();
        publishEvents(transport, BATCHSIZE);

        transport.flush();
        assertTrue(true);
    }

}
