/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.sdk.formatters.JsonStreamEventFormatter;
import org.junit.Assert;
import org.junit.jupiter.api.Disabled;
import org.junit.Test;

/**
 * MinIO JSON format publishing test
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class MinioJSONFormatTransportTest extends AbstractMinioTest {

    @Test
    public void publishJsonDataTest() throws Exception {
        MinioPublisherTransport transport = new MinioPublisherTransport(getMinioSpecification(new JsonStreamEventFormatter()));
        transport.initialize();
        publishEvents(transport, BATCHSIZE);
        transport.flush();
        Assert.assertTrue(true);
    }

}
