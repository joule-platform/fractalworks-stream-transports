/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.formatters.ParquetStreamEventFormatter;
import com.fractalworks.streams.transport.minio.internal.MinioSessionFactory;
import com.fractalworks.streams.transport.minio.specification.MinioConsumerSpecification;
import com.fractalworks.streams.transport.minio.specification.MinioConsumerSpecificationBuilder;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecification;
import io.minio.DownloadObjectArgs;
import io.minio.errors.*;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Disabled;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * MinIO parquet format publishing test
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class MinioParquetFormatConsumerTransportTest extends AbstractMinioTest {

    public static String tmpDir = "./tmp";

    @Test
    @Order(1)
    public void publishParquetDataTest() throws Exception {
        var formatter = new ParquetStreamEventFormatter();
        formatter.setSchemaPath("avro/marketDataSchema.avsc");
        formatter.setTmpFileDir(tmpDir);
        formatter.setDisableCrcFileCreation(false);

        MinioPublisherTransport transport = new MinioPublisherTransport(getMinioSpecification(formatter));
        transport.initialize();
        publishEvents(transport, BATCHSIZE);
        transport.flush();

        FileUtils.deleteQuietly(new File(tmpDir));
        Assert.assertTrue(true);
    }

    @Test
    @Order(2)
    public void consumeParquetDataUsingYamlConfigTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        MinioConsumerSpecificationBuilder builder = new MinioConsumerSpecificationBuilder();
        MinioConsumerSpecification spec = (MinioConsumerSpecification) builder.build(new File(classLoader.getResource("dsl/validConsumeMiniIOParquetEvents.yaml").getFile()));
        MinioConsumerTransport consumerTransport = new MinioConsumerTransport(spec);
        consumerTransport.setTransportListener(collection -> collection.forEach(System.out::println));
        consumerTransport.initialize();
        consumerTransport.start();
        Assert.assertTrue(true);
    }


    @Test
    @Order(3)
    public void downloadObject() throws InvalidSpecificationException, TransportException, ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        MinioPublisherSpecification spec = getMinioSpecification(null);
        var session = MinioSessionFactory.createSession(spec.getEndpointSpecification(),tmpDir);
        session.getClient().downloadObject(DownloadObjectArgs.builder()
                .bucket("marketdata")
                .object("20230720/19/prices-1691655182540.parquet")
                .filename("./prices-1691655182540.parquet")
                .build());

        File file = new File("./prices-1691655182540.parquet");
        assertTrue(file.exists(),"File not downloaded");
        FileUtils.deleteQuietly(file);
        Assert.assertTrue(true);
    }
}
