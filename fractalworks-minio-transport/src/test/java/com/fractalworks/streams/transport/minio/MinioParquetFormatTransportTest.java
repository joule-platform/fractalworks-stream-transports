/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.sdk.formatters.ParquetStreamEventFormatter;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecification;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecificationBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * MinIO parquet format publishing test
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class MinioParquetFormatTransportTest extends AbstractMinioTest {

    public static String tmpDir = "./tmp";

    @Test
    @Order(1)
    public void publishParquetDataTest() throws Exception {

        var formatter = new ParquetStreamEventFormatter();
        formatter.setSchemaPath("avro/marketDataSchema.avsc");
        formatter.setTmpFileDir(tmpDir);
        formatter.setDisableCrcFileCreation(false);

        MinioPublisherTransport transport = new MinioPublisherTransport(getMinioSpecification(formatter));
        transport.initialize();
        publishEvents(transport, BATCHSIZE);
        transport.flush();

        FileUtils.deleteQuietly(new File(tmpDir));
        assertTrue(true);
    }

    @Test
    @Order(2)
    public void publishParquetDataUsingYamlConfigTest() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validPublishingMiniIOParquetEvents.yaml").getFile());

        MinioPublisherSpecificationBuilder builder = new MinioPublisherSpecificationBuilder();
        MinioPublisherSpecification spec = (MinioPublisherSpecification) builder.build(file);

        MinioPublisherTransport transport = new MinioPublisherTransport(spec);
        transport.initialize();
        publishEvents(transport, BATCHSIZE);
        transport.flush();

        FileUtils.deleteQuietly(new File(tmpDir));
        assertTrue(true);
    }
}
