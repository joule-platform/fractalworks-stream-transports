/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio;

import com.fractalworks.streams.sdk.formatters.CSVStreamEventFormatter;
import org.junit.jupiter.api.Disabled;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import static org.junit.Assert.assertTrue;

/**
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class MinioTransportTest extends AbstractMinioTest {

    @Test
    @Order(1)
    public void connectivityTest() throws Exception {
        MinioPublisherTransport transport = new MinioPublisherTransport(getMinioSpecification(new CSVStreamEventFormatter()));
        transport.initialize();
        assertTrue(true);
    }

    @Test
    @Order(2)
    public void publishDataAsCSVTest() throws Exception {
        MinioPublisherTransport transport = new MinioPublisherTransport(getMinioSpecification( new CSVStreamEventFormatter()));
        transport.initialize();
        publishEvents(transport, BATCHSIZE);
        assertTrue(true);
    }
}
