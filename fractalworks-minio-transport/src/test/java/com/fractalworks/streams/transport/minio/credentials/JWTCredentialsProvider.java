package com.fractalworks.streams.transport.minio.credentials;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.CustomPluginException;
import com.fractalworks.streams.transport.minio.JouleProviderPlugin;
import io.minio.credentials.Provider;

import java.util.Properties;

/**
 * Basic example of Provider interface
 */
public class JWTCredentialsProvider implements JouleProviderPlugin {
    @Override
    public Provider getProvider() {
        return null;
    }

    @Override
    public void initialize() throws CustomPluginException {
    }

    @Override
    public void validate() throws InvalidSpecificationException {
    }

    @Override
    public void setProperties(Properties properties) {
    }
}
