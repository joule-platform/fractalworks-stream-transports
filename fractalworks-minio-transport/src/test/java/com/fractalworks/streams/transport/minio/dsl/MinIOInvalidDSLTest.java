/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.dsl;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecification;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecificationBuilder;
import org.junit.Test;

import java.io.File;

/**
 * Test invalid MinIO DSL
 *
 * @author Lyndon Adams
 */
public class MinIOInvalidDSLTest {

    @Test(expected = InvalidSpecificationException.class)
    public void invalidFilePublisherWithSSEYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/invalidPublishingMiniIOParquetEventsWithProvider.yaml").getFile());

        MinioPublisherSpecificationBuilder builder = new MinioPublisherSpecificationBuilder();
        MinioPublisherSpecification spec = (MinioPublisherSpecification) builder.build(file);
    }

}
