/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.minio.dsl;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresBuilder;
import com.fractalworks.streams.sdk.referencedata.specifications.ReferenceDataStoresSpecification;
import com.fractalworks.streams.transport.minio.specification.*;
import com.fractalworks.streams.transport.minio.specification.MinioConsumerSpecificationBuilder;
import com.fractalworks.streams.transport.minio.specification.MinioPublisherSpecificationBuilder;
import com.fractalworks.streams.transport.minio.specification.referencedata.MinioDataStoreSpecification;
import com.fractalworks.streams.transport.minio.specification.referencedata.MinioDataStoreSpecificationBuilder;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static org.junit.Assert.assertNotNull;

/**
 * Test valid MinIO DSL
 *
 * @author Lyndon Adams
 */
public class MinIOValidDSLTest {

    @Test
    public void validPublishingMiniIOParquetEventsYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("dsl/validPublishingMiniIOParquetEvents.yaml")).getFile());

        MinioPublisherSpecificationBuilder builder = new MinioPublisherSpecificationBuilder();
        MinioPublisherSpecification spec = (MinioPublisherSpecification) builder.build(file);
        assertNotNull(spec);
    }

    @Test
    public void validPublishingMiniIOParquetEventsWithProviderYamlTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("dsl/validPublishingMiniIOParquetEventsWithProvider.yaml")).getFile());
        MinioPublisherSpecificationBuilder builder = new MinioPublisherSpecificationBuilder();
        MinioPublisherSpecification spec = (MinioPublisherSpecification) builder.build(file);
        assertNotNull(spec);
    }

    @Test
    public void validConsumerMiniIOParquetEventsTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("dsl/validConsumeMiniIOParquetEvents.yaml")).getFile());
        MinioConsumerSpecificationBuilder builder = new MinioConsumerSpecificationBuilder();
        MinioConsumerSpecification spec = (MinioConsumerSpecification) builder.build(file);
        assertNotNull(spec);
    }

    @Test
    public void validMinioPlatformStoresTest() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("dsl/validMinioPlatformStores.yaml")).getFile());
        MinioDataStoreSpecificationBuilder builder = new MinioDataStoreSpecificationBuilder();
        MinioDataStoreSpecification spec = (MinioDataStoreSpecification) builder.build(file);
        assertNotNull(spec);
    }

    @Test
    public void validMinioPlatformStoreYamlTest() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("dsl/validCompleteMinioPlatformStores.yaml").getFile());
        ReferenceDataStoresBuilder builder = new ReferenceDataStoresBuilder();
        ReferenceDataStoresSpecification referDataSources = builder.build(file);
        assertNotNull(referDataSources);
    }

}
