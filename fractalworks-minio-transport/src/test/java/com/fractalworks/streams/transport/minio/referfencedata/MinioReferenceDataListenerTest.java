package com.fractalworks.streams.transport.minio.referfencedata;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.referencedata.S3ReferenceDataEvent;
import com.fractalworks.streams.transport.minio.AbstractMinioTest;
import com.fractalworks.streams.transport.minio.MinioReferenceDataStore;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.awaitility.Awaitility.await;

@Disabled("Comment out when running locally")
public class MinioReferenceDataListenerTest extends AbstractMinioTest {

    static String BUCKET = "models";
    static String DIRECTORY = "iris/";
    static String MODEL_FILE = "iris_rf.pmml";

    @Test
    public void testBucketListerTrigger() throws InvalidSpecificationException, TransportException {
        AtomicBoolean receivedEvent = new AtomicBoolean(false);
        var spec = getMinioReferenceDataSpecification(BUCKET,DIRECTORY, MODEL_FILE);
        MinioReferenceDataStore store = new MinioReferenceDataStore();
        store.setSpecification(spec);
        store.initialize();

        store.registerUpdateListener((event, context) -> {
            S3ReferenceDataEvent e = (S3ReferenceDataEvent)event;
            System.out.printf("Received %s event for %s object on bucket %s%n",e.getS3EventType(), e.getObjectname(), e.getBucket());
            receivedEvent.set(true);
        });

        await()
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .until(checkForEvents(receivedEvent));
    }

    private Callable<Boolean> checkForEvents(AtomicBoolean eventReceived) {
        return new Callable<>() {
            public Boolean call() throws Exception {
                return eventReceived.get();
            }
        };
    }

    @After
    public void cleanup(){
        FileUtils.deleteQuietly(new File(MODEL_FILE));
    }
}
