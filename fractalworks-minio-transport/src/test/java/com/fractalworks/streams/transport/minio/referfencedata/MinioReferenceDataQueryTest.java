package com.fractalworks.streams.transport.minio.referfencedata;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.StorageException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.minio.AbstractMinioTest;
import com.fractalworks.streams.transport.minio.MinioReferenceDataStore;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.util.*;

import static org.junit.Assert.assertNotNull;

@Disabled("Comment out when running locally")
public class MinioReferenceDataQueryTest extends AbstractMinioTest {

    static String BUCKET = "models";
    static String DIRECTORY = "iris/";
    static String MODEL_FILE = "iris_rf.pmml";

    @Test
    public void testLoadInitialImage() throws InvalidSpecificationException, StorageException, TransportException {
        var spec = getMinioReferenceDataSpecification(BUCKET,DIRECTORY, null);

        MinioReferenceDataStore store = new MinioReferenceDataStore();
        store.setSpecification(spec);
        store.initialize();

        var response = store.getInitialImage(MODEL_FILE);
        if(response.isPresent()){
            var dr = response.get();
            assertNotNull("Failed to get data", dr.getData());
        }
    }

    @Test
    public void testQueryForData() throws InvalidSpecificationException, TransportException {
        var spec = getMinioReferenceDataSpecification(BUCKET,DIRECTORY, null);

        MinioReferenceDataStore store = new MinioReferenceDataStore();
        store.setSpecification(spec);
        store.initialize();

        var response = store.query(MODEL_FILE);
        if(response.isPresent()){
            var dr = response.get();
            assertNotNull("Failed to get data", dr.getData());
        }
    }

    @Test
    public void testQueryForVersionedData() throws InvalidSpecificationException, TransportException {
        var spec = getMinioReferenceDataSpecification(BUCKET, DIRECTORY, null);

        MinioReferenceDataStore store = new MinioReferenceDataStore();
        store.setSpecification(spec);
        store.initialize();

        var response = store.query(MODEL_FILE,  Collections.singletonMap("versionId", ""));
        if(response.isPresent()){
            var dr = response.get();
            assertNotNull("Failed to get data", dr.getData());
        }
    }

    @After
    public void cleanup(){
        FileUtils.deleteQuietly(new File(MODEL_FILE));
    }
}
