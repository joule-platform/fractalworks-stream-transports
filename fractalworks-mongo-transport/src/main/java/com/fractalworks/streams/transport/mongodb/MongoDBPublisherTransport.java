/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mongodb;


import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.transport.mongodb.specification.MongoDBPublisherSpecification;
import com.fractalworks.streams.transport.mongodb.specification.serialization.MongoSerializerSpecification;
import com.fractalworks.streams.transport.mongodb.specification.serialization.MongoStreamEventFormatter;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.InsertManyOptions;
import com.mongodb.client.model.InsertOneOptions;
import com.mongodb.connection.ClusterSettings;
import com.mongodb.connection.SslSettings;
import org.bson.Document;

import javax.net.ssl.SSLContext;
import java.lang.reflect.InvocationTargetException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * MongoDB publisher transport
 *
 * @author Lyndon Adams
 */
public class MongoDBPublisherTransport extends AbstractPublisherTransport {

    private MongoClient mongoClient;
    private MongoCollection<Document> collection;
    private boolean isOrderedInserts = true;
    private boolean isBatch = true;
    private CustomTransformer<?> transformer;

    public MongoDBPublisherTransport() {
        super();
        formatterSpecification = new MongoStreamEventFormatter();
    }

    public MongoDBPublisherTransport(MongoDBPublisherSpecification specification) {
        super(specification);
        formatterSpecification = new MongoStreamEventFormatter();
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();

        MongoDBPublisherSpecification spec = (MongoDBPublisherSpecification) specification;
        MongoSerializerSpecification serializerSpecification = spec.getSerializerSpecification();
        isOrderedInserts = spec.isOrderedInserts();

        ClusterSettings clusterSettings = ClusterSettings.builder()
                .hosts( spec.getServerList())
                .build();

        MongoClientSettings settings = MongoClientSettings.builder()
                .applyToClusterSettings( b -> b.applySettings(clusterSettings))
                .credential(spec.getCredentialSpecification().buildMongoCredential())
                .applyToSslSettings(b -> b.applySettings( buildSSLSettings(spec)))
                .build();

        mongoClient = MongoClients.create(settings);

        MongoDatabase database = mongoClient.getDatabase(spec.getDatabase());
        collection = database.getCollection(spec.getCollectionName());

        if( serializerSpecification!=null){
            if (serializerSpecification.getTransformer() != null) {
                try {
                    transformer = serializerSpecification.getTransformer().getConstructor().newInstance();
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException  e) {
                    throw new TransportInitializationException("Failed to create transformer instance.", e);
                }
            } else {
                formatterSpecification = serializerSpecification.getFormatter();
                formatterSpecification.initialise();
            }
            isBatch = serializerSpecification.isBatch();
        }

        if (logger.isInfoEnabled()) {
            logger.info(String.format("MongoDB collection [%s] ready to receive events.", spec.getCollectionName()));
        }
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        try {
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Publishing %d events to MongoDB collection %s.", events.size(), ((MongoDBPublisherSpecification) specification).getCollectionName()));
                }

                final List<Document> documents = createDocuments(events);
                if( isBatch ) {
                    publishAsBatchDocuments(documents);
                } else {
                    publishAsSingleDocuments(documents);
                }
            }
        } catch (Exception e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            if(logger.isErrorEnabled())
                logger.error("Error during event publishing to MongoDB.", e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    /**
     * Publishes a list of documents as individual documents to the MongoDB collection.
     *
     * @param documents List of documents to publish to MongoDB.
     */
    private void publishAsSingleDocuments(final List<Document> documents) {
        documents.forEach(d-> {
            InsertOneOptions oneOptions = new InsertOneOptions();
            oneOptions.bypassDocumentValidation(true);
            var result = collection.insertOne(d, oneOptions);
            if(!result.wasAcknowledged()){
                if(logger.isErrorEnabled())
                    logger.error("Write not acknowledged");
            } else {
                metrics.incrementMetric(Metric.PROCESSED);
            }
        });
    }

    /**
     * Publishes a list of documents as batch documents to the MongoDB collection.
     *
     * @param documents the list of documents to publish
     */
    private void publishAsBatchDocuments(final List<Document> documents) {
        InsertManyOptions manyOptions = new InsertManyOptions();
        manyOptions.ordered(isOrderedInserts);
        manyOptions.bypassDocumentValidation(true);

        var response = collection.insertMany(documents, manyOptions);
        if (!response.wasAcknowledged()) {
            if(logger.isErrorEnabled())
                logger.error("Write not acknowledged");
        } else {
            metrics.incrementMetric(Metric.PROCESSED, response.getInsertedIds().size());
        }
    }

    /**
     * Creates a list of documents based on the given collection of stream events.
     *
     * @param events a collection of stream events
     * @return a list of documents
     */
    private List<Document> createDocuments(Collection<StreamEvent> events){
        final List<Document> documents = new ArrayList<>();
        if(transformer!=null){
            try {
                documents.addAll( (Collection<Document>)transformer.transform(events));
            } catch (TranslationException e) {
                logger.error("Failure during custom transforming processes.", e);
            }
        } else {
            if(!isOrderedInserts) {
                documents.addAll( (Collection<Document>)formatterSpecification.format(events) );
            } else {
                events.forEach(evt -> documents.add( (Document) formatterSpecification.format(evt)));
            }
        }
        return documents;
    }

    private SslSettings buildSSLSettings(MongoDBPublisherSpecification spec) {
        final SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance(spec.getSslProtocol());
            sslContext.init(null, null, null);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("No such Algorithm is supported ",  e);
        } catch (KeyManagementException e) {
            throw new IllegalStateException("KeyManagementException ", e);
        }
        return SslSettings.builder()
                .context(sslContext)
                .enabled(spec.isSslEnabled())
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MongoDBPublisherTransport that = (MongoDBPublisherTransport) o;
        return isOrderedInserts == that.isOrderedInserts && isBatch == that.isBatch;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isOrderedInserts, isBatch);
    }

    @Override
    public void shutdown() {
        super.shutdown();
        mongoClient.close();
    }
}
