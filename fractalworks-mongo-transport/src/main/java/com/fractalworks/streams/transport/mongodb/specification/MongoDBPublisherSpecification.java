/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport.mongodb.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.mongodb.MongoDBPublisherTransport;
import com.fractalworks.streams.transport.mongodb.specification.auth.CredentialSpecification;
import com.fractalworks.streams.transport.mongodb.specification.serialization.MongoSerializerSpecification;
import com.mongodb.ServerAddress;

import java.util.*;

/**
 * MongoDB publishing specification.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "mongodbPublisher")
public class MongoDBPublisherSpecification extends AbstractTransportSpecification {

    private List<ServerAddress> serverList = new ArrayList<>();

    private Map<String, Integer> servers;

    private CredentialSpecification credentialSpecification;

    private MongoSerializerSpecification serializerSpecification = new MongoSerializerSpecification();

    private String database;
    private String collectionName;
    private boolean orderedInserts = true;

    private String sslProtocol = "TLSv1.2";
    private boolean sslEnabled = false;

    public MongoDBPublisherSpecification() {
        batchSize = 1000;
        sendTimeout = 250;
    }

    public MongoDBPublisherSpecification(String name) {
        super(name);
        batchSize = 1000;
        sendTimeout = 250;
    }

    public List<ServerAddress> getServerList() {
        return serverList;
    }

    @JsonProperty(value = "servers", required = true)
    public void setServers(Map<String, Integer> servers){
        this.servers = servers;
    }

    public void setServerList(List<ServerAddress> serverList) {
        this.serverList = serverList;
    }

    public CredentialSpecification getCredentialSpecification() {
        return credentialSpecification;
    }

    @JsonProperty(value = "credentials", required = true)
    public void setCredentialSpecification(CredentialSpecification credentialSpecification) {
        this.credentialSpecification = credentialSpecification;
    }

    public String getDatabase() {
        return database;
    }

    @JsonProperty(value = "database", required = true)
    public void setDatabase(String database) {
        this.database = database;
    }

    public String getCollectionName() {
        return collectionName;
    }

    @JsonProperty(value = "collection", required = true)
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public boolean isOrderedInserts() {
        return orderedInserts;
    }

    @JsonProperty(value = "ordered inserts")
    public void setOrderedInserts(boolean orderedInserts) {
        this.orderedInserts = orderedInserts;
    }

    public boolean isSslEnabled() {
        return sslEnabled;
    }

    @JsonProperty(value = "enable ssl")
    public void setSslEnabled(boolean sslEnabled) {
        this.sslEnabled = sslEnabled;
    }

    public String getSslProtocol() {
        return sslProtocol;
    }

    @JsonProperty(value = "ssl protocol")
    public void setSslProtocol(String sslProtocol) {
        this.sslProtocol = sslProtocol;
    }

    public MongoSerializerSpecification getSerializerSpecification() {
        return serializerSpecification;
    }

    @JsonProperty(value = "serializer")
    public void setSerializerSpecification(MongoSerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();

        if (servers == null || servers.isEmpty()) {
            throw new InvalidSpecificationException("servers must be provided.");
        } else{
            servers.entrySet().stream().forEach( e->
                serverList.add(new ServerAddress(e.getKey(), e.getValue()))
            );
        }

        if(credentialSpecification == null ){
            throw new InvalidSpecificationException("database must be provided.");
        }
        credentialSpecification.validate();

        if (database == null || database.isEmpty()) {
            throw new InvalidSpecificationException("database must be provided.");
        }

        if (collectionName == null || collectionName.isEmpty()) {
            throw new InvalidSpecificationException("collectionName must be provided.");
        }

        if(serializerSpecification == null ){
            throw new InvalidSpecificationException("serializer must be provided.");
        }
        serializerSpecification.validate();
    }


    @Override
    public Class<? extends Transport> getComponentClass() {
        return MongoDBPublisherTransport.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MongoDBPublisherSpecification that = (MongoDBPublisherSpecification) o;
        return orderedInserts == that.orderedInserts && sslEnabled == that.sslEnabled && Objects.equals(serverList, that.serverList) && Objects.equals(servers, that.servers) && Objects.equals(credentialSpecification, that.credentialSpecification) && Objects.equals(serializerSpecification, that.serializerSpecification) && Objects.equals(database, that.database) && Objects.equals(collectionName, that.collectionName) && Objects.equals(sslProtocol, that.sslProtocol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), serverList, servers, credentialSpecification, serializerSpecification, database, collectionName, orderedInserts, sslProtocol, sslEnabled);
    }

    @Override
    public String toString() {
        return "MongoDBPublisherSpecification{" +
                "serverList=" + serverList +
                ", servers=" + servers +
                ", credentialSpecification=" + credentialSpecification +
                ", serializerSpecification=" + serializerSpecification +
                ", database='" + database + '\'' +
                ", collectionName='" + collectionName + '\'' +
                ", orderedInserts=" + orderedInserts +
                ", sslProtocol='" + sslProtocol + '\'' +
                ", sslEnabled=" + sslEnabled +
                '}';
    }
}
