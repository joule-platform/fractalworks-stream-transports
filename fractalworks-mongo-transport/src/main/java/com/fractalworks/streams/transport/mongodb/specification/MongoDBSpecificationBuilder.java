/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mongodb.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.mongodb.specification.auth.CredentialSpecification;
import com.fractalworks.streams.transport.mongodb.specification.serialization.MongoSerializerSpecification;
import com.mongodb.ServerAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * MongoDB specification builder
 *
 * @author Lyndon Adams
 */
public class MongoDBSpecificationBuilder extends AbstractSpecificationBuilder<MongoDBPublisherSpecification> {

    private int batchSize = 100;
    private long timeout = 500;
    private final List<ServerAddress> serverList = new ArrayList<>();

    private String database;
    private String collectionName;

    private boolean orderedInserts = true;
    private boolean sslEnabled;

    private MongoSerializerSpecification serializerSpecification = new MongoSerializerSpecification();

    private final CredentialSpecification credentialSpecification = new CredentialSpecification();

    public MongoDBSpecificationBuilder() {
        specificationClass = MongoDBPublisherSpecification.class;
    }

    public MongoDBSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public MongoDBSpecificationBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public MongoDBSpecificationBuilder setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public MongoDBSpecificationBuilder addServerConfig(String serverAddress, int port) {
        serverList.add(new ServerAddress(serverAddress, port));
        return this;
    }

    public MongoDBSpecificationBuilder setUser(String user) {
        credentialSpecification.setUser(user);
        return this;
    }

    public MongoDBSpecificationBuilder setPassword(String password) {
        credentialSpecification.setPassword(password);
        return this;
    }

    public MongoDBSpecificationBuilder setAuthenticationDatabase(String authenticationDatabase) {
        credentialSpecification.setAuthenticationDatabase(authenticationDatabase);
        return this;
    }

    public MongoDBSpecificationBuilder setDatabase(String database) {
        this.database = database;
        return this;
    }

    public MongoDBSpecificationBuilder setCollectionName(String collectionName) {
        this.collectionName = collectionName;
        return this;
    }

    public MongoDBSpecificationBuilder setOrderedInserts(boolean orderedInserts) {
        this.orderedInserts = orderedInserts;
        return this;
    }

    public MongoDBSpecificationBuilder setSslEnabled(boolean sslEnabled) {
        this.sslEnabled = sslEnabled;
        return this;
    }

    public MongoDBSpecificationBuilder setSerializerSpecification(MongoSerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
        return this;
    }

    @Override
    public MongoDBPublisherSpecification build() throws InvalidSpecificationException {
        MongoDBPublisherSpecification spec = new MongoDBPublisherSpecification(name);
        spec.setServerList(serverList);
        spec.setDatabase(database);
        spec.setCollectionName(collectionName);
        spec.setOrderedInserts(orderedInserts);
        spec.setSslEnabled(sslEnabled);
        spec.setBatchSize(batchSize);
        spec.setSendTimeout(timeout);
        spec.setCredentialSpecification(credentialSpecification);
        spec.setSerializerSpecification(serializerSpecification);
        spec.validate();

        return spec;
    }
}
