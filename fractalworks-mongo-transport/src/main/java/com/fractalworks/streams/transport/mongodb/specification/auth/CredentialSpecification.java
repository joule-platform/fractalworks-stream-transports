/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport.mongodb.specification.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.mongodb.MongoCredential;

import java.util.Arrays;
import java.util.Objects;

/**
 * Credential specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "credentials")
public class CredentialSpecification {

    private String user;
    private char[] password;
    private String authenticationDatabase = "admin";

    private CredentialMechanism mechanism = CredentialMechanism.SCRAM_SHA_1;

    public CredentialSpecification() {
        // REQUIRED
    }

    public String getAuthenticationDatabase() {
        return authenticationDatabase;
    }

    @JsonProperty(value = "authentication database", required = false)
    public void setAuthenticationDatabase(String authenticationDatabase) {
        this.authenticationDatabase = authenticationDatabase;
    }

    public String getUser() {
        return user;
    }

    @JsonProperty(value = "user", required = true)
    public void setUser(String user) {
        this.user = user;
    }

    @JsonProperty(value = "password", required = true)
    public void setPassword(String password) {
        this.password = password.toCharArray();
    }

    public CredentialMechanism getMechanism() {
        return mechanism;
    }

    @JsonProperty(value = "mechanism")
    public void setMechanism(CredentialMechanism mechanism) {
        this.mechanism = mechanism;
    }

    public MongoCredential buildMongoCredential() {
        MongoCredential credential;
        switch (mechanism){
            case SCRAM_SHA_1 -> credential = MongoCredential.createScramSha1Credential(user, getAuthenticationDatabase(), password);
            case SCRAM_SHA_256 -> credential = MongoCredential.createScramSha256Credential(user, getAuthenticationDatabase(), password);
            default -> credential = MongoCredential.createPlainCredential(user, getAuthenticationDatabase(), password);
        }
        return credential;
    }

    public void validate() throws InvalidSpecificationException {
        if (user == null || user.isEmpty()) {
            throw new InvalidSpecificationException("user must be provided.");
        }

        if (password == null) {
            throw new InvalidSpecificationException("password must be provided.");
        }

        if (authenticationDatabase == null || authenticationDatabase.isEmpty()) {
            throw new InvalidSpecificationException("authentication database must be provided.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CredentialSpecification that = (CredentialSpecification) o;
        return Objects.equals(user, that.user) && Arrays.equals(password, that.password) && Objects.equals(authenticationDatabase, that.authenticationDatabase) && mechanism == that.mechanism;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(user, authenticationDatabase, mechanism);
        result = 31 * result + Arrays.hashCode(password);
        return result;
    }
}
