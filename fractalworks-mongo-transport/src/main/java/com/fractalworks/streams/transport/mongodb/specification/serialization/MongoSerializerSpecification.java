/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport.mongodb.specification.serialization;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;
import com.fractalworks.streams.sdk.formatters.Formatter;


import java.util.Objects;

/**
 * Serialization specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "serializer")
public class MongoSerializerSpecification {

    private Class<? extends CustomTransformer> transformer;
    private Formatter formatter = new MongoStreamEventFormatter();

    private boolean batch = true;

    public MongoSerializerSpecification() {
        // Required
    }

    public Class<? extends CustomTransformer> getTransformer() {
        return transformer;
    }

    @JsonProperty(value = "transform")
    public void setTransformer(Class<? extends CustomTransformer> transformer) {
        this.transformer = transformer;
    }

    public Formatter getFormatter() {
        return formatter;
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    public boolean isBatch() {
        return batch;
    }

    @JsonProperty(value = "batch")
    public void setBatch(boolean batch) {
        this.batch = batch;
    }

    public void validate() throws InvalidSpecificationException {
        if((transformer== null && formatter == null) || (transformer!= null && formatter != null)) {
            throw new InvalidSpecificationException("Either a custom transform implementation or formatter must be provided ");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MongoSerializerSpecification that = (MongoSerializerSpecification) o;
        return batch == that.batch && Objects.equals(transformer, that.transformer) && Objects.equals(formatter, that.formatter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transformer, formatter, batch);
    }

    @Override
    public String toString() {
        return "MongoSerializerSpecification{" +
                "transformer=" + transformer +
                ", formatter=" + formatter +
                ", batch=" + batch +
                '}';
    }
}
