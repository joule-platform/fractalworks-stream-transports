/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport.mongodb.specification.serialization;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.formatters.AbstractFormatterSpecification;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Collection;

/**
 * MongoDB StreamEvent formatter
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "documentFormatter")
public class MongoStreamEventFormatter extends AbstractFormatterSpecification {

    public MongoStreamEventFormatter() {
        super();
        contentType = "application/octet-stream";
        encoding = "base64";
    }

    @Override
    public Object format(StreamEvent event) {
        Document doc = new Document();
        doc.put("eventType", event.getEventType());
        if (event.getSubType() != null) {
            doc.put("subType", event.getSubType());
        }
        doc.put("ingestTime", event.getIngestTime());
        doc.put("eventTime", event.getEventTime());
        event.getDictionary().entrySet().forEach(entry -> doc.put(entry.getKey(), entry.getValue()));
        return doc;
    }

    @Override
    public Object format(Collection<StreamEvent> events) {
        Collection<Document> documents = new ArrayList<>();
        events.parallelStream().forEach( e -> documents.add((Document) format(e) ) );
        return documents;
    }

    @Override
    public Class<? extends AbstractFormatterSpecification> getComponentClass() {
        return MongoStreamEventFormatter.class;
    }
}
