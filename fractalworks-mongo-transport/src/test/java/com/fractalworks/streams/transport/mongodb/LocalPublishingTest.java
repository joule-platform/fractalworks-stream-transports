package com.fractalworks.streams.transport.mongodb;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.mongodb.specification.MongoDBPublisherSpecification;
import com.fractalworks.streams.transport.mongodb.specification.MongoDBSpecificationBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@Disabled("Comment out when running locally")
public class LocalPublishingTest {

    private static final int EVENT_COUNT = 1000;
    public static Collection<StreamEvent> events;

    @BeforeClass
    public static void createStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT-1; i++){
            StreamEvent evt = new StreamEvent();
            evt.setEventType("customer");
            evt.addValue("customerId", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("city", "london" );
            events.add( evt);
        }
        StreamEvent evt = new StreamEvent();
        evt.setEventType("customer");
        evt.addValue("customerId", EVENT_COUNT);
        evt.addValue("firstname", "Lyndon");
        evt.addValue("surname", "Adams");
        evt.addValue("city", "london" );
        events.add( evt);
    }

    @Test
    public void localPublish() throws InvalidSpecificationException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("valid/validBasicPublisher.yaml").getFile());

        MongoDBSpecificationBuilder builder = new MongoDBSpecificationBuilder();
        MongoDBPublisherSpecification spec = (MongoDBPublisherSpecification) builder.build(file);

        MongoDBPublisherTransport transport = new MongoDBPublisherTransport(spec);
        transport.initialize();

        transport.publish(events);
        assertTrue(true);
    }
}
