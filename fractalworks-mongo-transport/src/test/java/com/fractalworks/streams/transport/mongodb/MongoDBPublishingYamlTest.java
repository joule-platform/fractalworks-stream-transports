package com.fractalworks.streams.transport.mongodb;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.mongodb.specification.MongoDBPublisherSpecification;
import com.fractalworks.streams.transport.mongodb.specification.MongoDBSpecificationBuilder;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertNotNull;

public class MongoDBPublishingYamlTest {

    @Test
    public void basicPublishingYaml() throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("valid/validBasicPublisher.yaml").getFile());

        MongoDBSpecificationBuilder builder = new MongoDBSpecificationBuilder();
        MongoDBPublisherSpecification spec = (MongoDBPublisherSpecification) builder.build(file);
        assertNotNull(spec);
    }

}
