db.createUser({
    user: 'joule',
    pwd: 'password',
    roles: [
        {
            role: 'readWrite',
            db: 'customerdb',
        },
    ],
});