/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.transport.mqtt.specification.AbstractMQTTSpecification;
import com.fractalworks.streams.transport.mqtt.specification.MQTTLastWillSpecification;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecification;
import com.fractalworks.streams.transport.security.JwtClaimSpecification;
import com.fractalworks.streams.transport.security.SSLSpecification;
import com.fractalworks.streams.transport.security.SecuritySpecification;

import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.eclipse.paho.mqttv5.common.packet.UserProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

import org.eclipse.paho.mqttv5.client.*;

/**
 *  MQTT v3.1.1 client factory
 *
 * @author Lyndon Adams
 */
public class MQTTClientFactory {

    private static final Map<String, MqttClient> mqttClients = new HashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(MQTTClientFactory.class.getName());

    private MQTTClientFactory(){
        // Default
    }

    /**
     * Creates an MQTT client based on the provided MQTT specification.
     *
     * @param spec The MQTT specification for creating the client.
     * @return The MQTT client.
     * @throws TransportException if there is an error creating the client.
     */
    public static MqttClient createMqttClient(AbstractMQTTSpecification spec) throws TransportException{
        var specId = spec.getId();
        if (!mqttClients.containsKey(specId)) {
            var client = initialize(spec);
            mqttClients.put(specId, client);
        }
        return mqttClients.get(specId);
    }


    /**
     * Enable SSL connection in MqttConnectionOptions with TLSv1.2 protocol.
     *
     * @param connOpts The MqttConnectionOptions object to enable SSL connection for.
     */
    private static void enableSSLConnection(MqttConnectionOptions connOpts){
        Properties sslProps = new Properties();
        sslProps.setProperty("com.ibm.ssl.protocol", "TLSv1.2");    // TLSv1.2

        //One of: SSL, SSLv3, TLS, TLSv1, SSL_TLS.
        connOpts.setSSLProperties(sslProps);
    }

    /**
     * Initializes an MQTT Client based on the provided MQTT specification.
     *
     * @param spec The MQTT specification for creating the client.
     * @return The initialized MQTT client.
     * @throws TransportException if there is an error initializing the client.
     */
    private static MqttClient initialize(AbstractMQTTSpecification spec) throws TransportException {
        MqttClient mqttClient;
        try {
            mqttClient = new MqttClient(spec.getBroker(), spec.getClientId(), new MemoryPersistence());

            var connOpts = new MqttConnectionOptions();
            connOpts.setCleanStart(spec.isCleanStart());
            connOpts.setSessionExpiryInterval(spec.getSessionExpiryInterval());
            enableSSLConnection(connOpts);

            // Set user properties
            if(spec.getUserproperties()!=null) {
                List<UserProperty> userPropertyList = new ArrayList<>();
                spec.getUserproperties().forEach((key, value) -> userPropertyList.add(new UserProperty((String) key, (String) value)));
                connOpts.setUserProperties(userPropertyList);
            }

            connOpts.setAutomaticReconnect( spec.isAutoReconnect());
            connOpts.setConnectionTimeout( spec.getConnectionTimeOut());
            connOpts.setKeepAliveInterval( spec.getKeepAliveInterval());

            if(spec.getTenant() != null && !spec.getTenant().isBlank()) {
                connOpts.setUserName(spec.getTenant() + "/" + spec.getUsername());
            } else {
                connOpts.setUserName(spec.getUsername());
            }

            // Set security
            if( spec.getSecuritySpecification()!= null){
                SecuritySpecification securitySpecification = spec.getSecuritySpecification();
                if( securitySpecification instanceof SSLSpecification sslSpecification) {
                    SSLSocketFactory sslSocketFactory = sslSpecification.getSocketFactory(spec.getPassword().toCharArray());
                    connOpts.setSocketFactory(sslSocketFactory);
                } else if( securitySpecification instanceof JwtClaimSpecification jwtClaimSpecification) {
                    connOpts.setPassword( jwtClaimSpecification.createToken().getBytes());
                }
            } else if( spec.getPassword() != null) {
                connOpts.setPassword( spec.getPassword().getBytes());
            }

            // Publisher only logic
            if(spec instanceof MQTTPublisherSpecification publisherSpecification && publisherSpecification.getLastWillSpecification()!=null) {
                handleLastWillProperties(publisherSpecification.getLastWillSpecification(), connOpts);
            }

            // CONNECT DEVICE
            mqttClient.connect(connOpts);

            // Send registration message
            if( spec.getDeviceRegistrationMessage() != null && !spec.getDeviceRegistrationMessage().isBlank()){
                sendDeviceRegistrationMessage(mqttClient, spec);
            }

            if( logger.isInfoEnabled()) {
                logger.info(String.format("Connected to broker [ %s ] ", spec.getBroker()));
            }
        } catch (MqttException | InvalidKeySpecException | UnrecoverableKeyException | KeyManagementException | NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
            throw new TransportInitializationException("Fatal error occurred during initialization.", e);
        }
        return mqttClient;
    }


    /**
     * Handles the last will properties for MQTT.
     *
     * @param lastWillSpecification The MQTT last will specification.
     * @param connOpts The MQTT connection options.
     */
    private static void handleLastWillProperties(MQTTLastWillSpecification lastWillSpecification, MqttConnectionOptions connOpts){
        MqttProperties properties = new MqttProperties();
        properties.setWillDelayInterval(lastWillSpecification.getIntervalDelay());
        properties.setMaximumQoS(lastWillSpecification.getQos());

        MqttMessage message = new MqttMessage(lastWillSpecification.getLastWillMessage().getBytes(), lastWillSpecification.getQos(), lastWillSpecification.isRetain(), null);
        connOpts.setWill(lastWillSpecification.getLastWillTopic(), message);
        connOpts.setWillMessageProperties(properties);
    }

    /**
     * Sends a device registration message using the given MQTT client and specification.
     *
     * @param mqttClient The MQTT client to use for sending the message.
     * @param spec The specification containing the device registration message, topic, and quality of service (QoS).
     * @throws MqttException if there is an error during message publishing.
     */
    private static void sendDeviceRegistrationMessage(MqttClient mqttClient, AbstractMQTTSpecification spec) throws MqttException {
        MqttMessage message = new MqttMessage( spec.getDeviceRegistrationMessage().getBytes() );
        message.setQos(spec.getQos());
        message.setRetained( true);
        mqttClient.publish(spec.getTopic(), message);
    }
}