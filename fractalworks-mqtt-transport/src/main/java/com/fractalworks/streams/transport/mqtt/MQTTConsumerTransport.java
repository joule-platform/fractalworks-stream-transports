/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;
import com.fractalworks.streams.sdk.codec.StreamEventJsonDeserializer;
import com.fractalworks.streams.sdk.codec.StreamEventParser;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportRuntimeException;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.transport.mqtt.specification.serialization.MQTTBatch;
import com.fractalworks.streams.transport.mqtt.specification.MQTTConsumerSpecification;
import org.apache.commons.lang3.SerializationUtils;
import org.eclipse.paho.mqttv5.client.*;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.eclipse.paho.mqttv5.common.packet.MqttProperties;
import org.xerial.snappy.Snappy;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * MQTT consumer implementation
 *
 * @author Lyndon Adams
 */
public class MQTTConsumerTransport extends AbstractConsumerTransport implements Runnable {

    private MqttClient mqttClient;
    private StreamEventParser parser = new StreamEventJsonDeserializer();
    private boolean isCompressed = false;
    private boolean isBatched = false;

    private final AtomicBoolean closed = new AtomicBoolean(false);

    public MQTTConsumerTransport() {
        super();
    }

    public MQTTConsumerTransport(MQTTConsumerSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        MQTTConsumerSpecification spec = (MQTTConsumerSpecification)specification;
        // Get connection to MQTT Broker
        mqttClient = MQTTClientFactory.createMqttClient( spec );

        // Set the deserializer process
        DeserializerSpecification deserializerSpecification = spec.getDeserializerSpecification();
        if(deserializerSpecification != null ) {
            // Create parser
            initialiseDeserializer(deserializerSpecification);
        }
    }

    /**
     * Initializes the deserializer based on the given deserializer specification.
     *
     * @param deserializerSpecification the deserializer specification to use
     * @throws TransportInitializationException if failed to create or initialize the parser
     */
    private void initialiseDeserializer(DeserializerSpecification deserializerSpecification) throws TransportInitializationException {

        try {
            parser = (deserializerSpecification.getParser()!=null) ? deserializerSpecification.getParser().getConstructor().newInstance() : parser;
            parser.initialise();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new TransportInitializationException("Failed to create parser class", e);
        } catch (StreamsException e) {
            throw new TransportInitializationException("Failed to initialise parser", e);
        }
        isCompressed = deserializerSpecification.isCompressed();
        isBatched = deserializerSpecification.isBatch();
    }

    @Override
    public void start() throws TransportException {
        super.start();
        try {
            MQTTConsumerSpecification spec = (MQTTConsumerSpecification)specification;
            mqttClient.setCallback(new MqttCallback() {
                @Override
                public void disconnected(MqttDisconnectResponse disconnectResponse) {
                    disconnectedHandler(disconnectResponse);
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    messageArrivedHandler(topic,message);
                }

                @Override
                public void deliveryComplete(IMqttToken token) {
                    if(logger.isDebugEnabled())
                        logger.debug("deliveryComplete for token {}", token.getMessageId());
                }

                @Override
                public void connectComplete(boolean reconnect, String serverURI) {
                    connectCompleteHandler(reconnect,serverURI);
                }

                @Override
                public void authPacketArrived(int reasonCode, MqttProperties properties) {
                    if(logger.isDebugEnabled())
                        logger.debug("Received auth packet with reason code {}, Properties {}", reasonCode, properties);
                }

                @Override
                public void mqttErrorOccurred(MqttException exception) {
                    if(logger.isErrorEnabled())
                        logger.error("Error occurred within MQTT client.", exception);
                }
            });

            mqttClient.subscribe(spec.getTopic(), spec.getQos());
        } catch (MqttException e) {
            throw new TransportInitializationException("Fatal error occurred during start up.", e);
        }
    }

    /**
     * Handles the completion of the connection to the MQTT broker.
     *
     * @param reconnect   true if the connection was a reconnection, false if it was an initial connection.
     * @param serverURI   the URI of the server that the connection was made to.
     */
    private void connectCompleteHandler(boolean reconnect, String serverURI) {
        if(logger.isDebugEnabled()) {
            if(reconnect)
                logger.debug("Reconnection to {} successful", serverURI);
            else
                logger.debug("Connection to {} successful", serverURI);
        }
    }

    /**
     * Handles the disconnection event from the MQTT broker.
     *
     * @param disconnectResponse the response containing information about the disconnection
     */
    private void disconnectedHandler(MqttDisconnectResponse disconnectResponse) {
        try {
            logger.error("Connection lost to MQTT broker. Reconnecting......", disconnectResponse.getException());
            mqttClient.reconnect();
            if( mqttClient.isConnected()){
                logger.info("Reconnection successful");
            } else {
                logger.error("Unable to reconnect");
                closed.getAndSet(true);
            }
        } catch (MqttException e) {
            logger.error("Failed to reconnect");
        }
    }

    /**
     * Handles the received MQTT message.
     *
     * @param topic the topic of the message
     * @param message the MQTT message
     * @throws IOException if an I/O error occurs while handling the message
     */
    private void messageArrivedHandler(String topic, MqttMessage message) throws IOException {
        if(logger.isDebugEnabled())
            logger.debug("Processing message from topic {}", topic);
        byte[] payload = message.getPayload();
        if (isCompressed) {
            payload = Snappy.uncompress(payload);
        }
        if( isBatched ){
            MQTTBatch mqttBatch = SerializationUtils.deserialize(payload);
            Collection<byte[]> payloads = mqttBatch.getPayloads();
            payloads.forEach(this::handlePayload);
        } else {
            handlePayload( payload);
        }
    }

    /**
     * Handles the payload received from the MQTT message.
     *
     * @param payload the payload received from the MQTT message
     */
    private void handlePayload(byte[] payload) {
        try {
            Collection<StreamEvent> events = parser.translate(payload);
            if (events != null) {
                events.forEach(evt -> {
                    forwardingChannel.publish(evt, new Context());
                    metrics.incrementMetric(Metric.PROCESSED);
                });

                if(transportListener!=null){
                    transportListener.onEvent(events);
                }
            }
        } catch (TranslationException e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            logger.error("Failure during event translation.", e);
        }
    }

    @Override
    public void shutdown() throws TransportException {
        try {
            if( mqttClient.isConnected()) {
                mqttClient.unsubscribe(((MQTTConsumerSpecification) specification).getTopic());
                mqttClient.disconnect();
                mqttClient.close();
                logger.info("Client disconnected.");
            } else {
                logger.info("Client already disconnected.");
            }
        } catch (MqttException e) {
            throw new TransportException("Fatal error occurred during shutdown.", e);
        }
    }

    @Override
    public void run() {
        try {
            start();
        } catch (TransportException e) {
            throw new TransportRuntimeException("Failed to startup",e);
        }
        while(!closed.get() ){
            Thread.onSpinWait();
        }
        try {
            shutdown();
        } catch (TransportException e) {
            throw new TransportRuntimeException("Failed to shutdown transport",e);
        }
    }
}
