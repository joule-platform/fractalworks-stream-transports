/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import org.eclipse.paho.mqttv5.client.IMqttMessageListener;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A listener class for receiving MQTT messages using the Joule MQTT library.
 * This class implements the IMqttMessageListener interface.
 * When a message arrives on the specified topic, the messageArrived method is called.
 *
 * @author Lyndon Adams
 */
public class MQTTJouleMessageListener implements IMqttMessageListener {

    protected Logger logger;

    public MQTTJouleMessageListener() {
        logger = LoggerFactory.getLogger(getClass().getName());
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        if(logger.isDebugEnabled())
            logger.debug("Received message on topic [{}] - msg[{}]", topic, message.toDebugString());
    }
}
