/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.transport.mqtt.specification.serialization.MQTTBatch;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecification;
import com.google.common.base.Objects;
import org.apache.commons.lang3.SerializationUtils;

import org.eclipse.paho.mqttv5.client.MqttClient;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;
import org.xerial.snappy.Snappy;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

/**
 * MQTT event publisher
 *
 * @author Lyndon Adams
 */
public class MQTTPublisherTransport extends AbstractPublisherTransport {

    private MqttClient mqttClient;

    private boolean isCompress = false;
    private boolean isBatch = false;
    private CustomTransformer<?> transformer;

    public MQTTPublisherTransport() {
        super();
    }

    public MQTTPublisherTransport(MQTTPublisherSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        MQTTPublisherSpecification spec = (MQTTPublisherSpecification)specification;
        mqttClient = MQTTClientFactory.createMqttClient( spec );

        SerializerSpecification serializerSpecification = spec.getSerializerSpecification();
        if( serializerSpecification!=null){
            // Either a transform function is provided or the formatter for StreamEvents is used
            if (serializerSpecification.getTransform() != null) {
                try {
                    transformer = serializerSpecification.getTransform().getConstructor().newInstance();
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException  e) {
                    throw new TransportInitializationException("Failed to create transformer instance.", e);
                }
            } else {
                formatterSpecification = serializerSpecification.getFormatter();
                formatterSpecification.initialise();
            }
            isCompress = serializerSpecification.isCompressed();
            isBatch = serializerSpecification.isBatch();
        }
    }

    /**
     * Send message and compress if required
     *
     * @param spec
     * @param payload
     * @throws MqttException
     * @throws IOException
     */
    private void sendMessage(MQTTPublisherSpecification spec, byte[] payload) throws MqttException, IOException {
        MqttMessage message = new MqttMessage(  (isCompress) ?  Snappy.rawCompress( payload , payload.length ) : payload );
        message.setQos(spec.getQos());
        mqttClient.publish(spec.getTopic(), message);
    }

    /**
     * Convert StreamEvent to byte[] either using a formatter or a custom transformer
     *
     * @param event
     * @return
     * @throws TranslationException
     */
    private byte[] convertStreamEvent(StreamEvent event) throws TranslationException {
        byte[] payload;
        if( transformer != null){
            Object obj = transformer.transform( event);
            if( obj instanceof byte[] byteArray){
                payload = byteArray;
            } else {
                payload = SerializationUtils.serialize((Serializable) obj);
            }
        } else {
            payload = ((String)formatterSpecification.format(event)).getBytes();
        }
        return payload;
    }

    private void batchPublish(Collection<StreamEvent> events) {
        try {
            MQTTPublisherSpecification spec = (MQTTPublisherSpecification)specification;
            MQTTBatch batch = new MQTTBatch(events.size());
            for(StreamEvent event : events) {
                batch.addElement( convertStreamEvent(event));
            }
            sendMessage(spec, batch.toByteArray());
            metrics.incrementMetric(Metric.PROCESSED, events.size());
        } catch(MqttException | IOException | TranslationException me) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            logger.error("Failure occurred during publishing", me);
        }
    }

    private void eventPublish(Collection<StreamEvent> events) {
        MQTTPublisherSpecification spec = (MQTTPublisherSpecification)specification;
        events.forEach(event -> {
            try {
                sendMessage(spec, convertStreamEvent( event));
                metrics.incrementMetric(Metric.PROCESSED);
            } catch(MqttException | IOException | TranslationException me) {
                metrics.incrementMetric(Metric.PROCESS_FAILED);
                logger.error("Failure occurred during publishing", me);
            }
        });
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
            // Send events as a batch process
            try {
                if ( isBatch) {
                    batchPublish(events);
                } else {
                    eventPublish(events);
                }
            } finally {
                isPublishing.compareAndSet(true, false);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MQTTPublisherTransport that = (MQTTPublisherTransport) o;
        return Objects.equal(mqttClient, that.mqttClient);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), mqttClient);
    }

    @Override
    public void shutdown() {
        super.shutdown();
        try {
            mqttClient.disconnect();
            mqttClient.close();
        } catch (MqttException e) {
            logger.error("MQTT client disconnection failure", e);
        }
    }
}
