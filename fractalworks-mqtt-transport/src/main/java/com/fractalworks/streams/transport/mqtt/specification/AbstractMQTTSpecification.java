/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.mqtt.MQTTPublisherTransport;
import com.fractalworks.streams.transport.security.SecuritySpecification;

import java.util.Objects;
import java.util.Properties;

/**
 * MQTT v5 publisher specification including best practices
 *
 * @author Lyndon Adams
 */
public abstract class AbstractMQTTSpecification extends AbstractTransportSpecification {

    protected String tenant;
    protected String username = "unused";
    protected String password;
    protected String deviceRegistrationMessage;
    protected String topic;
    protected int qos = 0;
    protected String broker;
    protected String clientId;
    protected SecuritySpecification securitySpecification;
    protected Properties userproperties;
    protected int connectionTimeOut = 30;
    protected int keepAliveInterval = 30;
    protected long sessionExpiryInterval = 300;
    protected boolean cleanStart = true;
    protected boolean autoReconnect = true;

    protected String id;

    protected AbstractMQTTSpecification(){
        super();
    }

    protected AbstractMQTTSpecification(String name) {
        super(name);
    }

    public String getTenant() {
        return tenant;
    }

    @JsonProperty(value = "tenant")
    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getUsername() {
        return username;
    }

    @JsonProperty(value = "username", required = true)
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @JsonProperty(value = "password", required = true)
    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceRegistrationMessage() {
        return deviceRegistrationMessage;
    }

    @JsonProperty(value = "registration message")
    public void setDeviceRegistrationMessage(String deviceRegistrationMessage) {
        this.deviceRegistrationMessage = deviceRegistrationMessage;
    }

    public boolean isAutoReconnect() {
        return autoReconnect;
    }

    @JsonProperty(value = "auto reconnect")
    public void setAutoReconnect(boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
    }

    public boolean isCleanStart() {
        return cleanStart;
    }

    @JsonProperty(value = "clean start")
    public void setCleanStart(boolean cleanStart) {
        this.cleanStart = cleanStart;
    }

    public long getSessionExpiryInterval() {
        return sessionExpiryInterval;
    }

    @JsonProperty(value = "sessionExpiry interval")
    public void setSessionExpiryInterval(long sessionExpiryInterval) {
        this.sessionExpiryInterval = sessionExpiryInterval;
    }

    public String getTopic() {
        return topic;
    }

    @JsonProperty(value = "topic", required = true)
    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getQos() {
        return qos;
    }

    @JsonProperty(value = "qos")
    public void setQos(int qos) {
        this.qos = qos;
    }

    public String getBroker() {
        return broker;
    }

    @JsonProperty(value = "broker", required = true)
    public void setBroker(String broker) {
        this.broker = broker;
    }

    public String getClientId() {
        return clientId;
    }

    @JsonProperty(value = "clientId", required = true)
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Properties getUserproperties() {
        return userproperties;
    }

    @JsonProperty(value = "user properties")
    public void setUserproperties(Properties userproperties) {
        this.userproperties = userproperties;
    }

    public SecuritySpecification getSecuritySpecification() {
        return securitySpecification;
    }

    @JsonProperty(value = "security")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setSecuritySpecification(SecuritySpecification securitySpecification) {
        this.securitySpecification = securitySpecification;
    }

    public int getConnectionTimeOut() {
        return connectionTimeOut;
    }

    @JsonProperty(value = "connection timeout")
    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }

    public int getKeepAliveInterval() {
        return keepAliveInterval;
    }

    @JsonProperty(value = "keepalive interval")
    public void setKeepAliveInterval(int keepAliveInterval) {
        this.keepAliveInterval = keepAliveInterval;
    }

    public String getId(){
        return id;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( topic == null || topic.isBlank()){
            throw new InvalidSpecificationException("Topic is required");
        }

        if( topic.contains(" ") ){
            throw new InvalidSpecificationException("Topic should not contain spaces");
        }
        if( topic.startsWith("/")){
            throw new InvalidSpecificationException("Topic should not start with leading forward slash");
        }

        if( broker == null || broker.isBlank()){
            throw new InvalidSpecificationException("Broker is required");
        }

        if( clientId == null || clientId.isBlank()){
            throw new InvalidSpecificationException("clientId is required");
        }

        if( qos <0 && qos > 2){
            throw new InvalidSpecificationException("QOS is out of bounds. Valid bounds {0,1,2}");
        }
        if (connectionTimeOut <= 0) {
            throw new InvalidSpecificationException("connectionTimeOut must be greater than 0");
        }
        if (keepAliveInterval < 0) {
            throw new InvalidSpecificationException("keepAliveInterval must be greater than -1");
        }

        if( securitySpecification != null){
            securitySpecification.validate();
        }

        id = getBroker() + ":" + getTenant();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractMQTTSpecification that)) return false;
        return qos == that.qos
                && Objects.equals(tenant, that.tenant)
                && Objects.equals(username, that.username)
                && Objects.equals(password, that.password)
                && Objects.equals(broker, that.broker)
                && Objects.equals(securitySpecification, that.securitySpecification)
                && Objects.equals(userproperties, that.userproperties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tenant, username, broker);
    }

    @Override
    public String toString() {
        return "MQTTPublisherSpecification{" +
                "tenant='" + tenant + '\'' +
                ", username='" + username + '\'' +
                ", password=" + password +
                ", deviceRegistrationMessage='" + deviceRegistrationMessage + '\'' +
                ", topic='" + topic + '\'' +
                ", qos=" + qos +
                ", broker='" + broker + '\'' +
                ", clientId='" + clientId + '\'' +
                ", securitySpecification=" + securitySpecification +
                ", properties=" + userproperties +
                ", connectionTimeOut=" + connectionTimeOut +
                ", keepAliveInterval=" + keepAliveInterval +
                "} " + super.toString();
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return MQTTPublisherTransport.class;
    }
}
