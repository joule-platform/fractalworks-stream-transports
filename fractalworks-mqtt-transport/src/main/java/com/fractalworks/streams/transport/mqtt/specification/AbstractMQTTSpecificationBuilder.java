/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt.specification;

import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.security.SecuritySpecification;
import java.util.Properties;

/**
 * Abstract MQTT specification builder has all the common setting required to connect to a MQTT broker
 *
 * @author Lyndon Adams
 */
public abstract class AbstractMQTTSpecificationBuilder extends AbstractSpecificationBuilder {

    protected String tenant;
    protected String username = "unused";
    protected String password;
    protected String deviceRegistrationMessage;
    protected String topic;
    protected int qos = 0;
    protected String broker;
    protected String clientId;
    protected boolean batch = false;
    protected SecuritySpecification securitySpecification;
    protected Properties properties;
    protected int connectionTimeOut = 60;
    protected int keepAliveInterval = 60;

    protected AbstractMQTTSpecificationBuilder() {
        super();
    }

    public AbstractMQTTSpecificationBuilder setTenant(String tenant) {
        this.tenant = tenant;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setDeviceRegistrationMessage(String deviceRegistrationMessage) {
        this.deviceRegistrationMessage = deviceRegistrationMessage;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setTopic(String topic) {
        this.topic = topic;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setQos(int qos) {
        this.qos = qos;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setBroker(String broker) {
        this.broker = broker;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setBatch(boolean batch) {
        this.batch = batch;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setSecuritySpecification(SecuritySpecification securitySpecification) {
        this.securitySpecification = securitySpecification;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setProperties(Properties properties) {
        this.properties = properties;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
        return this;
    }

    public AbstractMQTTSpecificationBuilder setKeepAliveInterval(int keepAliveInterval) {
        this.keepAliveInterval = keepAliveInterval;
        return this;
    }

    protected void assignCommonSetting(AbstractMQTTSpecification spec){
        spec.setTenant( tenant);
        spec.setUsername( username);
        spec.setPassword( password);
        spec.setDeviceRegistrationMessage( deviceRegistrationMessage);
        spec.setTopic( topic);
        spec.setQos(qos);
        spec.setBroker( broker);
        spec.setClientId(clientId);
        spec.setConnectionTimeOut(connectionTimeOut);
        spec.setKeepAliveInterval(keepAliveInterval);
        spec.setSecuritySpecification(securitySpecification);
        spec.setUserproperties(properties);
    }
}
