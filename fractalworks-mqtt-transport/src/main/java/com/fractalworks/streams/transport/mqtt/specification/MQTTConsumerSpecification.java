/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.mqtt.MQTTConsumerTransport;
import com.google.common.base.Objects;

/**
 * MQTT consumer specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "mqttConsumer")
public class MQTTConsumerSpecification extends AbstractMQTTSpecification {

    DeserializerSpecification mqttDeserializerSpecification = new DeserializerSpecification();

    public MQTTConsumerSpecification() {
        super();
    }

    public MQTTConsumerSpecification(String name) {
        super(name);
    }

    public DeserializerSpecification getDeserializerSpecification() {
        return mqttDeserializerSpecification;
    }

    @JsonProperty(value = "deserializer")
    public void setDeserializerSpecification(DeserializerSpecification mqttDeserializerSpecification) {
        this.mqttDeserializerSpecification = mqttDeserializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( mqttDeserializerSpecification == null){
            throw new InvalidSpecificationException("deserializer must be provided");
        }

        if(!isCleanStart() && sessionExpiryInterval < 0){
            throw new InvalidSpecificationException("sessionExpiryInterval value is invalid");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MQTTConsumerSpecification that = (MQTTConsumerSpecification) o;
        return Objects.equal(mqttDeserializerSpecification, that.mqttDeserializerSpecification);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), mqttDeserializerSpecification);
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return MQTTConsumerTransport.class;
    }
}
