/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;

/**
 * MQTT consumer specification builder
 * @author Lyndon Adams
 */
public class MQTTConsumerSpecificationBuilder extends AbstractMQTTSpecificationBuilder {

    DeserializerSpecification deserializerSpecification;

    public MQTTConsumerSpecificationBuilder() {
        specificationClass = MQTTConsumerSpecification.class;
    }

    public MQTTConsumerSpecificationBuilder setMQTTDeserializerSpecification(DeserializerSpecification specification){
        this.deserializerSpecification = specification;
        return this;
    }

    @Override
    public MQTTConsumerSpecification build() throws InvalidSpecificationException {
        MQTTConsumerSpecification spec = new MQTTConsumerSpecification();
        assignCommonSetting( spec);
        spec.setDeserializerSpecification( deserializerSpecification);

        spec.validate();

        return spec;
    }
}
