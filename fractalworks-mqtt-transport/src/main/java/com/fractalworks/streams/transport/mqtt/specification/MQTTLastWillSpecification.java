package com.fractalworks.streams.transport.mqtt.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;

import java.util.Objects;

/**
 * MQTT last will specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "last will")
public class MQTTLastWillSpecification {

    private String lastWillTopic;
    private String lastWillMessage = "Publisher disconnected from broker";

    private long intervalDelay = 5;
    private boolean retain = true;
    protected int qos = 2;

    public MQTTLastWillSpecification() {
        // REQUIRED
    }

    public String getLastWillTopic() {
        return lastWillTopic;
    }

    @JsonProperty(value = "topic", required = true)
    public void setLastWillTopic(String lastWillTopic) {
        this.lastWillTopic = lastWillTopic;
    }

    public String getLastWillMessage() {
        return lastWillMessage;
    }

    @JsonProperty(value = "message", required = true)
    public void setLastWillMessage(String lastWillMessage) {
        this.lastWillMessage = lastWillMessage;
    }

    public long getIntervalDelay() {
        return intervalDelay;
    }

    @JsonProperty(value = "interval delay")
    public void setIntervalDelay(long intervalDelay) {
        this.intervalDelay = intervalDelay;
    }

    public boolean isRetain() {
        return retain;
    }

    @JsonProperty(value = "retain")
    public void setRetain(boolean retain) {
        this.retain = retain;
    }

    public int getQos() {
        return qos;
    }

    @JsonProperty(value = "qos")
    public void setQos(int qos) {
        this.qos = qos;
    }

    public void validate() throws InvalidSpecificationException {
        if( lastWillTopic == null){
            throw new InvalidSpecificationException("lastWillTopic must be provided");
        }
        if( lastWillMessage == null || lastWillMessage.isEmpty()){
            throw new InvalidSpecificationException("lastWillMessage must be provided");
        }
        if( intervalDelay < 0 ){
            throw new InvalidSpecificationException("intervalDelay must be a positive value");
        }
        if( qos <0 && qos > 2){
            throw new InvalidSpecificationException("QOS is out of bounds. Valid bounds {0,1,2}");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MQTTLastWillSpecification that = (MQTTLastWillSpecification) o;
        return intervalDelay == that.intervalDelay && retain == that.retain && qos == that.qos && Objects.equals(lastWillTopic, that.lastWillTopic) && Objects.equals(lastWillMessage, that.lastWillMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastWillTopic, lastWillMessage, intervalDelay, retain, qos);
    }
}
