/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;

import com.fractalworks.streams.sdk.codec.SerializerSpecification;


/**
 * MQTT publisher specification builder
 *
 * @author Lyndon Adams
 */
public class MQTTPublisherSpecificationBuilder extends AbstractMQTTSpecificationBuilder {
    private SerializerSpecification serializerSpecification;
    private MQTTLastWillSpecification lastWillSpecification;

    public MQTTPublisherSpecificationBuilder() {
        specificationClass = MQTTPublisherSpecification.class;
    }

    public MQTTPublisherSpecificationBuilder setMQTTSerializerSpecification(SerializerSpecification specification){
        this.serializerSpecification = specification;
        return this;
    }

    public MQTTPublisherSpecificationBuilder setLastWillSpecification(MQTTLastWillSpecification lastWillSpecification) {
        this.lastWillSpecification = lastWillSpecification;
        return this;
    }

    @Override
    public MQTTPublisherSpecification build() throws InvalidSpecificationException {
        MQTTPublisherSpecification spec = new MQTTPublisherSpecification();
        assignCommonSetting( spec);
        spec.setSerializerSpecification(serializerSpecification);
        spec.setLastWillSpecification(lastWillSpecification);
        spec.validate();

        return spec;
    }
}
