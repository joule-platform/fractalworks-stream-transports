/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt.specification.serialization;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * consider iterable
 */
public class MQTTBatch implements Serializable {

    byte[][] payloads;
    private transient int ptr = 0;

    public MQTTBatch() {
    }

    public MQTTBatch(int entries) {
        payloads = new byte[entries][];
    }

    public void addElement(Object obj) {
        if (ptr < payloads.length) {
            if (obj instanceof byte[] byteArr) {
                payloads[ptr++] = byteArr;
            } else {
                payloads[ptr++] = SerializationUtils.serialize((Serializable) obj);
            }
        }
    }

    public Collection<byte[]> getPayloads(){
        return new ArrayList<>(Arrays.asList(payloads));
    }

    public byte[] toByteArray(){
        return SerializationUtils.serialize( this);
    }
}
