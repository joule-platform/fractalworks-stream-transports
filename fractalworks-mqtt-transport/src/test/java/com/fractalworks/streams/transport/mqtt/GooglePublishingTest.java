package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecification;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecificationBuilder;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;

import static org.junit.Assert.assertTrue;

@Disabled("Comment out when running locally")
public class GooglePublishingTest {

    @Test
    public void testPublishMessage() throws InvalidSpecificationException, TransportException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("valid/publisher/validGoogleMQTTPublishing.yaml").getFile());

        MQTTPublisherSpecificationBuilder builder = new MQTTPublisherSpecificationBuilder();
        MQTTPublisherSpecification spec = (MQTTPublisherSpecification) builder.build(file);

        MQTTPublisherTransport publisherTransport = new MQTTPublisherTransport(spec);
        publisherTransport.initialize();

        assertTrue(true);
    }

}
