/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.mqtt.specification.MQTTConsumerSpecification;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecification;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * MQTT published tests
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class LocalConsumerTest {

    private static final int EVENT_COUNT = 10;
    public static Collection<StreamEvent> events;

    public final String topic = "testCustomer";

    @BeforeClass
    public static void createStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT-1; i++){
            StreamEvent evt = new StreamEvent();
            evt.setEventType("customer");
            evt.addValue("customerId", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("city", "london" );
            events.add( evt);
        }
        StreamEvent evt = new StreamEvent();
        evt.setEventType("customer");
        evt.addValue("customerId", EVENT_COUNT);
        evt.addValue("firstname", "Lyndon");
        evt.addValue("surname", "Adams");
        evt.addValue("city", "london" );
        events.add( evt);
    }

    public MQTTPublisherTransport setupMQTTPublisherTransport(boolean isBatch, boolean compress) throws InvalidSpecificationException, TransportException {
        MQTTPublisherSpecification spec = new MQTTPublisherSpecification();
        spec.setBroker("tcp://localhost:1883");
        spec.setTopic("testCustomer");
        spec.setQos(0);

        spec.setClientId("iMacPublisher");
        spec.setTenant("uk:home");
        spec.setUsername("lyndon");
        spec.setPassword("lyndon");

        SerializerSpecification serializerSpecification = new SerializerSpecification();
        serializerSpecification.setCompressed( compress);
        serializerSpecification.setBatch( isBatch);

        spec.setSerializerSpecification( serializerSpecification);
        spec.validate();

        MQTTPublisherTransport transport = new MQTTPublisherTransport( spec);
        transport.initialize();
        transport.start();

        return transport;
    }

    public MQTTConsumerSpecification createMQTTConsumerTransportSpec(boolean isBatch, boolean compress) throws InvalidSpecificationException {

        MQTTConsumerSpecification spec = new MQTTConsumerSpecification();
        spec.setBroker("tcp://localhost:1883");
        spec.setTopic(topic + "/#");
        spec.setQos(0);

        spec.setClientId("iMacConsumer");
        spec.setTenant("uk:home");
        spec.setUsername("lyndon");
        spec.setPassword("lyndon");

        DeserializerSpecification deserializerSpecification = new DeserializerSpecification();
        deserializerSpecification.setBatch( isBatch);
        deserializerSpecification.setCompressed( compress);

        spec.setDeserializerSpecification( deserializerSpecification);
        spec.validate();
        return spec;
    }

    @Test
    public void consumeEventsTest() throws InvalidSpecificationException, TransportException {
        final AtomicInteger counter = new AtomicInteger();

        MQTTPublisherTransport publisherTransport = setupMQTTPublisherTransport( false, false);

        MQTTConsumerTransport consumerTransport = new MQTTConsumerTransport( createMQTTConsumerTransportSpec(false,false));
        consumerTransport.setTransportListener(events -> {
            events.forEach(System.out::println);
            counter.incrementAndGet();
        });
        Thread t = new Thread(consumerTransport);
        t.start();
        publisherTransport.publish( events);

        while( counter.get() != EVENT_COUNT){
            Thread.onSpinWait();
        }
    }
}