/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.mqtt.specification.MQTTLastWillSpecification;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecification;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.util.ArrayList;
import java.util.Collection;

/**
 * MQTT published tests
 *
 * @author Lyndon Adams
 */
@Disabled("Comment out when running locally")
public class LocalPublishingTest {

    private static final int EVENT_COUNT = 1000;
    public static Collection<StreamEvent> events;

    @BeforeClass
    public static void createStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT-1; i++){
            StreamEvent evt = new StreamEvent();
            evt.setEventType("customer");
            evt.addValue("customerId", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("city", "london" );
            events.add( evt);
        }
        StreamEvent evt = new StreamEvent();
        evt.setEventType("customer");
        evt.addValue("customerId", EVENT_COUNT);
        evt.addValue("firstname", "Lyndon");
        evt.addValue("surname", "Adams");
        evt.addValue("city", "london" );
        events.add( evt);
    }

    public MQTTPublisherTransport setupMQTTTransport(boolean isBatch, boolean compress) throws InvalidSpecificationException, TransportException {

        MQTTPublisherSpecification spec = new MQTTPublisherSpecification();
        spec.setBroker("tcp://localhost:1883");
        spec.setTopic("testCustomer");
        spec.setQos(0);
        spec.setClientId("iMacPublisher");
        spec.setTenant("uk:home");
        spec.setUsername("lyndon");
        spec.setPassword("lyndon");

        var serializerSpecification = new SerializerSpecification();
        serializerSpecification.setCompressed( compress);
        serializerSpecification.setBatch( isBatch);
        spec.setSerializerSpecification( serializerSpecification);

        MQTTLastWillSpecification lastWillSpecification = new MQTTLastWillSpecification();
        lastWillSpecification.setLastWillTopic("status/test-joule");
        lastWillSpecification.setLastWillMessage("Publisher gone");
        spec.setLastWillSpecification(lastWillSpecification);

        spec.validate();

        MQTTPublisherTransport transport = new MQTTPublisherTransport( spec);
        transport.initialize();
        transport.start();

        return transport;
    }

    @Test
    public void publishSingleEventsTest() throws InvalidSpecificationException, TransportException {
        MQTTPublisherTransport transport = setupMQTTTransport( false, false);
        transport.publish( events);
    }

    @Test
    public void publishBatchEvents() throws InvalidSpecificationException, TransportException {
        MQTTPublisherTransport transport = setupMQTTTransport( true, false);
        transport.publish( events);
    }

    @Test
    public void publishBatchCompressedEvents() throws InvalidSpecificationException, TransportException {
        MQTTPublisherTransport transport = setupMQTTTransport( true, true);
        transport.publish( events);
    }

}