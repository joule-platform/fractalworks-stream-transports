/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.mqtt;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecification;
import com.fractalworks.streams.transport.mqtt.specification.MQTTPublisherSpecificationBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;

/**
 * MQTT Yaml tests
 *
 * @author Lyndon Adams
 */
class MQTTPublishingYamlTest {

    @ParameterizedTest
    @ValueSource(strings = {
            "valid/publisher/validBasicMQTTPublishing.yaml",
            "valid/publisher/validBasicWithSeriailizerMQTTPublishing.yaml",
            "valid/publisher/validSSLMQTTPublishing.yaml",
            "valid/publisher/validJwtRSAMQTTPublishing.yaml",
            "valid/publisher/validJwtECMQTTPublishing.yaml"
    })
    void validMQTTYaml(String filename)  throws InvalidSpecificationException {
    ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        MQTTPublisherSpecificationBuilder builder = new MQTTPublisherSpecificationBuilder();
        MQTTPublisherSpecification spec = (MQTTPublisherSpecification) builder.build(file);
        Assertions.assertNotNull(spec);
    }
}
