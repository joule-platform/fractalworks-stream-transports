package com.fractalworks.streams.transport.rabbitmq;

import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import com.fractalworks.streams.transport.rabbitmq.specification.AbstractRabbitMQSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.Routing;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public abstract class AbstractRabbitTransport extends AbstractConsumerTransport {

    protected Connection connection;
    protected Channel channel;
    protected Routing routing;
    protected String queueName= "";

    protected AbstractRabbitTransport(){
        super();
    }
    protected AbstractRabbitTransport(AbstractRabbitMQSpecification specification) {
        super(specification);
    }
}
