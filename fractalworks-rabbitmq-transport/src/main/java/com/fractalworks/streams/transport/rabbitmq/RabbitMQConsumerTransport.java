/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq;

import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.StreamEventParser;
import com.fractalworks.streams.sdk.exceptions.StreamsException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportRuntimeException;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import com.fractalworks.streams.transport.rabbitmq.specification.Exchange;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQConsumerSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.Routing;
import com.rabbitmq.client.*;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.concurrent.TimeoutException;

/**
 * RabbitMQ consumer
 *
 * @author Lyndon Adams
 */
public class RabbitMQConsumerTransport extends AbstractConsumerTransport implements Runnable {

    private Connection connection;
    private Channel channel;
    private String queueName;
    private Consumer consumer;
    private boolean autoAck = true;
    private Routing routing;
    private String consumerTag;
    private StreamEventParser parser;

    public RabbitMQConsumerTransport() {
        super();
        logger = LoggerFactory.getLogger(RabbitMQConsumerTransport.class);
    }

    public RabbitMQConsumerTransport(RabbitMQConsumerSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();

        RabbitMQConsumerSpecification spec = (RabbitMQConsumerSpecification) specification;
        autoAck = spec.isAutoAck();
        routing = spec.getRoutingKey();
        queueName = spec.getQueueName();
        if(spec.getDeserializerSpecification()!=null && spec.getDeserializerSpecification().getParser()!=null) {
            try {
                parser = spec.getDeserializerSpecification().getParser().getConstructor().newInstance();
                parser.initialise();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException e) {
                throw new TransportInitializationException("Failed to create parser class", e);
            } catch (StreamsException e) {
                throw new TransportInitializationException("Failed to initialise parser", e);
            }
        }

        try {
            connection = RabbitMQUtils.newConnection(spec);
            channel = connection.createChannel();

            // Set quality of service
            channel.basicQos(spec.getPrefetchCount(), spec.isGlobal());

            // Using exchanges now bind
            Exchange exchange = spec.getExchange();
            if(exchange != null) {
                setupExchange(spec,exchange);
            } else {
                // Using a basic queue to exchange messages
                channel.queueDeclare(spec.getQueueName(), spec.isDurable(), spec.isExclusive(), spec.isAutoDelete(), spec.getArguments());
            }

            // Set up the consumer
            consumer = setupConsumer(channel);

            if (logger.isInfoEnabled()) {
                logger.info(String.format("RabbitMQ [%s] consumer initialized successfully.", spec.getName()));
            }

        } catch (TimeoutException | IOException e) {
            throw new TransportInitializationException("Fatal error occurred during initialization.", e);
        }
    }

    /**
     * Sets up the specified exchange for a RabbitMQ consumer.
     *
     * @param spec the RabbitMQ consumer specification
     * @param exchange the exchange to set up
     * @throws IOException if there is an error during setup
     */
    private void setupExchange(RabbitMQConsumerSpecification spec,Exchange exchange) throws IOException {
        String exchangeName =  exchange.getName();
        channel.exchangeDeclare(exchange.getName(), exchange.getType().value(), spec.isDurable(), spec.isAutoDelete(), exchange.getArguments());
        queueName = channel.queueDeclare().getQueue();

        // Now subscribe to all interested routing keys
        if(routing !=null) {
            // Subscribe to messages that match the bind key criteria
            for (String bindingKey : routing.getKeys()) {
                channel.queueBind(queueName, exchangeName, bindingKey, spec.getArguments());
            }
        } else {
            channel.queueBind(queueName, exchangeName, "");
        }
    }

    /**
     * Sets up a consumer for the provided channel.
     * @param channel the channel to set up the consumer for
     * @return the configured DefaultConsumer
     */
    private DefaultConsumer setupConsumer(Channel channel){
       return new DefaultConsumer(channel) {
           @Override
           public void handleDelivery(String consumerTag, Envelope envelope,
                                      AMQP.BasicProperties properties, byte[] body) throws IOException {
               try {
                   Collection<StreamEvent> events = deserializeEvent(body);
                   if(!autoAck){
                       channel.basicAck(envelope.getDeliveryTag(), true);
                   }

                   if (events != null && !events.isEmpty()) {
                       // Send on to internal channel
                       events.forEach(evt -> forwardingChannel.publish(evt, new Context()));
                       metrics.incrementMetric(Metric.PROCESSED, events.size());

                       if(transportListener != null){
                           transportListener.onEvent(events);
                       }
                   }
               } catch (Exception e) {
                   metrics.incrementMetric(Metric.PROCESS_FAILED);
                   logger.error("Error occurred during message processing.", e);
               }
           }
       };
    }

    /**
     * Deserializes a byte array payload into a collection of StreamEvent objects.
     *
     * @param payload the byte array payload to deserialize
     * @return a collection of StreamEvent objects
     * @throws TranslationException if there is an error during deserialization
     */
    private Collection<StreamEvent> deserializeEvent(byte[] payload) throws TranslationException {
        return parser.translate(SerializationUtils.deserialize(payload));
    }

    @Override
    public void start() throws TransportException {
        try {
            super.start();
            consumerTag = channel.basicConsume(queueName, autoAck, consumer);
        } catch (IOException e) {
            throw new TransportException("Error consuming messages",e);
        }
    }

    @Override
    public void shutdown() {
        try {
            if (connection != null) {
                channel.basicCancel(consumerTag);
                channel.close();
                connection.close();
            }
        } catch (TimeoutException | IOException e) {
            logger.error("Connection close failure.", e);
        }
    }

    @Override
    public void run() {
        try {
            start();
        } catch (TransportException e) {
            throw new TransportRuntimeException("Failed during startup",e);
        }
    }

    /**
     * Get the consumer tag.
     *
     * @return the consumer tag
     */
    public String getConsumerTag() {
        return consumerTag;
    }
}
