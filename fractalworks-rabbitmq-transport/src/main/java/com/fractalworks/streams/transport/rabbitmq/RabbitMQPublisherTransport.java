/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import com.fractalworks.streams.transport.rabbitmq.specification.Exchange;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQPublisherSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.Routing;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.TimeoutException;

/**
 * RabbitMQ event publisher
 *
 * @author Lyndon Adams
 */
public class RabbitMQPublisherTransport extends AbstractPublisherTransport {

    private Connection connection;
    private Channel channel;
    private Routing routing;
    private Exchange exchange;

    private CustomTransformer<?> transformer;

    private String queueName = "";
    private String exchangeName = "";

    private AMQP.BasicProperties basicProperties;

    public RabbitMQPublisherTransport(){
        super();
    }

    public RabbitMQPublisherTransport(RabbitMQPublisherSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        RabbitMQPublisherSpecification spec = (RabbitMQPublisherSpecification) specification;
        exchange = spec.getExchange();
        routing = spec.getRoutingKey();

        try {
            if( spec.getSerializerSpecification().getTransform() != null) {
                transformer = spec.getSerializerSpecification().getTransform().getConstructor().newInstance();
            } else {
                formatterSpecification = spec.getSerializerSpecification().getFormatter();
                formatterSpecification.initialise();
            }
            connection = RabbitMQUtils.newConnection(spec);

            // Now bind to exchange
            channel = connection.createChannel();
            basicProperties = RabbitMQUtils.createProperties(spec,formatterSpecification);

            if(exchange != null){
                exchangeName = exchange.getName();
                channel.exchangeDeclare(exchangeName, exchange.getType().value());
            } else {
                queueName = spec.getQueueName();
                channel.queueDeclare(spec.getQueueName(), spec.isDurable(), spec.isExclusive(), spec.isAutoDelete(), spec.getArguments());
            }

            if (logger.isInfoEnabled()) {
                logger.info("RabbitMQ connection successful.");
            }

        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException  e) {
            throw new TransportInitializationException("Failed to create transformer instance.", e);
        } catch (TimeoutException | IOException e) {
            throw new TransportInitializationException("Fatal error occurred during initialization.", e);
        }
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
            Map<String, Collection<StreamEvent>> eventsGroupBy;
            if(routing != null) {
                eventsGroupBy = createEventGroupBy(events);
            } else {
                eventsGroupBy = new HashMap<>();
                eventsGroupBy.put("NULL", events);
            }
            // Single events
            eventPublish(eventsGroupBy);
            isPublishing.compareAndSet(true, false);
        }
    }

    /**
     * Publishes events grouped by key using the provided dispatchEvent method.
     *
     * @param eventsGroupBy a Map containing a collection of events grouped by key
     */
    private void eventPublish(Map<String, Collection<StreamEvent>> eventsGroupBy) {
        eventsGroupBy.entrySet().stream().parallel().forEach( k ->{
            try {
                for (StreamEvent evt : k.getValue()) {
                    byte[] bytesToSend = serializeEvent(evt);
                    if( bytesToSend.length > 0) {
                        dispatchEvent(k.getKey(), bytesToSend);
                        metrics.incrementMetric(Metric.PROCESSED);
                    } else {
                        metrics.incrementMetric(Metric.PROCESS_FAILED);
                    }
                }
            } catch (IOException e) {
                metrics.incrementMetric(Metric.PROCESS_FAILED);
                logger.error("Failure occurred during batch object publishing", e);
            } catch (TranslationException e) {
                metrics.incrementMetric(Metric.PROCESS_FAILED);
                logger.error("Failure occurred during custom event transformation", e);
            }
        });
    }

    /**
     * Dispatches an event by publishing it to the RabbitMQ exchange.
     *
     * @param routingKey the routing key to use when publishing the event. If "NULL" is provided, an empty string will be used as the routing key.
     * @param bytesToSend the event data to be published as a byte array.
     * @throws IOException if an I/O error occurs while publishing the event.
     */
    private void dispatchEvent(String routingKey,byte[] bytesToSend) throws IOException {
        if (exchange != null) {
            channel.basicPublish(exchangeName, (routingKey.equals("NULL")) ? "" : routingKey, basicProperties, bytesToSend);
        } else {
            channel.basicPublish(exchangeName, queueName, basicProperties, bytesToSend);
        }
    }


    /**
     * Serializes a StreamEvent object into a byte array.
     *
     * @param e the StreamEvent object to serialize
     * @return the serialized byte array of the StreamEvent object, or an empty byte array if serialization fails
     * @throws TranslationException if an error occurs during event transformation
     */
    private byte[ ] serializeEvent(StreamEvent e) throws TranslationException {
        Object response  = ( transformer!=null) ? transformer.transform(e) : formatterSpecification.format(e);
        if( response instanceof Serializable serializableResponse){
            return SerializationUtils.serialize(serializableResponse);
        }
        return new byte[]{};
    }

    /**
     * Creates a routing key for a given StreamEvent object.
     * The routing key is created by concatenating the values of the fields defined in the routing object,
     * using the delimiter defined in the routing object.
     *
     * @param evt The StreamEvent object for which to create the routing key.
     * @return The created routing key as a String.
     */
    private String createRoutingKey(StreamEvent evt){
        StringBuilder sb = new StringBuilder();
        for(String f : routing.getFields()) {
            sb.append(evt.getValue(f)).append(routing.getDelimiter());
        }
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

    /**
     * Creates a Map of StreamEvents grouped by routing key.
     *
     * @param events the collection of StreamEvents to group
     * @return a Map of StreamEvents grouped by routing key
     */
    private Map<String, Collection<StreamEvent>> createEventGroupBy(Collection<StreamEvent> events){
        Map<String, Collection<StreamEvent>> groupedEvents = new HashMap<>();
        for (StreamEvent e: events) {
            String key = createRoutingKey(e);
            Collection<StreamEvent> streamEvents = groupedEvents.computeIfAbsent(key, k -> new ArrayList<>());
            streamEvents.add(e);
        }
        return groupedEvents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RabbitMQPublisherTransport that = (RabbitMQPublisherTransport) o;
        return Objects.equals(queueName, that.queueName) && Objects.equals(exchangeName, that.exchangeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), queueName, exchangeName);
    }

    @Override
    public void shutdown() {
        super.shutdown();
        try {
            channel.close();
            connection.close();
        } catch (TimeoutException | IOException e) {
            if(logger.isErrorEnabled())
                logger.error("Connection close failure.", e);
        }
    }
}
