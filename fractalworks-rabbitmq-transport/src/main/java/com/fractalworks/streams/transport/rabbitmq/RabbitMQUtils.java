package com.fractalworks.streams.transport.rabbitmq;

import com.fractalworks.streams.sdk.formatters.Formatter;
import com.fractalworks.streams.transport.rabbitmq.specification.AbstractRabbitMQSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQPublisherSpecification;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQUtils {

    private RabbitMQUtils(){}

    /**
     * Creates a new Connection to RabbitMQ server using the provided RabbitMQPublisherSpecification.
     *
     * @param spec the RabbitMQPublisherSpecification object containing the connection details
     * @return a new Connection object
     * @throws IOException if an I/O error occurs while creating the connection
     * @throws TimeoutException if the connection times out
     */
    public static Connection newConnection(AbstractRabbitMQSpecification spec) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(spec.getUser());
        factory.setPassword(spec.getPassword());

        factory.setHost(spec.getHost());
        factory.setPort(spec.getPort());
        factory.setVirtualHost(spec.getVirtualHost());
        return factory.newConnection();
    }

    /**
     * Creates AMQP basic properties for RabbitMQ publisher based on the specified RabbitMQPublisherSpecification.
     *
     * @param spec the RabbitMQPublisherSpecification object containing the properties details
     * @return the AMQP.BasicProperties object
     */
    public static AMQP.BasicProperties createProperties(RabbitMQPublisherSpecification spec, Formatter formatter){
        AMQP.BasicProperties.Builder propertiesBuilder = new AMQP.BasicProperties.Builder()
                .contentType(formatter.getContentType())
                .contentEncoding(formatter.getEncoding())
                .expiration(Long.toString(spec.getMessageExpiration()))
                .deliveryMode(spec.getDeliveryMode())
                .priority(spec.getPriority())
                .type("Stream-" + spec.getName())
                .userId(spec.getTargetUserAccount())
                .headers(spec.getHeaders());

        return propertiesBuilder.build();
    }
}
