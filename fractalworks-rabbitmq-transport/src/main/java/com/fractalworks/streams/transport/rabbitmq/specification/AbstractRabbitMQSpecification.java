/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;

import java.util.Map;
import java.util.Objects;

/**
 * Base RabbitMQ configurations
 *
 * @author Lyndon Adams
 */
public abstract class AbstractRabbitMQSpecification extends AbstractTransportSpecification {

    protected String user = "guest";
    protected String password = "guest";
    protected String host = "localhost";
    protected int port = 5672;
    protected String virtualHost = "/";

    protected Routing routing;

    protected Exchange exchange;
    protected String queueName;
    protected boolean durable = true;
    protected boolean exclusive = false;
    protected boolean autoDelete = true;
    protected Map<String, Object> arguments;

    protected AbstractRabbitMQSpecification() {
        // Required
    }

    protected AbstractRabbitMQSpecification(String name) {
        super(name);
    }

    public String getUser() {
        return user;
    }

    @JsonProperty(value = "user", required = false)
    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    @JsonProperty(value = "password", required = false)
    public void setPassword(String password) {
        this.password = password;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    @JsonProperty(value = "virtualHost")
    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public String getHost() {
        return host;
    }

    @JsonProperty(value = "host", required = true)
    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    @JsonProperty(value = "port", required = false)
    public void setPort(int port) {
        this.port = port;
    }

    public Routing getRoutingKey() {
        return routing;
    }

    @JsonProperty(value = "routing", required = false)
    public void setRoutingKey(Routing routing) {
        this.routing = routing;
    }


    public Exchange getExchange() {
        return exchange;
    }

    @JsonProperty(value = "exchange", required = false)
    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public String getQueueName() {
        return queueName;
    }

    @JsonProperty(value = "queue", required = true)
    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public boolean isDurable() {
        return durable;
    }

    @JsonProperty(value = "durable")
    public void setDurable(boolean durable) {
        this.durable = durable;
    }

    public boolean isExclusive() {
        return exclusive;
    }

    @JsonProperty(value = "exclusive")
    public void setExclusive(boolean exclusive) {
        this.exclusive = exclusive;
    }

    public boolean isAutoDelete() {
        return autoDelete;
    }

    @JsonProperty(value = "autoDelete")
    public void setAutoDelete(boolean autoDelete) {
        this.autoDelete = autoDelete;
    }

    public Map<String, Object> getArguments() {
        return arguments;
    }

    @JsonProperty(value = "arguments")
    public void setArguments(Map<String, Object> properties) {
        this.arguments = properties;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        AbstractRabbitMQSpecification that = (AbstractRabbitMQSpecification) o;
        return port == that.port && durable == that.durable && exclusive == that.exclusive && autoDelete == that.autoDelete && Objects.equals(user, that.user) && Objects.equals(password, that.password) && Objects.equals(host, that.host) && Objects.equals(virtualHost, that.virtualHost) && Objects.equals(exchange, that.exchange) && Objects.equals(routing, that.routing) && Objects.equals(queueName, that.queueName) && Objects.equals(arguments, that.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), user, password, host, port, virtualHost, exchange, routing, queueName, durable, exclusive, autoDelete, arguments);
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();

        if (host == null || host.isEmpty()) {
            throw new InvalidSpecificationException("url must be provided.");
        }

        if (port < 0) {
            throw new InvalidSpecificationException("port must be provided.");
        }

        if(user!=null) {

            if (user.isEmpty()) {
                throw new InvalidSpecificationException("user must be provided.");
            }

            if (password == null || password.isEmpty()) {
                throw new InvalidSpecificationException("password must be provided.");
            }
        }
    }
}
