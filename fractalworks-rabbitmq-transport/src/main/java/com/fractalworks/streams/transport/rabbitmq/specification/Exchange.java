/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Map;
import java.util.Objects;

@JsonRootName(value = "exchange")
public class Exchange {

    private String name;
    private ExchangeType type = ExchangeType.TOPIC;
    protected Map<String, Object> arguments;

    public Exchange() {
        // REQUIRED
    }

    public String getName() {
        return name;
    }

    @JsonProperty(value = "name", required = true)
    public void setName(String name) {
        this.name = name;
    }

    public ExchangeType getType() {
        return type;
    }

    @JsonProperty(value = "type", required = false)
    public void setType(ExchangeType type) {
        this.type = type;
    }

    public Map<String, Object> getArguments() {
        return arguments;
    }

    @JsonProperty(value = "arguments", required = false)
    public void setArguments(Map<String, Object> arguments) {
        this.arguments = arguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exchange exchange = (Exchange) o;
        return Objects.equals(name, exchange.name) && type == exchange.type && Objects.equals(arguments, exchange.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, arguments);
    }
}
