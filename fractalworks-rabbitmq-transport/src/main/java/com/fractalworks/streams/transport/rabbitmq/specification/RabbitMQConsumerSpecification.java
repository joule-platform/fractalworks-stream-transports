/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.DeserializerSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.rabbitmq.RabbitMQConsumerTransport;

import java.util.Objects;

/**
 * RabbitMQ subscription transport specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "rabbitConsumer")
public class RabbitMQConsumerSpecification extends AbstractRabbitMQSpecification {

    private boolean autoAck = true;
    private int prefetchCount = 1;
    private boolean global = true;

    private DeserializerSpecification deserializerSpecification;

    public RabbitMQConsumerSpecification() {
        super();
    }

    public RabbitMQConsumerSpecification(String name) {
        super(name);
    }

    public boolean isAutoAck() {
        return autoAck;
    }

    @JsonProperty(value = "autoAck")
    public void setAutoAck(boolean autoAck) {
        this.autoAck = autoAck;
    }

    public int getPrefetchCount() {
        return prefetchCount;
    }

    @JsonProperty(value = "prefetchCount")
    public void setPrefetchCount(int prefetchCount) {
        this.prefetchCount = prefetchCount;
    }

    public boolean isGlobal() {
        return global;
    }

    @JsonProperty(value = "global")
    public void setGlobal(boolean global) {
        this.global = global;
    }

    @JsonProperty(value = "deserializer", required = true)
    public DeserializerSpecification getDeserializerSpecification() {
        return deserializerSpecification;
    }

    public void setDeserializerSpecification(DeserializerSpecification deserializerSpecification) {
        this.deserializerSpecification = deserializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();

        if (exchange != null && exchange.getName() != null) {
            // Validate routing keys are correctly formatted
            if(exchange.getType().equals(ExchangeType.TOPIC)){
                routing.validate();
            }

        } else if (queueName == null || queueName.isEmpty()) {
            throw new InvalidSpecificationException("queueName must be provided when exchange is not provided");
        }

        if( deserializerSpecification == null){
            throw new InvalidSpecificationException("deserializer must be provided");
        }
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return RabbitMQConsumerTransport.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RabbitMQConsumerSpecification that)) return false;
        if (!super.equals(o)) return false;
        return autoAck == that.autoAck &&
                prefetchCount == that.prefetchCount &&
                global == that.global;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), autoAck, prefetchCount, global);
    }

    @Override
    public String toString() {
        return "RabbitMQConsumerSpecification{" +
                "autoAck=" + autoAck +
                ", prefetchCount=" + prefetchCount +
                ", global=" + global +
                "} " + super.toString();
    }
}
