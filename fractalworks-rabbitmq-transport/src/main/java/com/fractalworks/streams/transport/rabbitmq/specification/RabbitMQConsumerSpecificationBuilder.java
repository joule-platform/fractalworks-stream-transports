/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * RabbitMQ consumer specification builder
 *
 * @author Lyndon Adams on 19/12/2016.
 */
public class RabbitMQConsumerSpecificationBuilder extends AbstractSpecificationBuilder<RabbitMQConsumerSpecification> {

    private String user;
    private String password;
    private String host = "localhost";
    private int port = 5672;
    private String virtualHost = "/";
    private Exchange exchange;
    private boolean autoAck = true;
    private int prefetchCount = 1;
    private boolean global = true;
    private List<String> topics = new ArrayList<>();

    public RabbitMQConsumerSpecificationBuilder() {
        specificationClass = RabbitMQConsumerSpecification.class;
    }

    public RabbitMQConsumerSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setUser(String user) {
        this.user = user;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setHost(String host) {
        this.host = host;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setExchangeName(Exchange exchange) {
        this.exchange = exchange;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setAutoAck(boolean autoAck) {
        this.autoAck = autoAck;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setPrefetchCount(int prefetchCount) {
        this.prefetchCount = prefetchCount;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setGlobal(boolean global) {
        this.global = global;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder setTopics(List<String> topics) {
        this.topics = topics;
        return this;
    }

    public RabbitMQConsumerSpecificationBuilder addTopic(String topic) {
        this.topics.add(topic);
        return this;
    }

    @Override
    public RabbitMQConsumerSpecification build() throws InvalidSpecificationException {
        RabbitMQConsumerSpecification spec = new RabbitMQConsumerSpecification(name);
        spec.setHost(host);
        spec.setPort(port);
        spec.setVirtualHost(virtualHost);
        spec.setUser(user);
        spec.setPassword(password);
        spec.setExchange(exchange);
        spec.setAutoAck(autoAck);
        spec.setGlobal(global);
        spec.setPrefetchCount(prefetchCount);
        spec.validate();
        return spec;
    }

}
