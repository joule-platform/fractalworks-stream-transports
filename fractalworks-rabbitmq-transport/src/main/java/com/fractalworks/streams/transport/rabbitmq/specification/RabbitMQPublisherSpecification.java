/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.rabbitmq.RabbitMQPublisherTransport;

import java.util.Map;
import java.util.Objects;

/**
 * RabbitMQ publishing specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "rabbitPublisher")
public class RabbitMQPublisherSpecification extends AbstractRabbitMQSpecification {
    private long messageExpiration = 250;
    private int deliveryMode = 2;
    private int priority = 1;
    private String targetUserAccount = null;

    private Map<String, Object> headers;

    private SerializerSpecification serializerSpecification = new SerializerSpecification();

    public RabbitMQPublisherSpecification() {
        super();
    }

    public RabbitMQPublisherSpecification(String name) {
        super(name);
    }

    public long getMessageExpiration() {
        return messageExpiration;
    }

    @JsonProperty(value = "messageExpiration")
    public void setMessageExpiration(long messageExpiration) {
        this.messageExpiration = messageExpiration;
    }

    public int getDeliveryMode() {
        return deliveryMode;
    }

    @JsonProperty(value = "deliveryMode")
    public void setDeliveryMode(int deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public int getPriority() {
        return priority;
    }

    @JsonProperty(value = "priority")
    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTargetUserAccount() {
        return targetUserAccount;
    }

    @JsonProperty(value = "targetUserAccount")
    public void setTargetUserAccount(String targetUserAccount) {
        this.targetUserAccount = targetUserAccount;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    @JsonProperty(value = "headers")
    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }


    public SerializerSpecification getSerializerSpecification() {
        return serializerSpecification;
    }

    @JsonProperty(value = "serializer")
    public void setSerializerSpecification(SerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();

        if (deliveryMode != 1 && deliveryMode != 2) {
            throw new InvalidSpecificationException("deliveryMode can only be - 1 (non-persistent) or 2 (persistent).");
        }

        if (priority < 0 || priority > 9) {
            throw new InvalidSpecificationException("Invalid priority. Valid range (0..9).");
        }

        if(routing != null && routing.getKeys() !=null){
            throw new InvalidSpecificationException("keys not supported for publisher");
        }
        serializerSpecification.validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RabbitMQPublisherSpecification that = (RabbitMQPublisherSpecification) o;
        return messageExpiration == that.messageExpiration && deliveryMode == that.deliveryMode && priority == that.priority && Objects.equals(targetUserAccount, that.targetUserAccount) && Objects.equals(headers, that.headers) && Objects.equals(serializerSpecification, that.serializerSpecification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), messageExpiration, deliveryMode, priority, targetUserAccount, headers, serializerSpecification);
    }

    @Override
    public Class<? extends Transport> getComponentClass() {
        return RabbitMQPublisherTransport.class;
    }
}
