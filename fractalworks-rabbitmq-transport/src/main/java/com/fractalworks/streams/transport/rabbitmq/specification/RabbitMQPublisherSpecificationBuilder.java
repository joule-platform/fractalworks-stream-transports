/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.codec.SerializerSpecification;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;

import java.util.Map;

/**
 * RabbitMQ publisher specification builder
 *
 * @author Lyndon Adams
 */
public class RabbitMQPublisherSpecificationBuilder extends AbstractSpecificationBuilder<RabbitMQPublisherSpecification> {

    private String user;
    private String password;
    private String host = "localhost";
    private int port = 5672;
    private String virtualHost = "/";
    private Exchange exchange;
    private Routing routingKey;
    private long messageExpiration = 250;
    private long sendTimeout = 25;
    private int batchSize = 0;
    private int deliveryMode = 2;
    private int priority = 1;
    private String targetUserAccount = null;

    private SerializerSpecification serializerSpecification = new SerializerSpecification();

    private Map<String, Object> headers;


    public RabbitMQPublisherSpecificationBuilder() {
        specificationClass = RabbitMQPublisherSpecification.class;
    }

    public RabbitMQPublisherSpecificationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setUser(String user) {
        this.user = user;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setHostDetails(String host, int port) {
        this.host = host;
        this.port = port;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setExchange(Exchange exchange) {
        this.exchange = exchange;
        return this;
    }

    /**
     * @param routingKey
     * @return
     */
    public RabbitMQPublisherSpecificationBuilder setStaticRoutingKey(Routing routingKey) {
        this.routingKey = routingKey;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setMessageExpiration(long messageExpiration) {
        this.messageExpiration = messageExpiration;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public void setSendTimeout(long sendTimeout) {
        this.sendTimeout = sendTimeout;
    }

    public RabbitMQPublisherSpecificationBuilder setDeliveryMode(int deliveryMode) {
        this.deliveryMode = deliveryMode;
        return this;
    }

    public RabbitMQPublisherSpecificationBuilder setPriority(int priority) {
        this.priority = priority;
        return this;
    }



    /**
     * Provide a rabbit username for message to be sent too.
     *
     * @param targetUserAccount
     * @return
     */
    public RabbitMQPublisherSpecificationBuilder setTargetUserAccount(String targetUserAccount) {
        this.targetUserAccount = targetUserAccount;
        return this;
    }

    /**
     * Application header set
     *
     * @param headers
     */
    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    /**
     * Set serialization
     * @param serializerSpecification
     * @return
     */
    public RabbitMQPublisherSpecificationBuilder setSerializerSpecification(SerializerSpecification serializerSpecification) {
        this.serializerSpecification = serializerSpecification;
        return this;
    }

    @Override
    public RabbitMQPublisherSpecification build() throws InvalidSpecificationException {

        RabbitMQPublisherSpecification spec = new RabbitMQPublisherSpecification(name);
        spec.setHost(host);
        spec.setPort(port);
        spec.setPassword(password);
        spec.setUser(user);
        spec.setExchange(exchange);
        spec.setRoutingKey(routingKey);
        spec.setVirtualHost(virtualHost);
        spec.setSendTimeout(sendTimeout);
        spec.setBatchSize(batchSize);
        spec.setMessageExpiration(messageExpiration);
        spec.setPriority(priority);
        spec.setDeliveryMode(deliveryMode);
        spec.setTargetUserAccount(targetUserAccount);
        spec.setHeaders(headers);
        spec.setSerializerSpecification(serializerSpecification);

        spec.validate();
        return spec;
    }

}
