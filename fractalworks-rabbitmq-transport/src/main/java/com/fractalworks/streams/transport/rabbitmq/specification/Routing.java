/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rabbitmq.specification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

@JsonRootName(value = "routing")
public class Routing {

    private static final String ROUTING_KEY_PATTERN = "([a-zA-Z0-9*#]+.?)+";

    private static final char DELIMITER = '.';
    private List<String> fields;

    private List<String> keys;

    public Routing() {
        // REQUIRED
    }

    public char getDelimiter() {
        return DELIMITER;
    }

    public List<String> getFields() {
        return fields;
    }

    @JsonProperty(value = "event fields", required = false)
    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<String> getKeys() {
        return keys;
    }

    @JsonProperty(value = "keys", required = false)
    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public void validate() throws InvalidSpecificationException {
        if(keys != null){
            Pattern pattern = Pattern.compile(ROUTING_KEY_PATTERN);
            for(String k : keys) {
                if(k.getBytes().length > 255){
                    throw new InvalidSpecificationException(String.format("%s is too long. Maximum byte length is 255", k));
                }
                if( !pattern.matcher(k).matches() ){
                    throw new InvalidSpecificationException(String.format("Invalid binding key format for %s.", k));
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Routing routing = (Routing) o;
        return Objects.equals(fields, routing.fields) && Objects.equals(keys, routing.keys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(DELIMITER, fields, keys);
    }
}
