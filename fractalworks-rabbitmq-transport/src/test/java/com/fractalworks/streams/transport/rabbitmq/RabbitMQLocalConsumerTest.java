package com.fractalworks.streams.transport.rabbitmq;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQConsumerSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQConsumerSpecificationBuilder;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQPublisherSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQPublisherSpecificationBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertNotNull;

@Disabled("Comment out when running locally")
public class RabbitMQLocalConsumerTest {

    private static final int EVENT_COUNT = 1000;
    public static Collection<StreamEvent> events;

    @BeforeClass
    public static void createStreamEvents(){
        events = new ArrayList<>();
        for(int i=0; i<EVENT_COUNT-1; i++){
            StreamEvent evt = new StreamEvent();
            evt.setEventType("customer");
            evt.addValue("customerId", 1000 + i);
            evt.addValue("firstname", "John");
            evt.addValue("surname", "Smith");
            evt.addValue("city", "london" );
            events.add( evt);
        }
        StreamEvent evt = new StreamEvent();
        evt.setEventType("customer");
        evt.addValue("customerId", EVENT_COUNT);
        evt.addValue("firstname", "Lyndon");
        evt.addValue("surname", "Adams");
        evt.addValue("city", "london" );
        events.add( evt);
    }

    public void executePublishConsumeStreamEventsAsJson(String publisherFile, String consumerFile, int expectedEventCount) throws InvalidSpecificationException, TransportException {
        ClassLoader classLoader = getClass().getClassLoader();
        var file = new File(classLoader.getResource(publisherFile).getFile());

        var builder = new RabbitMQPublisherSpecificationBuilder();
        var spec = (RabbitMQPublisherSpecification) builder.build(file);
        assertNotNull(spec);

        var publisherTransport = new RabbitMQPublisherTransport(spec);
        publisherTransport.initialize();

        file = new File(classLoader.getResource(consumerFile).getFile());
        var consumerBuilder = new RabbitMQConsumerSpecificationBuilder();
        var consumerSpec = (RabbitMQConsumerSpecification) consumerBuilder.build(file);
        assertNotNull(spec);

        final AtomicInteger counter = new AtomicInteger();
        var consumerTransport = new RabbitMQConsumerTransport( consumerSpec);

        consumerTransport.setTransportListener(events -> {
            //events.forEach(System.out::println);
            counter.incrementAndGet();
        });
        consumerTransport.initialize();
        consumerTransport.start();

        publisherTransport.publish(events);


        while( counter.get() != expectedEventCount){
            Thread.onSpinWait();
        }
    }

    @Test
    public void simpleQueueTest() throws InvalidSpecificationException, TransportException {
        executePublishConsumeStreamEventsAsJson("localValidation/queue/validPublishingJsonEvents.yaml","localValidation/queue/validConsumeJsonEvents.yaml", EVENT_COUNT);
    }

    @Test
    public void simplePuSubTest() throws InvalidSpecificationException, TransportException {
        executePublishConsumeStreamEventsAsJson("localValidation/pubsub/validPublishingJsonEvents.yaml","localValidation/pubsub/validConsumeJsonEvents.yaml", EVENT_COUNT);
    }

    @Test
    public void simpleRoutingTest() throws InvalidSpecificationException, TransportException {
        executePublishConsumeStreamEventsAsJson("localValidation/routing/validPublishingJsonEvents.yaml","localValidation/routing/validConsumeJsonEvents.yaml", EVENT_COUNT);
    }

    @Test
    public void simpleTopicTest() throws InvalidSpecificationException, TransportException {
        executePublishConsumeStreamEventsAsJson("localValidation/topic/validPublishingJsonEvents.yaml","localValidation/topic/validConsumeJsonEvents.yaml", 1);
    }
}
