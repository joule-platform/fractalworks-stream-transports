/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport.rabbitmq;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQPublisherSpecification;
import com.fractalworks.streams.transport.rabbitmq.specification.RabbitMQPublisherSpecificationBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;

class RabbitMQPublisherValidYamlTest {

    @ParameterizedTest
    @ValueSource(strings = {"valid/publisher/validRabbitMQPublisherSimpleQueue.yaml",
            "valid/publisher/validRabbitMQPublisherWorkerQueue.yaml",
            "valid/publisher/validRabbitMQPublisherPubSub.yaml",
            "valid/publisher/validRabbitMQPublisherRouting.yaml",
            "valid/publisher/validRabbitMQPublisherTopic.yaml"})
    void validateRabbitPublishingDSL(String filename) throws InvalidSpecificationException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        RabbitMQPublisherSpecificationBuilder builder = new RabbitMQPublisherSpecificationBuilder();
        var spec = (RabbitMQPublisherSpecification) builder.build(file);
        Assertions.assertNotNull(spec);
    }

}
