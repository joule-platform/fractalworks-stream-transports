/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rdms;


import com.fractalworks.streams.sdk.exceptions.transport.TransportRuntimeException;

import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * This class provide the ability to reuse RDMSQueryHelper class when connection optimisation is required
 *
 * @author Lyndon Adams
 */
public class RDMSQueryFactory {

    public static final String JDBC_URL = "jdbcurl";

    private static final Map<String, RDMSQueryHelper> helpers = new HashMap<>();

    private RDMSQueryFactory() {}


    /**
     * Retrieves an instance of RDMSQueryHelper based on the provided Properties.
     * If an instance has already been created for the given jdbcurl, it will be reused.
     * Otherwise, a new instance will be created and stored for future use.
     *
     * @param properties The properties used to configure the RDMSQueryHelper instance.
     *                   Must contain the following properties:
     *                   - jdbcurl: The JDBC URL of the database
     *
     * @return The RDMSQueryHelper instance associated with the provided jdbcurl.
     *
     * @throws TransportRuntimeException If there is an error creating the RDMSQueryHelper instance.
     * @throws InvalidParameterException If the 'jdbcurl' property is missing or blank in the properties.
     */
    public static synchronized RDMSQueryHelper getRDMSQueryHelper(Properties properties) throws TransportRuntimeException {
        String jdbcurl = (String)properties.get(JDBC_URL);

        if( jdbcurl == null || jdbcurl.isBlank() ){
            throw new InvalidParameterException("Missing jdbcurl property");
        }

        helpers.computeIfAbsent(jdbcurl, k -> {
            try {
                return createHelper(properties);
            } catch (SQLException e) {
                throw new TransportRuntimeException(e);
            }
        });
        return helpers.get( jdbcurl );
    }

    /**
     * Creates an RDMSQueryHelper instance and initializes it with the given properties.
     *
     * @param properties the properties used to initialize the RDMSQueryHelper
     * @return an RDMSQueryHelper instance
     * @throws SQLException if an error occurs during initialization
     */
    private static RDMSQueryHelper createHelper(Properties properties) throws SQLException {
        RDMSQueryHelper helper = new RDMSQueryHelper();
        helper.initialize(properties);
        return helper;
    }
}
