/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.rdms;

import com.fractalworks.streams.sdk.referencedata.ReferenceDataObject;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.security.InvalidParameterException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Relational database utility class to support the loading of data to {@link ReferenceDataObject}. Class is leverages a
 * connection pool configured using passed properties file
 *
 * @author Lyndon Adams
 */
public class RDMSQueryHelper {

    private final Logger logger;
    private HikariDataSource dataSource;

    public RDMSQueryHelper() {
        logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    /**
     * Initializes the connection pool for the database.
     *
     * @param properties The properties used to configure the connection pool.
     *                   Must contain the following properties:
     *                   - "jdbcurl": The JDBC URL of the database
     *                   - "username": The username for the database connection
     *                   - "password": The password for the database connection
     *
     * @throws InvalidParameterException If any of the required properties are missing
     */
    public void initizedConnectionPool(Properties properties) {
        if (!properties.containsKey( RDMSQueryFactory.JDBC_URL)) {
            throw new InvalidParameterException("Missing jdbcurl property");
        }

        if (!properties.containsKey("username")) {
            throw new InvalidParameterException("Missing username property");
        }
        if (!properties.containsKey("password")) {
            throw new InvalidParameterException("Missing password property");
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl((String) properties.get( RDMSQueryFactory.JDBC_URL));
        config.setUsername((String) properties.get("username"));
        config.setPassword((String) properties.get("password"));
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        dataSource = new HikariDataSource(config);
    }

    /**
     * Initializes the database connection pool.
     *
     * @param properties The properties used to configure the connection pool. Must contain the following properties:
     *                   - jdbcurl: The JDBC URL of the database
     *                   - username: The username for the database connection
     *                   - password: The password for the database connection
     * @throws SQLException If there is an error while initializing the connection pool
     */
    public void initialize(Properties properties) throws SQLException {
        if(logger.isInfoEnabled()) {
            logger.info("Initializing database connection pool");
        }
        // Create connection pool
        initizedConnectionPool( properties);

        // Perform test query to gain basic meta data
        try(var c = dataSource.getConnection() ) {
            var stmt = c.prepareStatement("select 1;");
            try(var rs = stmt.executeQuery()) {
                rs.next();
            }
        }
        if(logger.isInfoEnabled()) {
            logger.info("Completed initializing database connection pool");
        }
    }

    /**
     * Retrieves a list of ReferenceDataObject based on the provided query.
     *
     * @param keyField The name of the column to be used as the key in the ReferenceDataObject.
     * @param query The SQL query to execute.
     * @param params The parameters to be used in the query, can be null.
     * @return A list of ReferenceDataObject retrieved from the database.
     */
    public List<ReferenceDataObject> queryReferenceData(final String keyField, final String query, final Object[] params) {
        List<ReferenceDataObject> referenceDataObjects = new ArrayList<>();
        try {
            var c = dataSource.getConnection();
            try(var stmt = c.prepareStatement(query)) {

                // Set query params
                if (params != null) {
                    for (int i = 0; i < params.length; i++) {
                        stmt.setObject(i + 1, params[i]);
                    }
                }

                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Executing query %s", query));
                }

                try (var rs = stmt.executeQuery()) {
                    var metaData = rs.getMetaData();
                    while (rs.next()) {
                        referenceDataObjects.add(buildReferenceDataObject(rs, metaData, keyField));
                    }
                }
            }
        } catch (SQLException ex) {
            logger.error("SQL during query function", ex);
        }
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Loaded %d rows", referenceDataObjects.size()));
        }
        return referenceDataObjects;
    }

    /**
     * Builds a ReferenceDataObject from a ResultSet and ResultSetMetaData.
     *
     * @param rs The ResultSet object containing the data retrieved from the database.
     * @param metaData The ResultSetMetaData object containing the metadata of the result set.
     * @param keyField The name of the column to be used as the key in the ReferenceDataObject.
     * @return The built ReferenceDataObject.
     * @throws SQLException If there is an error while accessing the ResultSet or ResultSetMetaData.
     */
    private ReferenceDataObject buildReferenceDataObject(ResultSet rs, ResultSetMetaData metaData, String keyField) throws SQLException {
        var r = new ReferenceDataObject();
        for (int i = 0; i < metaData.getColumnCount(); i++) {
            String columnName = metaData.getColumnName(i+1);
            Object value = rs.getObject( columnName);
            if (value instanceof String strValue) {
                value = strValue.strip();
            }
            if (columnName.equals(keyField)) {
                r.setKey(value);
            } else {
                r.put(columnName, (value != null) ? value : "");
            }
        }
        return r;
    }
}
