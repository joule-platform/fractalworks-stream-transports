/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;
import com.fractalworks.streams.transport.raspberrypi.sensors.PISensor;

import java.util.List;

/**
 * Specification builder for Raspberry PI sensor consumer
 *
 * @author Lyndon Adams
 */
public class RaspberryPISensorConsumerSpecificationBuilder extends AbstractSpecificationBuilder<RaspberryPISensorSpecification> {

    private String eventType;

    private List<PISensor> sensors;

    private long pollingTimeout = 5000L;

    private boolean combineInfo = true;

    public RaspberryPISensorConsumerSpecificationBuilder() {
        specificationClass = RaspberryPISensorSpecification.class;
    }

    public RaspberryPISensorConsumerSpecificationBuilder setEventType(String eventType) {
        this.eventType = eventType;
        return this;
    }

    public RaspberryPISensorConsumerSpecificationBuilder setSensors(List<PISensor> sensors) {
        this.sensors = sensors;
        return this;
    }

    public RaspberryPISensorConsumerSpecificationBuilder setPollingTimeout(long pollingTimeout) {
        this.pollingTimeout = pollingTimeout;
        return this;
    }

    public RaspberryPISensorConsumerSpecificationBuilder setCombineInfo(boolean combineInfo) {
        this.combineInfo = combineInfo;
        return this;
    }

    @Override
    public RaspberryPISensorSpecification build() throws InvalidSpecificationException {
        RaspberryPISensorSpecification spec = new RaspberryPISensorSpecification();
        spec.setEventType( eventType);
        spec.setSensors( sensors);
        spec.setCombineInfo( combineInfo);
        spec.setPollingTimeOut( pollingTimeout);
        spec.validate();
        return spec;
    }
}
