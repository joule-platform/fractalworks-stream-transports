/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;
import com.fractalworks.streams.transport.raspberrypi.sensors.PISensor;
import java.util.List;
import java.util.Objects;

/**
 * Raspberry PI sensor specification
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "raspberryPISensorConsumer")
public class RaspberryPISensorSpecification extends AbstractTransportSpecification {

    @JsonProperty(value = "eventType", required = true)
    private String eventType;

    private List<PISensor> sensors;

    @JsonProperty(value = "pollingFrequency")
    private long pollingTimeout = 5000L;

    @JsonProperty(value = "combineInfo")
    private boolean combineInfo = true;

    public RaspberryPISensorSpecification() {
    }

    public RaspberryPISensorSpecification(String name) {
        super(name);
    }

    public List<PISensor> getSensors() {
        return sensors;
    }

    @JsonProperty(value = "sensors", required = true)
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    public void setSensors(List<PISensor> sensors) {
        this.sensors = sensors;
    }

    public void setPollingTimeOut(long pollingTimeout) {
        this.pollingTimeout = pollingTimeout;
    }

    public long getPollingTimeOut() {
        return pollingTimeout;
    }

    public boolean isCombineInfo() {
        return combineInfo;
    }

    public void setCombineInfo(boolean combineInfo) {
        this.combineInfo = combineInfo;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    @JsonIgnore
    @Override
    public Class<? extends Transport> getComponentClass() {
        return RaspberryPiSensorConsumer.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RaspberryPISensorSpecification that)) return false;
        if (!super.equals(o)) return false;
        return pollingTimeout == that.pollingTimeout && combineInfo == that.combineInfo && Objects.equals(eventType, that.eventType) && Objects.equals(sensors, that.sensors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), eventType, sensors, pollingTimeout, combineInfo);
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( sensors == null || sensors.isEmpty()){
            throw new InvalidSpecificationException("sensor cannot be null or empty");
        }
        if( combineInfo && pollingTimeout < 250 ){
            throw new InvalidSpecificationException("pollingTimeout cannot be less than 250ms for multi sensor aggregation");
        }
        for(PISensor sensor : sensors){
            sensor.validate();
        }
    }
}
