/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi;

import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.management.channels.subscribable.PublishSubscribeChannel;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.transport.AbstractConsumerTransport;
import com.fractalworks.streams.transport.raspberrypi.sensors.Device;
import com.fractalworks.streams.transport.raspberrypi.sensors.PISensor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Raspberry PI sensor data collector
 *
 * @author Lyndon Adams
 */
public class RaspberryPiSensorConsumer extends AbstractConsumerTransport implements Runnable {

    private List<ScheduledFuture<?>> scheduledSensorCollectors;

    private Context ctx;

    private String eventType;

    private final Device hwInfo = new Device();

    private ScheduledExecutorService execSrv;

    private final AtomicBoolean closed = new AtomicBoolean(false);


    public RaspberryPiSensorConsumer(RaspberryPISensorSpecification specification) {
        super(specification);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        var spec = (RaspberryPISensorSpecification) getSpecification();
        scheduledSensorCollectors = new ArrayList<>();
        eventType = spec.getEventType();
        ctx = new Context();
        var hw = hwInfo.getSensorInfo();
        if (hw.isPresent()) {
            hw.get().forEach((k, v) -> ctx.addValue(k, v));
        }

        execSrv = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
        initialiseSensors();

        // Aggregated sensor data
        if (spec.isCombineInfo()) {
            SensorInfoCollector sensorInfoGathering = new SensorInfoCollector( spec.getSensors(), forwardingChannel);
            scheduledSensorCollectors.add(execSrv.scheduleAtFixedRate(sensorInfoGathering, spec.getPollingTimeOut(), spec.getPollingTimeOut(), TimeUnit.MILLISECONDS));
        }
    }

    /**
     * Setup sensors
     */
    private void initialiseSensors(){
        for (PISensor sensor : ((RaspberryPISensorSpecification) specification).getSensors()) {
            sensor.initialise();

            if( sensor.isLiveNotification()){
                sensor.addListener(event -> {
                    StreamEvent evt = new StreamEvent(eventType);
                    evt.setSubType( sensor.getType());
                    evt.addValue(sensor.getType().name(), event);
                    evt.addValue("serialNumber", ctx.getValue("serialNumber"));
                    forwardingChannel.publish( evt, ctx);
                });
            } else {
                // Uses a poll based frequency
                long frequency = sensor.getPollingFrequency();
                SensorInfoCollector sensorInfoGathering = new SensorInfoCollector(sensor, forwardingChannel);
                scheduledSensorCollectors.add(execSrv.scheduleAtFixedRate(sensorInfoGathering, frequency, frequency, TimeUnit.MILLISECONDS));
            }
        }
    }

    @Override
    public void run() {
        final long timeout = ((RaspberryPISensorSpecification) specification).getPollingTimeOut();
        try {
            while (!closed.get()) {
                // Change to wait and get notified on closed
                Thread.sleep( timeout );
            }
        } catch (Exception e) {
            logger.error("Critical error occurred.", e);
        } finally {
            try {
                shutdown();
            } catch (TransportException e) {
                logger.error("Failed to shutdown transport",e);
            }
        }
    }

    @Override
    public void shutdown() throws TransportException {
        super.shutdown();
        // shutdown sensor collectors
        scheduledSensorCollectors.forEach(s-> s.cancel(true));
    }

    private class SensorInfoCollector implements Runnable {
        private List<? extends PISensor> sensors;
        private PISensor sensor;
        private final PublishSubscribeChannel publishChannel;

        public SensorInfoCollector(PISensor sensor, PublishSubscribeChannel publishChannel) {
            this.sensor = sensor;
            this.publishChannel = publishChannel;
        }

        public SensorInfoCollector(List<? extends PISensor> sensors, PublishSubscribeChannel publishChannel) {
            this.sensors = sensors;
            this.publishChannel = publishChannel;
        }

        @Override public void run() {
            StreamEvent evt = new StreamEvent(eventType);
            evt.addValue("cpuTemperature", hwInfo.getCpuTemperature());

            if( sensor != null) {
                evt.addValue(sensor.getType().name(), sensor.getSensorInfo());
            } else {
                sensors.forEach(s-> {
                    var info = s.getSensorInfo();
                    if( info.isPresent()) {
                        evt.addValue(s.getType().name(), s.getSensorInfo());
                    }
                });
            }
            publishChannel.publish( evt, ctx);
            metrics.incrementMetric(Metric.PROCESSED);
        }
    }

}
