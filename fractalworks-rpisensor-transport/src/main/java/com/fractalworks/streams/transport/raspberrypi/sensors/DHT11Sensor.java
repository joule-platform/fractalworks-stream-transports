/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi.sensors;

import com.fractalworks.streams.core.data.Tuple;
import com.pi4j.wiringpi.Gpio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * DHT11Sensor provides temperature and humidity on a gpio pin
 *
 * @author Lyndon Adams
 */
public class DHT11Sensor extends PISensor {

    private final Logger logger;

    private static final int MAX_TIMINGS = 85;

    public DHT11Sensor() {
        this.logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    @Override
    public Optional<Map<String, Object>> getSensorInfo() {
        var data = readData();
        if (data != null) {
            Map<String, Object> map = new HashMap<>();
            map.put("humidityPercentage", data.getX());
            map.put("temperatureCelsius", data.getY());
            return Optional.of(map);
        }
        return Optional.empty();
    }

    private Tuple<Float,Float> readData() {
        int[] dht11dat = {0, 0, 0, 0, 0};

        int lastState = Gpio.HIGH;

        Gpio.pinMode(gpioPin, Gpio.OUTPUT);
        Gpio.digitalWrite(gpioPin, Gpio.LOW);
        Gpio.delay(18);

        Gpio.digitalWrite(gpioPin, Gpio.HIGH);
        Gpio.pinMode(gpioPin, Gpio.INPUT);

        int j = 0;

        for (int i = 0; i < MAX_TIMINGS; i++) {
            int counter = 0;
            while (Gpio.digitalRead(gpioPin) == lastState) {
                counter++;
                Gpio.delayMicroseconds(1);
                if (counter == 255) {
                    break;
                }
            }

            lastState = Gpio.digitalRead(gpioPin);

            if (counter == 255) {
                break;
            }

            /* ignore first 3 transitions */
            if (i >= 4 && i % 2 == 0) {
                /* shove each bit into the storage bytes */
                dht11dat[j / 8] <<= 1;
                if (counter > 16) {
                    dht11dat[j / 8] |= 1;
                }
                j++;
            }
        }
        // check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
        if (j >= 40 && checkParity(dht11dat)) {
            float humidity = (float) ((dht11dat[0] << 8) + dht11dat[1]) / 10;
            if (humidity > 100) {
                humidity = dht11dat[0]; // for DHT11
            }
            float celsius = (float) (((dht11dat[2] & 0x7F) << 8) + dht11dat[3]) / 10;
            if (celsius > 125) {
                celsius = dht11dat[2]; // for DHT11
            }
            if ((dht11dat[2] & 0x80) != 0) {
                celsius = -celsius;
            }

            return new Tuple<>(humidity, celsius);
        } else {
            logger.debug("Invalid data, skipping...");
        }
        return null;
    }

    private boolean checkParity(int[] dht11dat) {
        return dht11dat[4] == (dht11dat[0] + dht11dat[1] + dht11dat[2] + dht11dat[3] & 0xFF);
    }

    @Override
    public PISensorType getType() {
        return PISensorType.DHT11;
    }

}
