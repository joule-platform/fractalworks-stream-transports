/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi.sensors;

import com.fractalworks.streams.core.data.Tuple;
import com.pi4j.system.SystemInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Raspberry Pi hardware information
 *
 * @author Lyndon Adams
 */
public class Device extends PISensor {

    private final Logger logger;

    public Device() {
        this.logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    @Override
    public PISensorType getType() {
        return PISensorType.DEVICE;
    }

    @Override
    public Optional<Map<String, Object>> getSensorInfo() {
        var details = new HashMap<String, Object>();
        try{
            details.put("serialNumber", SystemInfo.getSerial());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access serialNumber");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("cpuRevision", SystemInfo.getCpuRevision());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access cpuRevision");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("cpuArchitecture", SystemInfo.getCpuArchitecture());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access cpuArchitecture");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("cpuPart", SystemInfo.getCpuPart());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access cpuPart");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("modelName", SystemInfo.getModelName());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access modelName");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("processor", SystemInfo.getProcessor());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access processor");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("hardware", SystemInfo.getHardware());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access hardware");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("revision", SystemInfo.getRevision());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access revision");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("isHardFloatAbi", SystemInfo.isHardFloatAbi());
        } catch(UnsupportedOperationException ex){
            logger.warn("Unable to access isHardFloatAbi");
            Thread.currentThread().interrupt();
        }

        try{
            details.put("boardType", SystemInfo.getBoardType().name());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access boardType");
            Thread.currentThread().interrupt();
        }
        return Optional.of(details);
    }

    /**
     * Get CPU temperature
     *
     * @return Tuple<String, Float>
     */
    public Tuple<String, Float> getCpuTemperature(){
        Tuple<String, Float> cpuTemperature = null;
        try{
            cpuTemperature = new Tuple<>("cpuTemperature", SystemInfo.getCpuTemperature());
        } catch(UnsupportedOperationException | InterruptedException | IOException ex){
            logger.warn("Unable to access cpuTemperature");
            Thread.currentThread().interrupt();
        }
        return cpuTemperature;
    }

}
