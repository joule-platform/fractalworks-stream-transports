/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi.sensors;

import com.pi4j.io.gpio.*;
import com.pi4j.wiringpi.Gpio;

import java.util.HashMap;
import java.util.Map;

/**
 * Gpio provider
 *
 * @author Lyndon Adams
 */
public class GpioPinProvider {

    private static GpioPinProvider instance;
    private static GpioController gpio;

    private static final Map<Integer, GpioPinDigitalInput> inputCache = new HashMap<>();
    private static final Map<Integer, GpioPinDigitalOutput> outputCache = new HashMap<>();

    private GpioPinProvider() {
        // Required
    }

    public static GpioPinProvider getInstance(){
        if( instance == null){
            gpio = gpioController();
            instance = new GpioPinProvider();
        }
        return instance;
    }

    /**
     * Provides digital input for pin.
     */
    public synchronized GpioPinDigitalInput getDigitalInput( Pin pin) {
        return inputCache.computeIfAbsent(pin.getAddress(), gpioPinId -> provideDigitalInput(pin));
    }

    /**
     * Provides digital output for pin.
     */
    public synchronized GpioPinDigitalOutput getDigitalOutput( Pin pin) {
        return outputCache.computeIfAbsent(pin.getAddress(), gpioPinId -> provideDigitalOutput(pin));
    }

    private static GpioPinDigitalInput provideDigitalInput(Pin pin) {
        return gpio.provisionDigitalInputPin(pin);
    }

    private static GpioPinDigitalOutput provideDigitalOutput(Pin pin) {
        return gpio.provisionDigitalOutputPin(pin);
    }

    private static GpioController gpioController() {
        if (Gpio.wiringPiSetup() == -1) {
            throw new IllegalStateException("Raspberry PI wiring lib is missing");
        }
        return GpioFactory.getInstance();
    }
}
