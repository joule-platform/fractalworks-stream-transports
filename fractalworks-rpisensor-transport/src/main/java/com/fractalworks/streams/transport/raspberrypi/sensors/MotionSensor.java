/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi.sensors;

import com.fractalworks.streams.core.data.Tuple;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Motion sensor alerting
 *
 * @author Lyndon Adams
 */
public class MotionSensor extends PISensor {

    public enum State { STARTED, ENDED}

    private AtomicLong startTime;

    private AtomicInteger motionsStarted;
    private AtomicInteger motionsEnded;

    ConcurrentLinkedQueue<Tuple<State, Long>> queue;

    public MotionSensor() {
        // Required
    }

    @Override
    public void initialise(){
        queue = new ConcurrentLinkedQueue<>();
        motionsStarted = new AtomicInteger();
        motionsEnded = new AtomicInteger();
        gpioPinProvider = GpioPinProvider.getInstance();

        startTime = new AtomicLong(System.currentTimeMillis());
        gpioPinProvider.getDigitalInput(RaspiPin.getPinByAddress(gpioPin))
                .addTrigger(new GpioCallbackTrigger(PinState.HIGH, this::motionStarted),
                        new GpioCallbackTrigger(PinState.LOW, this::motionEnded) );
    }

    private Void motionStarted(){
        var evt = new Tuple<>(State.STARTED, System.currentTimeMillis());
        if( liveNotification ) {
            notifyListener(evt);
        }

        motionsStarted.addAndGet(1);
        queue.add(evt);

        return null;
    }

    private Void motionEnded(){
        var evt = new Tuple<>(State.ENDED, System.currentTimeMillis());
        if( liveNotification) {
            notifyListener(evt);
        }
        motionsEnded.addAndGet(1);
        queue.add(evt);
        return null;
    }

    @Override
    public PISensorType getType() {
        return PISensorType.MOTION;
    }

    @Override
    public Optional<Map<String, Object>> getSensorInfo() {
        Map<String, Object> info = new HashMap<>();
        var time = System.currentTimeMillis();

        // Need a lock around these items
        List<Tuple<State, Long>> events = new ArrayList<>();
        queue.removeAll( events);
        var started = motionsStarted.get();
        var ended = motionsEnded.get();

        info.put("startTime", startTime.get());
        info.put("endTime", time);
        info.put("motionsStarted", started);
        info.put("motionsEnded", ended);
        info.put("motionsTimeseries", events);

        startTime.set( time);
        return Optional.of( info);
    }
}
