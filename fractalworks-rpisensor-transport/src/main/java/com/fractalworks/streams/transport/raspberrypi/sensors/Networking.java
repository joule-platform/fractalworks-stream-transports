/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi.sensors;

import com.pi4j.system.NetworkInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Raspberry PI networking information
 *
 * @author Lyndon Adams
 */
public class Networking extends PISensor {

    private final Logger logger;

    public Networking() {
        this.logger = LoggerFactory.getLogger(this.getClass().getName());
    }

    @Override
    public Optional<Map<String, Object>> getSensorInfo() {

        var details = new HashMap<String, Object>();

        try {
            details.put("hostname", NetworkInfo.getHostname());
        } catch (IOException | InterruptedException e) {
            logger.warn("Unable to access hostname");
            Thread.currentThread().interrupt();
        }

        try {
            details.put("ipaddresses", NetworkInfo.getIPAddresses());
        } catch (IOException | InterruptedException e) {
            logger.warn("Unable to ipaddresses hostname");
            Thread.currentThread().interrupt();
        }

        try {
            details.put("fqdn", NetworkInfo.getFQDNs());
        } catch (IOException | InterruptedException e) {
            logger.warn("Unable to fqdn hostname");
            Thread.currentThread().interrupt();
        }

        try {
            details.put("nameserver", NetworkInfo.getNameservers());
        } catch (IOException | InterruptedException e) {
            logger.warn("Unable to nameserver hostname");
            Thread.currentThread().interrupt();
        }

        return Optional.of(details);
    }

    @Override
    public PISensorType getType() {
        return PISensorType.NETWORK;
    }

}
