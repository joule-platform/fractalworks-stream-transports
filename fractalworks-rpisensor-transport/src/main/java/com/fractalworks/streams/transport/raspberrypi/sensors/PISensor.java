/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.raspberrypi.sensors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fractalworks.streams.core.data.Tuple;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;

import java.util.Map;
import java.util.Optional;

/**
 * Base interface for Raspberry PI sensors implementation
 *
 * @author Lyndon Adams
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Networking.class, name = "networking"),
        @JsonSubTypes.Type(value = Device.class, name = "device"),
        @JsonSubTypes.Type(value = DHT11Sensor.class, name = "dht11Sensor"),
        @JsonSubTypes.Type(value = MotionSensor.class, name = "motionSensor")
})
public abstract class PISensor {

    protected GpioPinProvider gpioPinProvider;

    protected int gpioPin = -1;

    protected SensorListener listener;

    protected boolean liveNotification = false;

    protected long pollingFrequency = 1000;

    public void initialise(){}

    public abstract PISensorType getType();

    public abstract Optional<Map<String, Object>> getSensorInfo();

    public void notifyListener(Tuple<?,?> evt){
        listener.onEvent(evt);
    }

    public void addListener(SensorListener l){
        listener = l;
    }

    /**
     * Set the ability to receive real time notifications for this sensor.
     * This function overrides the poll based functionality
     */
    @JsonProperty(value = "enableNotification")
    public void setLiveNotification(boolean enabled){
        this.liveNotification = enabled;
    }

    public boolean isLiveNotification() {
        return liveNotification;
    }

    /**
     * Default polling frequency of one second
     */
    @JsonProperty(value = "pollingFrequency")
    public void setPollingFrequency(long pollingFrequency) {
        this.pollingFrequency = pollingFrequency;
    }

    public int getGpioPin() {
        return gpioPin;
    }

    @JsonProperty(value = "gpioPin")
    public void setGpioPin(int gpioPin) {
        this.gpioPin = gpioPin;
    }

    public long getPollingFrequency() {
        return pollingFrequency;
    }

    public GpioPinProvider getGpioPinProvider() {
        return gpioPinProvider;
    }

    public void setGpioPinProvider(GpioPinProvider gpioPinProvider) {
        this.gpioPinProvider = gpioPinProvider;
    }

    public void validate() throws InvalidSpecificationException {
    }
}
