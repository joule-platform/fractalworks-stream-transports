/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.*;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Json Web Token using RSA and ES algorithm implementations
 *
 * Token can also be created using https://jwt.io/
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "jwtclaim")
public class JwtClaimSpecification implements SecuritySpecification {

    private final Logger logger;

    public enum ALGORITHM { RSA , EC}
    static final int DEFAULT_MINUTES = 60;

    private String audienceId;
    private String issuer;
    private String keyFile;

    private boolean isPrivate = true;

    private TimeUnit timeUnit = TimeUnit.DAYS;
    private int expiration = 31;

    private ALGORITHM algoType = ALGORITHM.RSA;

    public JwtClaimSpecification() {
        logger = LoggerFactory.getLogger(JwtClaimSpecification.class);

    }

    public ALGORITHM getAlgoType() {
        return algoType;
    }

    @JsonProperty(value = "algorithm")
    public void setAlgoType(ALGORITHM algoType) {
        this.algoType = algoType;
    }

    public String getIssuer() {
        return issuer;
    }

    @JsonProperty(value = "issuer")
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @JsonProperty(value = "audienceId", required = true)
    public void setAudienceId(String audienceId) {
        this.audienceId = audienceId;
    }

    public String getAudienceId() {
        return audienceId;
    }

    @JsonProperty(value = "timeUnit")
    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    @JsonProperty(value = "expiration")
    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public int getExpiration() {
        return expiration;
    }

    @JsonProperty(value = "keyFile", required = true)
    public void setKeyFile(String keyFile) {
        this.keyFile = keyFile;
    }

    public String getKeyFile() {
        return keyFile;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    @JsonProperty(value = "isPrivateKey")
    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    /**
     * Create JWT token using set attributes
     * @return
     */
    public String createToken() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        DateTime now = new DateTime();
        Date expirationDate;

        switch (timeUnit){
            case DAYS : expirationDate = now.plusDays( expiration ).toDate();
                        break;
            case HOURS: expirationDate = now.plusHours( expiration ).toDate();
                        break;
            case MINUTES: expirationDate = now.plusMinutes( expiration ).toDate();
                        break;
            default: expirationDate = now.plusMinutes( DEFAULT_MINUTES ).toDate();
                break;
        }

        return createToken( algoType, audienceId, issuer, keyFile, expirationDate, isPrivate);
    }


    /**
     * Create a time based JWT for the given audience id, signed with the given private key and expiration date
     */
    public static String createToken(ALGORITHM algorithm, String audienceId, String issuer, String keyFile, Date expirationDate, boolean isPrivate)
            throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        DateTime now = new DateTime();


        JwtBuilder jwtBuilder = Jwts.builder()
                .issuedAt(now.toDate())
                .expiration(expirationDate)
                .issuer(issuer);

        jwtBuilder.audience().add(audienceId);

        Key key = ( isPrivate ) ?
                readPrivateKeyFromFile(keyFile, algorithm.name()) :
                readPublicKeyFromFile(keyFile, algorithm.name());

        return jwtBuilder.signWith(key).compact();
    }


    public static PrivateKey readPrivateKeyFromFile(String filepath, String algorithm) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] bytes = parsePEMFile(new File(filepath));
        return (PrivateKey) getKey(bytes, algorithm, true);
    }

    public static PublicKey readPublicKeyFromFile(String filepath, String algorithm) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] bytes = parsePEMFile(new File(filepath));
        return (PublicKey) getKey(bytes, algorithm, false);
    }

    /**
     * Convert key byte to actual key
     *
     * @param keyBytes
     * @param algorithm
     * @param isPrivate
     * @return
     */
    private static Key getKey(byte[] keyBytes, String algorithm, boolean isPrivate) throws NoSuchAlgorithmException, InvalidKeySpecException {
        Key key = null;
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        key = (isPrivate) ? kf.generatePrivate(keySpec) : kf.generatePublic(keySpec);
        return key;
    }


    /**
     * Parse key file to byte[]
     *
     * @param pemFile
     * @return
     * @throws IOException
     */
    private static byte[] parsePEMFile(File pemFile) throws IOException {
        if (!pemFile.isFile() || !pemFile.exists()) {
            throw new FileNotFoundException(String.format("The file '%s' doesn't exist.", pemFile.getAbsolutePath()));
        }
        byte[] content;
        try (PemReader reader = new PemReader(new FileReader(pemFile))) {
            PemObject pemObject = reader.readPemObject();
            content = pemObject.getContent();
        }
        return content;
    }


    @Override
    public void validate() throws InvalidSpecificationException {
        if( audienceId == null || audienceId.isBlank()){
            throw new  InvalidSpecificationException("audienceId is required");
        }
        if( keyFile == null || keyFile.isBlank()){
            throw new  InvalidSpecificationException("keyFile is required");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JwtClaimSpecification that)) return false;
        return isPrivate == that.isPrivate && expiration == that.expiration && Objects.equals(logger, that.logger) && Objects.equals(audienceId, that.audienceId) && Objects.equals(issuer, that.issuer) && Objects.equals(keyFile, that.keyFile) && timeUnit == that.timeUnit && algoType == that.algoType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(logger, DEFAULT_MINUTES, audienceId, issuer, keyFile, isPrivate, timeUnit, expiration, algoType);
    }

    @Override
    public String toString() {
        return "JwtClaimSpecification{" +
                ", audienceId='" + audienceId + '\'' +
                ", issuer='" + issuer + '\'' +
                ", keyFile='" + keyFile + '\'' +
                ", isPrivate=" + isPrivate +
                ", timeUnit=" + timeUnit +
                ", expiration=" + expiration +
                ", algoType=" + algoType +
                '}';
    }
}
