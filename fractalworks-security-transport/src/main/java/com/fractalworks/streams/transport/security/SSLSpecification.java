/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Objects;

/**
 * SSL Specification support for X.509
 *
 * ES256
 * @author Lyndon Adams
 */
@JsonRootName(value = "ssl")
public class SSLSpecification implements SecuritySpecification {

    private final Logger logger;

    private String caCertFile;
    private String clientCertFile;
    private String privateKeyFile;

    public SSLSpecification() {
        logger = LoggerFactory.getLogger(SSLSpecification.class);
    }

    public SSLSocketFactory getSocketFactory(final char[] password)
            throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        Security.addProvider(new BouncyCastleProvider());

        // load CA certificate
        X509Certificate caCert = null;
        CertificateFactory cf = null;
        FileInputStream fis = new FileInputStream(caCertFile);
        try( BufferedInputStream bis = new BufferedInputStream(fis)) {
            cf = CertificateFactory.getInstance("X.509");
            while (bis.available() > 0) {
                caCert = (X509Certificate) cf.generateCertificate(bis);
            }
        }

        // load client certificate
        X509Certificate cert = null;
        try( BufferedInputStream bis = new BufferedInputStream(new FileInputStream(clientCertFile)) ) {
            while (bis.available() > 0) {
                cert = (X509Certificate) cf.generateCertificate(bis);
            }
        }

        // load client private key
        KeyPair key;
        try (PEMParser pemParser = new PEMParser(new FileReader(privateKeyFile))) {
            Object object = pemParser.readObject();
            PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder()
                    .build(password);
            JcaPEMKeyConverter converter = new JcaPEMKeyConverter()
                    .setProvider("BC");

            if (object instanceof PEMEncryptedKeyPair pemEncryptedKeyPair) {
                if(logger.isInfoEnabled())
                    logger.info("Encrypted key - we will use provided password");
                key = converter.getKeyPair(pemEncryptedKeyPair.decryptKeyPair(decProv));
            } else {
                if(logger.isInfoEnabled())
                    logger.info("Unencrypted key - no password needed");
                key = converter.getKeyPair((PEMKeyPair) object);
            }
        }

        // CA certificate is used to authenticate server
        KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
        caKs.load(null, null);
        caKs.setCertificateEntry("ca-certificate", caCert);
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
        tmf.init(caKs);

        // client key and certificates are sent to server so it can authenticate
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);
        ks.setCertificateEntry("certificate", cert);
        ks.setKeyEntry("private-key", key.getPrivate(), password,
                new java.security.cert.Certificate[] { cert });
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
                .getDefaultAlgorithm());
        kmf.init(ks, password);

        // finally, create SSL socket factory
        SSLContext context = SSLContext.getInstance("TLSv1.2");
        context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        return context.getSocketFactory();
    }

    public String getCaCertFile() {
        return caCertFile;
    }

    @JsonProperty(value = "caCertFile", required = true)
    public void setCaCertFile(String caCertFile) {
        this.caCertFile = caCertFile;
    }

    public String getClientCertFile() {
        return clientCertFile;
    }

    @JsonProperty(value = "clientCertFile", required = true)
    public void setClientCertFile(String clientCertFile) {
        this.clientCertFile = clientCertFile;
    }

    public String getPrivateKeyFile() {
        return privateKeyFile;
    }

    @JsonProperty(value = "privateKeyFile", required = true)
    public void setPrivateKeyFile(String privateKeyFile) {
        this.privateKeyFile = privateKeyFile;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        if( caCertFile == null || caCertFile.isBlank()){
           throw new  InvalidSpecificationException("caCertFile is required");
        }
        if( clientCertFile == null || clientCertFile.isBlank()){
            throw new  InvalidSpecificationException("clientCertFile is required");
        }
        if( privateKeyFile == null || privateKeyFile.isBlank()){
            throw new  InvalidSpecificationException("privateKeyFile is required");
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SSLSpecification that)) return false;
        return Objects.equals(caCertFile, that.caCertFile) && Objects.equals(clientCertFile, that.clientCertFile) && Objects.equals(privateKeyFile, that.privateKeyFile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(caCertFile, clientCertFile, privateKeyFile);
    }

    @Override
    public String toString() {
        return "SSLSpecification{" +
                "caCrtFile='" + caCertFile + '\'' +
                ", crtFile='" + clientCertFile + '\'' +
                ", keyFile='" + privateKeyFile + '\'' +
                '}';
    }
}
