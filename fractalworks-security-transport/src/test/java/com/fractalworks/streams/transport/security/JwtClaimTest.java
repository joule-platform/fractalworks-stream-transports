/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.security;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.*;

/**
 * Json Web Token tests for RSA and EC algorithms
 *
 * Keys used from - https://github.com/auth0/java-jwt/tree/master/lib/src/test/resources
 * Create EC keys - https://www.scottbrady91.com/OpenSSL/Creating-Elliptical-Curve-Keys-using-OpenSSL
 *
 * @author Lyndon Adams
 */
public class JwtClaimTest {

    @Test
    public void testRSATokenCreation() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        JwtClaimSpecification jwtClaim = new JwtClaimSpecification();
        jwtClaim.setAlgoType(JwtClaimSpecification.ALGORITHM.RSA);
        jwtClaim.setAudienceId("myprojectId");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("rsakey.pem").getFile());

        jwtClaim.setKeyFile( file.getAbsolutePath());

        String token = jwtClaim.createToken();
        assertNotNull( token);
        assertFalse(token.isBlank());
    }

    @Test
    public void testECTokenCreation() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        JwtClaimSpecification jwtClaim = new JwtClaimSpecification();
        jwtClaim.setAlgoType(JwtClaimSpecification.ALGORITHM.EC);
        jwtClaim.setAudienceId("myprojectId");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("ec-private-key.pem").getFile());
        jwtClaim.setKeyFile( file.getAbsolutePath());

        String token = jwtClaim.createToken();
        assertNotNull( token);
        assertFalse(token.isBlank());
    }
}
