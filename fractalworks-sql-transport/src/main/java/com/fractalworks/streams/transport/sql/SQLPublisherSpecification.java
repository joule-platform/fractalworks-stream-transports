package com.fractalworks.streams.transport.sql;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.transport.AbstractTransportSpecification;
import com.fractalworks.streams.sdk.transport.Transport;


import java.util.Objects;
import java.util.Properties;

/**
 * ANSI SQL event batch publisher
 * <p>
 * Events are published to a target databased either on a batch window or on a timeout. An 'insert' statement is
 * dynamically generated using the projection event provided defined by the use case select expression.
 * <p>
 * All JDBC compliant databases are supported.
 * <p>
 * Default setting
 * - Batch size: 1024
 * - Timeout: 5 seconds
 * - includeTimestamps: false
 * <p>
 * Specific database attributes are defined using the properties attribute.
 *
 * @author Lyndon Adams
 */
@JsonRootName(value = "sqlPublisher")
public class SQLPublisherSpecification extends AbstractTransportSpecification {

    private String jdbcDriver;
    private String host;
    private String database;
    private String table;
    private long timeout = 5000L;
    private boolean includeTimestamps = false;
    private boolean autocommit = false;

    private Properties properties;

    public SQLPublisherSpecification() {
    }

    public SQLPublisherSpecification(String name) {
        super(name);
    }

    public String getJdbcDriver() {
        return jdbcDriver;
    }

    @JsonProperty(value = "jdbcDriver", required = true)
    public void setJdbcDriver(String jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

    public String getHost() {
        return host;
    }

    @JsonProperty(value = "host", required = true)
    public void setHost(String host) {
        this.host = host;
    }

    public Properties getProperties() {
        return properties;
    }

    @JsonProperty(value = "properties", required = true)
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getDatabase() {
        return database;
    }

    @JsonProperty(value = "database", required = true)
    public void setDatabase(String database) {
        this.database = database;
    }

    public boolean isAutocommit() {
        return autocommit;
    }

    @JsonProperty(value = "autocommit", required = false)
    public void setAutocommit(boolean autocommit) {
        this.autocommit = autocommit;
    }

    public long getTimeout() {
        return timeout;
    }

    @JsonProperty(value = "timeout", required = false)
    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public boolean isIncludeTimestamps() {
        return includeTimestamps;
    }

    @JsonProperty(value = "includeTimestamps", required = false)
    public void setIncludeTimestamps(boolean includeTimestamps) {
        this.includeTimestamps = includeTimestamps;
    }

    public String getTable() {
        return table;
    }

    @JsonProperty(value = "table", required = true)
    public void setTable(String table) {
        this.table = table;
    }

    @Override
    public void validate() throws InvalidSpecificationException {
        super.validate();
        if( jdbcDriver == null || jdbcDriver.isBlank() )
            throw new InvalidSpecificationException("jdbcDriver must be provided");

        if( host == null || host.isBlank() )
            throw new InvalidSpecificationException("host must be provided");

        if( properties == null || properties.isEmpty() )
            throw new InvalidSpecificationException("properties must be provided");

        if( database == null || database.isBlank() )
            throw new InvalidSpecificationException("database must be provided");

        if( table == null || table.isBlank() )
            throw new InvalidSpecificationException("table must be provided");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SQLPublisherSpecification that = (SQLPublisherSpecification) o;
        return timeout == that.timeout && includeTimestamps == that.includeTimestamps && autocommit == that.autocommit && Objects.equals(jdbcDriver, that.jdbcDriver) && Objects.equals(host, that.host) && Objects.equals(database, that.database) && Objects.equals(table, that.table) && Objects.equals(properties, that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), jdbcDriver, host, database, table, timeout, includeTimestamps, autocommit, properties);
    }

    @JsonIgnore
    @Override
    public Class<? extends Transport> getComponentClass() {
        return SQLPublisherTransport.class;
    }
}
