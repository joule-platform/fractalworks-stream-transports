package com.fractalworks.streams.transport.sql;

import com.fractalworks.streams.core.exceptions.InvalidSpecificationException;
import com.fractalworks.streams.sdk.specification.AbstractSpecificationBuilder;

import java.util.Properties;

/**
 * SQL publisher transport builder
 *
 * @author Lyndon Adams
 */
public class SQLPublisherSpecificationBuilder extends AbstractSpecificationBuilder<SQLPublisherSpecification> {
    private String jdbcDriver;
    private String connection;
    private String database;
    private String table;
    private boolean includeTimestamps = false;

    private Properties properties;
    private long timeout = 5000L;
    protected int batchSize = 1024;
    private boolean autocommit = false;

    public SQLPublisherSpecificationBuilder() {
        specificationClass = SQLPublisherSpecification.class;
    }

    public SQLPublisherSpecificationBuilder setJdbcDriver(String jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
        return this;
    }

    public SQLPublisherSpecificationBuilder setConnection(String connection) {
        this.connection = connection;
        return this;
    }

    public SQLPublisherSpecificationBuilder setDatabase(String database) {
        this.database = database;
        return this;
    }

    public SQLPublisherSpecificationBuilder setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public SQLPublisherSpecificationBuilder setIncludeTimestamps(boolean includeTimestamps) {
        this.includeTimestamps = includeTimestamps;
        return this;
    }

    public SQLPublisherSpecificationBuilder setTable(String table) {
        this.table = table;
        return this;
    }

    public SQLPublisherSpecificationBuilder setAutocommit(boolean autocommit) {
        this.autocommit = autocommit;
        return this;
    }

    public SQLPublisherSpecificationBuilder setProperties(Properties properties) {
        this.properties = properties;
        return this;
    }

    @Override
    public SQLPublisherSpecification build() throws InvalidSpecificationException {

        SQLPublisherSpecification spec = new SQLPublisherSpecification();
        spec.setJdbcDriver(jdbcDriver);
        spec.setHost(connection);
        spec.setDatabase(database);
        spec.setAutocommit(autocommit);
        spec.setTable(table);
        spec.setProperties(properties);

        spec.setIncludeTimestamps(includeTimestamps);
        spec.setTimeout(timeout);
        spec.setBatchSize(batchSize);
        spec.validate();

        return spec;
    }
}
