package com.fractalworks.streams.transport.sql;

import com.fractalworks.streams.core.data.streams.Metric;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.sdk.exceptions.transport.TransportException;
import com.fractalworks.streams.sdk.exceptions.transport.TransportInitializationException;
import com.fractalworks.streams.sdk.transport.AbstractPublisherTransport;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Relational SQL database publisher transport
 * <p>
 * Supported SQL: INSERT
 * <p>
 * @author Lyndon Adams
 */
public class SQLPublisherTransport extends AbstractPublisherTransport {

    private Connection connection;
    private PreparedStatement preparedStatement;

    boolean includeTimestamps = false;
    Map<String, Integer> fieldToPlacemenet = new HashMap<>();

    public SQLPublisherTransport() {
        super();
        logger = LoggerFactory.getLogger(SQLPublisherSpecification.class);
    }

    public SQLPublisherTransport(SQLPublisherSpecification specification) {
        super(specification);
        logger = LoggerFactory.getLogger(SQLPublisherSpecification.class);
    }

    @Override
    public void initialize() throws TransportException {
        super.initialize();
        SQLPublisherSpecification spec = (SQLPublisherSpecification) specification;
        try {
            Class.forName(spec.getJdbcDriver());
        } catch (ClassNotFoundException e) {
            throw new TransportInitializationException(String.format("Unable to load %s.",spec.getJdbcDriver()), e);
        }

        includeTimestamps = spec.isIncludeTimestamps();

        // Connect to DB
        String url = spec.getHost() + "/" + spec.getDatabase();
        try {
            connection = DriverManager.getConnection(url, spec.getProperties());
            connection.setAutoCommit( spec.isAutocommit());
        } catch (SQLException e) {
            throw new TransportInitializationException(String.format("Failed to gain a connection to %s.",url), e);
        }
        if (logger.isInfoEnabled()) {
            logger.info("Initialization completed");
        }
    }

    /**
     * Create a dynamic insert statement based upon the passed <code>StreamEvent</code> attributes
     * @param evt
     * @return
     */
    private String createInsertStatement(StreamEvent evt, String tableName){
        int position = (includeTimestamps) ? 3 : 1;

        StringBuilder sqlBuilder = new StringBuilder();
        StringBuilder sqlValuesBuilder = new StringBuilder();
        sqlBuilder.append("INSERT INTO ").append(tableName).append("(");
        sqlValuesBuilder.append("VALUES (");

        if(includeTimestamps) {
            sqlBuilder.append("eventTime, ingestTime");
            sqlValuesBuilder.append("?,?");
        }

        for(String field : evt.getDictionary().keySet()){
            sqlBuilder.append(field).append(",");
            sqlValuesBuilder.append("?,");
            fieldToPlacemenet.put( field, position++);
        }

        int i = sqlValuesBuilder.lastIndexOf(",");
        sqlValuesBuilder.replace(i,i+1,")");
        i = sqlBuilder.lastIndexOf(",");
        sqlBuilder.replace(i,i+1,")").append(sqlValuesBuilder);
        return sqlBuilder.toString();
    }

    /**
     * Performs dynamic configuration for the SQLPublisherTransport class based on the given StreamEvent.
     *
     * @param event the StreamEvent used for dynamic configuration
     * @throws SQLException if an error occurs during dynamic configuration
     */
    private void dynamicConfiguration(StreamEvent event) throws SQLException {
        SQLPublisherSpecification spec = (SQLPublisherSpecification) specification;
        String statement = createInsertStatement(event, spec.getTable());
        preparedStatement = connection.prepareStatement( statement);

        if( logger.isDebugEnabled()) {
            logger.debug(String.format("Created [%s] for database [%s]", statement, spec.getDatabase()));
        }
    }

    @Override
    public void publish(Collection<StreamEvent> events) {
        SQLPublisherSpecification spec = (SQLPublisherSpecification) specification;
        try {
            if (!events.isEmpty() && isPublishing.compareAndSet(false, true)) {
                preparedBatchStatement(events);
                int[] responses = preparedStatement.executeBatch();
                if( !spec.isAutocommit() ) connection.commit();

                preparedStatement.clearBatch();
                metrics.incrementMetric(Metric.PROCESSED, responses.length);
            }
        } catch (Exception e) {
            metrics.incrementMetric(Metric.PROCESS_FAILED);
            logger.error("Error occurred during event publishing to sql store.", e);
        } finally {
            isPublishing.compareAndSet(true, false);
        }
    }

    /**
     * Executes a batch of prepared statements for a collection of StreamEvents.
     *
     * @param events The collection of StreamEvents to be processed.
     * @throws TransportException if an error occurs during SQL execution.
     */
    private void preparedBatchStatement(Collection<StreamEvent> events) throws TransportException {
        try {
            for (StreamEvent evt : events) {
                if(fieldToPlacemenet.isEmpty()) {
                    dynamicConfiguration(evt);
                }

                if(includeTimestamps) {
                    preparedStatement.setObject(1, evt.getEventTime());
                    preparedStatement.setObject(2, evt.getIngestTime());
                }
                for(Map.Entry<String,Integer> entry : fieldToPlacemenet.entrySet()){
                    preparedStatement.setObject(entry.getValue(), evt.getValue(entry.getKey()));
                }
                preparedStatement.addBatch();

            }
        } catch (SQLException e) {
            try {
                preparedStatement.clearBatch();
            } catch (SQLException ex) {
                if(logger.isErrorEnabled())
                    logger.error("Failed to clear SQL batch",ex);
            }
            throw new TransportException("SQL publishing failed due to SQL error", e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SQLPublisherTransport that = (SQLPublisherTransport) o;
        return includeTimestamps == that.includeTimestamps && Objects.equals(connection, that.connection) && Objects.equals(preparedStatement, that.preparedStatement) && Objects.equals(fieldToPlacemenet, that.fieldToPlacemenet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), includeTimestamps, fieldToPlacemenet);
    }
}
