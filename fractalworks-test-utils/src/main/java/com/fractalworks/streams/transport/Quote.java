/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fractalworks.streams.transport;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Simple Quote record
 *
 * @author Lyndon Adams
 */
public record Quote(String symbol,
                    double mid,
                    double bid,
                    double ask,
                    long volume,
                    double volatility,
                    long time,
                    LocalDate date) implements Serializable {}
