/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.streams.transport.serializers;

import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.core.exceptions.TranslationException;
import com.fractalworks.streams.sdk.codec.CustomTransformer;
import com.fractalworks.streams.transport.Customer;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Example domain type transformer
 */
public class CustomerTransformer implements CustomTransformer<Customer> {

    public CustomerTransformer() {
        // Required
    }

    @Override
    public Collection<Customer> transform(Collection<StreamEvent> events) throws TranslationException {
        Collection<Customer> customers = new ArrayList<>();
        for (StreamEvent event : events) {
            customers.add(transform(event));
        }
        return customers;
    }

    @Override
    public Customer transform(StreamEvent event) throws TranslationException {
        Customer customer = new Customer();
        customer.setId( (int)event.getValue("Id"));
        customer.setFirstname( (String) event.getValue("firstname"));
        customer.setSurname( (String) event.getValue("surname"));
        customer.setAge( (int) event.getValue("age"));
        return customer;
    }
}
